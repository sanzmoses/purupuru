<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function scopeSearch($query, $string) {
        return $query->where('id', 'like', '%'.$string.'%')
        ->orWhere('number', 'like', '%'.$string.'%')
        ->orWhere('message', 'like', '%'.$string.'%');
    }
}
