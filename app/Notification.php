<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

    public function timeDiff() {
        $carbon = new Carbon($this->created_at);
        return $carbon->diffForHumans();
    }
}
