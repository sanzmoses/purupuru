<?php

namespace App\Listeners;

use App\Notification;
use App\Events\Notify;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class StoreNotify
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Notify  $event
     * @return void
     */
    public function handle(Notify $event)
    {
        $exist = Notification::where('name', $event->name)->where('status', 'new');
        // Log::info(json_encode($exist));

        if($exist->count() > 0) {
            $exist = $exist->first();
            $exist->qty = $exist->qty + 1;
            $exist->save();
        }
        else {
            $notif = new Notification;
            $notif->name = $event->name;
            $notif->qty = 1;
            $notif->info = $event->info;
            $notif->status = 'new';
            $notif->save();
        }
    }
}
