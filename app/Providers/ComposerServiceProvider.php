<?php

namespace App\Providers;

use App\Notification;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
        View::composer('partials.admin-navbar', function ($view) {
            $notif = Notification::all()->take(5)->sortByDesc('created_at');
            //optimize collection methods
            $newNotif = Notification::where('status', 'new')->count();
            $newMessage = Notification::where('name', 'Message')->where('status', 'new')->count();
            $arr = ['notification' => $notif, 'newN' => $newNotif, 'newM' => $newMessage];
            //send to view
            // dd($arr['newM']);
            $view->with('notif', $notif);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
