<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Cornford\Backup\Facades\Backup;
use App\Exports\UsersExport;
use App\Exports\ContactExport;
use App\Exports\ProductExport;
use Maatwebsite\Excel\Facades\Excel;
use App\User;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['test']);
        $this->middleware('admin')->except(['profile']);
    }

    public function dashboard()
    {
        return view('pages.admin');
    }

    public function products()
    {
        return view('pages.products');
    }

    public function images()
    {
    	return view('pages.images');
    }

    public function profile()
    {
        return view('pages.profile');
    }

    public function docs()
    {
        return view('pages.docs');
    }

    public function downloads()
    {
        return view('pages.downloads');
    }

    public function database(Request $request) {
        $return_var = NULL;
        $output = NULL;
        $command = "C:/xampp/mysql/bin/mysqldump -u root -h localhost -p laravue > storage/db/photopro.sql";
        return $command;
        exec($command, $output, $return_var);

        if($return_var) {
            return $output;
        }

        $file = public_path(). "/storage/db/photopro.sql";
        return response()->download($file);
    }

    public function excel(Request $request) {
        // return $request->table;
        if($request->table == 'contacts') {
            return Excel::download(new ContactExport, 'ContactUs.xlsx');
        }
        else if($request->table == 'users') {
            return Excel::download(new UsersExport, 'Users.xlsx');
        }
        else if($request->table == 'products') {
            return Excel::download(new ProductExport, 'Products.xlsx');
        }
    }

    public function test(Request $request)
    {
        // $master = User::find(1);
        // $master->password = bcrypt('master');
        // if($master->save()) {
        //     return 'success!';
        // }

        $user = User::find(30);
            // dd($user['fname']);
        return view('emails.verifyUser', ['user' => $user]);
        // return $url = asset('storage/itsh.png');
        // return Image::make($url)->response();
        // return response()->file($url);

        //   	if(isset($request->email)) {
        //   		$user = User::find(1);
        //   		// return $user;
	    //    	$user->email = $request->email;
	    //    	$user->fname = $request->fname;
	    //    	if($user->save()) {
	    //    		return $request;
	    //    	}
		// }

        // $msg = new Message;
        // $msg->send("+639074239571", "oh yeah!");
        // $account_sid = 'AC418da46149b758eb8c6672a8220e74d5'; 
        // $auth_token = '2520d19d4c22e532585da4b5db3b34e2'; 
        
        // $client = new Client($account_sid, $auth_token); 
         
        // $messages = $client
        //   ->messages->create("+639074239571", array( 
        //         'From' => "+13304224368",  
        //         'Body' => "matan is love",      
        // ));
        // $messages = $client->api->v2010->accounts->create("+639074239571", array( 
        //         'From' => "+13304224368",  
        //         'Body' => "mata is love",      
        // ));

    	return 'What are you doing here?';
    }
}
