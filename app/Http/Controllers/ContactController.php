<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Events\Notify;
use App\Notification;
use App\Contact;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['create']);
    }
    
    public function index() {
        $new = Notification::where('name', 'Message')->where('status', 'new')->first();

        if($new) {
            $new->status = 'seen';
            $new->save();
        }
        
        return view('pages.contact');
    }

    public function entries(Request $request) {
        $string = $request->string ?? '';

    	return $messages = Contact::search($string)->paginate($request->paginate);
    }

    public function create(Request $request) {
        // return  response()->json(['status' => 'success', 'request' => $request, 'ip' => $request->ip()]);
        $contact = new Contact;
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->message = $request->message;
        (Auth::user()) ? $contact->ip = Auth::user()->role : $contact->ip = $request->ip();;
                
        // return  response()->json(['status' => 'success', 'request' => $request, 'ip' => $request->ip()]);
        ($contact->save()) ? $msg = 'success' : $msg = 'error';

        if($msg == 'success') {
            event(new Notify('Message', '/message'));
        }

        return  response()->json(['status' => $msg, 'ip' => $request->ip()]);
    }

    public function delete(Contact $contact) {
        if($contact->delete()) {
            $msg = 'success';
        }
        else {
            $msg = 'error';
        }
        return  response()->json(['status' => $msg]);
    }
}
