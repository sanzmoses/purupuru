<?php

namespace App\Http\Controllers;

use App\Image;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{

    public function index(Request $request) {
        // return $request;
		//setting up variables
    	$string = $request->string ?? '';
    	return $products = Image::orderBy('created_at', 'desc')->search($string)->paginate($request->paginate);
    }

 	public function import(Request $request)
    {
    	// return $request->all();
    	$error = false;
    	$msg = "success";
        
        if($request->hasFile('file')){

        	foreach($request->file('file') as $image)
            {
                $name = $image->getClientOriginalName();
                $filename = pathinfo($name, PATHINFO_FILENAME);

                $extension = $image->getClientOriginalExtension();
                $fileNameToStore = $filename;
                (getimagesize($image)[0] > getimagesize($image)[1])? $aspectRatio = 'wide' : $aspectRatio = 'tall';

                $img = new Image;
                $img->name = $fileNameToStore;
                $img->extension = $extension;
                $img->path = '/storage/products/';
                $img->aspect_ratio = $aspectRatio;

                if($img->save()) {
                	try {
                		$path = $image->move(public_path().'/storage/products', $name);
                        // Log::info($path);
                	} catch(\Throwable $e) {
                		// $img->delete();
                		$error = true;
                	}
                }
            }

            if($error) {
            	$msg = "error";
            }
            
            return response()->json(['status' => $msg]);
        }
        else {
        	return 'what the fuck are you doing here?';
        }
    }

    public function search(Request $request) {
        if($request->string == '') {
            return 0;
        }
        else if($request->string == 'all') {
            $request->string = '';
        }

        $string = $request->string;
    	return $images = Image::search($string)->where('product_id', 0)->get();
    }

    public function link(Request $request) {
        //get images id
        foreach($request->images as $img) {
            $img_id[] = $img['id'];
        }
        //get product id
        $product_id = $request->product['id'];
        //query for multiple update
        $update = Image::whereIn('id', $img_id)
        ->update([
            'product_id' => $product_id
        ]); 

        //set thumbnail
        Log::info($img_id);
        Image::where('id', $img_id[0])->update(['thumbnail' => 1]);
        
        ($update)? $msg = 'success' : $msg = 'error';
        return response()->json(['status' => $msg, 'product' => Product::with('images')->find($product_id)]);
    }

    public function unlink(Request $request) {
        $image = Image::find($request->id);
        $image->product_id = 0;
        if($image->save()) {
            return response()->json(['status' => 'success']);
        }
        
    }

    public function delete(Request $request) {
        $image = Image::find($request->id);
        unlink(public_path().'/storage/products/'.$image->name.'.'.$image->extension);
        if($image->delete()) {
            return response()->json(['status' => 'success']);
        }
    }
}
