<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use App\Apikey;
class ApikeyController extends Controller
{
    //for longer processing time of key
    //set here
    public $difficulty = 4;

    // 'id' => Uuid::uuid4(),
    public function __construct() {
        $this->middleware('auth');        
    }

    public function index() {
        return response()->json(['status' => 'success', 
        'apikey' => Auth::user()->apikey()->latest()->get()]);
    }

    public function create() {
        //set variables
        $difficulty = $this->difficulty;
        $user_id = Auth::user()->id;
        $apikeys = Auth::user()->apikey();
        $id = md5(Auth::user()->username.date("YmdHis").rand(1, 999));
        $key = Uuid::uuid4();
               
        // if id is duplicate, run another
        while (Apikey::where('id', $id)->count() >= 1) {
            $id = md5(Auth::user()->username.date("YmdHis").rand(1, 9999));
        }

        $key = $this->processUuid($key);

        //if all validation and requirement pass
        //check if there are already 5 key for the user
        if($apikeys->count() == 5) {
            $apikeys->oldest()->first()->delete();
        }

        $apikey = new Apikey;
        $apikey->id = $id.$user_id;
        $apikey->user_id = $user_id;
        $apikey->key = $key;
        $apikey->status = 1;
        
        if($apikey->save()) {
            return  response()->json(['status' => 'success']);
        }
        return  response()->json(['status' => 'success']);
    }

    public function processUuid($key) {
        //setting comparison for uuid processing
        $diff = $this->difficulty;

        $comparison = '';
        for($x = 0; $x < $diff; $x++) {
            $comparison = $comparison.'0';
        }

        // set uuid difficulty for longer processing
        while (substr($key, 0, $diff) !== $comparison) {
            $key = Uuid::uuid4();
        }

        return $key;
    }

    public function deactivate(Request $request) {
        $key = Apikey::find($request->id);
        $key->status = 0;
        if($key->save()) {
            return  response()->json(['status' => 'success']);
        }
        return  response()->json(['status' => 'error']);
    }

    public function delete(Request $request) {
        $key = Apikey::find($request->id);
        if($key->delete()) {
            return  response()->json(['status' => 'success']);
        }
        return  response()->json(['status' => 'error']);
    }

    public function test() {
        $id = Auth::user()->id;
        $difficulty = 4;
        $comparison = $id;
        $min = 1;

        for($x = 0; $x < $difficulty-1; $x++) {
            $comparison = $comparison.$id;
            $min = $min.'0';
        }

        echo rand($min, $comparison);

        echo $comparison;
        if(2345 == '2345') {
            echo 'wtf';
        } else {
            echo 'noob';
        }
        
    }
    
}
