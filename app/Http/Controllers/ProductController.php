<?php

namespace App\Http\Controllers;

use Excel;
use App\Image;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'categoricalList', 'categories']);
        $this->middleware('admin')->except(['index', 'categoricalList', 'categories']);
    }

    public function index(Request $request) {
		//setting up variables
    	$string = $request->string ?? '';
    	$brand = $request->brand;
    	$type = $request->type;
    	return $products = Product::search($string)->brand($brand)->type($type)->with('images')->paginate($request->paginate);
    }

    public function categoricalList(Request $request) {
        $categories = $this->categories();
        $list = [];

        foreach ($categories as &$c) {
            $p = Product::all()->where('type', $c)->pluck('brand');
            $p = $p->sort();
            // $p->values()->all();
            $unique = $p->unique();
            // $list[] = $unique->values()->all();
            $list[] = ['type' => $c, 'active' => false, 'brands' => $unique->values()->all()];
        }

        return $list;
    }

    public function categories() {
        $p = Product::all()->pluck('type');
        // return $p;
        $unique = $p->unique();
        return $unique->values()->all();
    }

    public function setThumbnail(Product $product, Request $request) {
        Image::where('id', $request->current)->update(['thumbnail' => 0]);
        Image::where('id', $request->new)->update(['thumbnail' => 1]);
        return $product->images;
    }

    public function create(Request $request) {
        Log::info($request->product);
        // $new = Product::create([$request->product]);
        $p = new Product;
        $p->name = $request->product['name'];
        $p->description = $request->product['description'];
        $p->brand = $request->product['brand'];
        $p->type = $request->product['type'];
        $p->price = 0;
        // $p->price = $request->product['price'];
        
        if($p->save()) {
            return response()->json(['status' => 'Success!']);
        }

        // return $new;
    }

    public function checkDuplicates(Request $request) {
        $field = $request->field;
        $input = $request->input;
        // return $request;
        return Product::where($field, $input)->count();

    }

    public function import(Request $request)
    {

        if($request->hasFile('file')){

            Excel::load($request->file('file')->getRealPath(), function ($reader) {

                foreach ($reader->toArray() as $key => $row) {
                    $data['name'] = $row['name'];
                    $data['description'] = $row['description'];
                    $data['brand'] = $row['brand'];
                    $data['type'] = $row['type'];
                    $data['price'] = $row['price'];

                    Log::info($data);
                }

                return response()->json(['msg' => 'success'], 200);
            });
        }

        return $data;
    }

    public function update(Product $product, Request $request) {
        $product->name = $request->product['name'];
        $product->brand = $request->product['brand'];
        // $product->price = $request->product['price'];
        $product->description = $request->product['description'];
        if($product->save()) {
            return response()->json(['status' => 'success']);
        }
    }

    public function delete(Product $product) {
        $s = 'unknown';
        $m = 'the hell are you doing here?';
        if($product->images->count() > 0) {
            $deletion = Image::where('product_id', $product->id)->delete(); 

            if($deletion) {
                if($product->delete()) {
                    $s = 'Successful';
                    $m = 'Good!';
                }
                else {
                    $s = 'Unsuccesful';
                    $m = 'Images deleted only!';
                }
            }
            $s = 'Image present';
            $m = 'Not deleted though!';
        }
        else {
            if($product->delete()) {
                $s = 'Success';
                $m = 'Deleted!';
            }
        }
        
        return response()->json(['status' => $s, 'message' => $m]);
    }
}
