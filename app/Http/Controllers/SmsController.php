<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Sms;

class SmsController extends Controller
{

    public function __construct() {
        $this->middleware('auth');       
    }

    public function index(Request $request) {
        //get user
        $logs = Auth::user()->sms();

        //setting up variables
        $string = $request->string ?? '';
        $by = $request->orderby ?? 'id';
        $in = $request->orderin ?? '';
        $arr = [$by, $in];

        $logs = $logs->search($string)->paginate($request->paginate);
        return $logs;
    }
}
