<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Numbers;

class NumbersController extends Controller
{
    
    public function __construct() {
        $this->middleware('auth');       
    }

    public function index() {
        return response()->json(['status' => 'success', 
        'numbers' => Auth::user()->registeredNumbers()->latest()->get()]);
    }

    public function create(Request $request) {
        $num = new Numbers;
        $num->number = '+63'.$request->number;
        $num->user_id = Auth::user()->id;
        $num->status = 1;
        if($num->save()) {
            return response()->json(['status'=>'success']);
        }

        return $request->number;
    }

    public function delete(Numbers $number) {
        if($number->delete()) {
            return response()->json(['status'=>'success']);
        }

        return $number;
    }
}
