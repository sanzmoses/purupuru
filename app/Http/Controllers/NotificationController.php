<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index() {
        $notif = Notification::all()->take(5);
        return $notif->count();
    }

    public function view(Notification $notif) {
        $notif->status = 'seen';
        if($notif->save()) {
            return redirect($notif->info);
        }
    }
}
