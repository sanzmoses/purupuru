<?php

namespace App\Http\Controllers;

use App\User;
use App\Contact;
use Carbon\Carbon;
use App\Events\Notify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //get user
        $user = Auth::user();

        //setting up variables
        $role = $request->role ?? 'all';
        $string = $request->string ?? '';
        $by = $request->orderby ?? 'id';
        $in = $request->orderin ?? '';
        $arr = [$by, $in];

        //checks for parameters
        if(!$user->isMaster() 
            && $request->role == 'all') {
            $role = 'adminOnly';
        }

        $users = User::role($role)->search($string)->inOrderBy($arr)->paginate($request->paginate);
        return $users;
    }

    public function info() {
        $now = Carbon::now()->month;
        $users = User::all();
        $members = $users->where('role', '!=', 'master');
        $date = $users->find($users[$users->count()-1]->id)->created_at;
        $usersThisMonth = User::where('role', 'member')->whereMonth('created_at', $now)->get()->count();
        $currentLogs = DB::table('sessions')->where('user_id', '!=', null)->count();
        $contactForm = Contact::all();
        $contactCount = $contactForm->count();

        ($date)? $date = $date->toFormattedDateString() : $date = 'No members yet';  
        ($contactCount > 0)? $contactDate = $contactForm->find($contactCount)->created_at->toDayDateTimeString(): $contactDate = 0;

        return response()->json([
            'count' => $members->count(), 
            'asOf' => $date, 
            'thisMonth' => $usersThisMonth,
            'month' => $now,
            'activeUsers' => $currentLogs,
            'contactForm' => $contactCount,
            'contactDate' => $contactDate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // return $request;
        $user = new User;
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->role = $request->role;
        $user->password = Hash::make($request->password);

        if($user->save()) {
            event(new Notify('User', '/admin'));
            return "success";
        }

        return "error";
    }

    public function checkDuplicates(Request $request) {
        $field = $request->field;
        $input = $request->input;

        if($request->update) {
            return User::where($field, $input)->where('id', '!=', Auth::user()->id)->count();
        }
        // return $request;
        return User::where($field, $input)->count();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return $user = Auth::user();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user = User::find($request->id);
        return $user;
        // return (string)(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $user = Auth::user();
        //check for old password
        if(!Hash::check($request->old, $user->password)) {
            return response()->json(['status' => 'error', 'msg' => 'Incorrect password!']);
        }
        //check for the new password and confirm
        if(!preg_match('/(?=.*[a-z])(?=.*[0-9]).{8,}/i', $request->new) 
            || $request->new != $request->confirm) {
            return response()->json(['status' => 'error', 'msg' => 'Stop messing around!']);
        }
        //hash password
        $user->password = Hash::make($request->new);

        //save password
        if($user->save()) {
            return response()->json(['status' => 'success', 'msg' => 'Successful!']);
        }
        else {
            return response()->json(['status' => 'error', 'msg' => 'DB error']);
        }
        
        // return $request->new;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = User::find($request->id);
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->email = $request->email;
        $user->username = $request->username;
        if($user->save()) {
            return "success";
        }

        return "error";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = User::find($request->id);
        // return $user;
        if($user->delete()) {
            return 'success';
        }
        return 'error';
    }
}
