<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function scopeSearch($query, $string) {
        return $query->where('name', 'like', '%'.$string.'%');
    }
    
}
