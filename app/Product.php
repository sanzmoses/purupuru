<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'brand', 'type', 'description', 'price', 'thumbnail',
    ];
    
    public function images()
    {
        return $this->hasMany('App\Image')->orderBy('thumbnail', 'desc');
    }

    public function thumbnail() 
    {
        return $this->images()->where('thumbnail', true)->first();
    }

    public function scopeSearch($query, $string) {
        return $query->where('name', 'like', '%'.$string.'%')
        ->orWhere('brand', 'like', '%'.$string.'%')
        ->orWhere('type', 'like', '%'.$string.'%');
    }

    public function scopeBrand($query, $brand) {
        if($brand == 'all') {
            return $query;
        }
        return $query->where('brand', $brand);
    }

    public function scopeType($query, $type) {
        if($type == 'all') {
            return $query;
        }
        return $query->where('type', $type);
    }
}
