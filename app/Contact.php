<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public function scopeSearch($query, $string) {
        return $query->where('name', 'like', '%'.$string.'%')
        ->orWhere('email', 'like', '%'.$string.'%')
        ->orWhere('phone', 'like', '%'.$string.'%');
    }
}
