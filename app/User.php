<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 'lname', 'email', 'username', 'role', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function verifyUser() {
        return $this->hasOne('App\VerifyUser');
    }

    public function sms() {
        return $this->hasMany('App\Sms');
    }

    public function registeredNumbers() {
        return $this->hasMany('App\Numbers');
    }

    public function apikey() {
        return $this->hasMany('App\Apikey');
    }

    public function activeKey() {
        return $this->hasMany('App\Apikey')->where('status', 1)->first();
    }

    public function isAdmin() {
        if($this->role == 'master' || $this->role == 'admin') {
            return true;
        }

        return false;
    }

    public function isMaster() {
        if($this->role == 'master') {
            return true;
        }

        return false;
    }

    public function scopeSearch($query, $string) {
        return $query->where('fname', 'like', '%'.$string.'%')
        ->orWhere('lname', 'like', '%'.$string.'%')
        ->orWhere('email', 'like', '%'.$string.'%')
        ->orWhere('username', 'like', '%'.$string.'%');
    }

    public function scopeRole($query, $role) {
        if($role == 'all') {
            return $query;
        }
        else if ($role == 'adminOnly') {
            return $query->where('role', '!=', 'master');
        }

        return $query->where('role', '=', $role);
    }

    public function scopeInOrderBy($query, $arr) {
        if($arr[1] == '') {
            return $query->orderBy($arr[0]);
        }

        return $query->orderBy($arr[0], $arr[1]);
    }

}
