<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->userName,
        'description' => $faker->bs,
        'brand' => $faker->randomElement($array = array ('Cannon','Brother','Epson')),
        'type' => $faker->randomElement($array = array ('Printer','Fax','Photocopy')),
        'price' => $faker->numberBetween($min = 2000, $max = 20000),
    ];
});
