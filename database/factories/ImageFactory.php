<?php

use Faker\Generator as Faker;

$factory->define(App\Image::class, function (Faker $faker) {
    return [
    	'product_id' => $faker->numberBetween($min = 1, $max = 30),
        'name' => $faker->word,
        'extension' => $faker->randomElement($array = array ('jpg','png','jpeg')),
    ];
});
