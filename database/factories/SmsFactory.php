<?php

use Faker\Generator as Faker;

$factory->define(App\Sms::class, function (Faker $faker) {
    return [
        'number' => '+'.$faker->numberBetween($min = 100, $max = 999),
        'message' => $faker->bs,
        'status' => $faker->randomElement($array = array ('Sent','Pending','Error')),
        'user_id' => 3,
    ];
});
