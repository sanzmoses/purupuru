let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.js('resources/assets/js/admin.js', 'public/js')
	.js('resources/assets/js/products.js', 'public/js')
	.js('resources/assets/js/user.js', 'public/js')
	.js('resources/assets/js/images.js', 'public/js')
	.js('resources/assets/js/contact.js', 'public/js')
	.js('resources/assets/js/profile.js', 'public/js')
	.js('resources/assets/js/docs.js', 'public/js')
	.js('resources/assets/js/dl.js', 'public/js')
	.js('resources/assets/js/dashboard.js', 'public/js')
	.js('resources/assets/js/numbers.js', 'public/js')
	.js('resources/assets/js/sms.js', 'public/js')
	.sass('resources/assets/sass/app.scss', 'public/css')
	.sass('resources/assets/sass/index.scss', 'public/css')
	.sass('resources/assets/sass/admin.scss', 'public/css')
	.sass('resources/assets/sass/user.scss', 'public/css')
