<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/about', 'HomeController@about');
Route::get('/services', 'HomeController@services');
Route::get('/contact', 'HomeController@contact');
Route::get('/user', 'HomeController@user');

Auth::routes();
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@dashboard');
Route::get('/profile', 'AdminController@profile');
Route::get('/docs', 'AdminController@docs');
Route::get('/downloadables', 'AdminController@downloads');
Route::get('/dashboard', 'MemberController@index');
Route::get('/numbers', 'MemberController@numbers');
Route::get('/sms', 'MemberController@sms');

Route::get('/user/all', 'UserController@index');
Route::get('/user/show', 'UserController@show');
Route::post('/user/delete', 'UserController@destroy');
Route::post('/user/update', 'UserController@update');
Route::post('/user/create', 'UserController@create');
Route::get('/user/checkDuplicates', 'UserController@checkDuplicates');
Route::get('/user/profile', 'UserController@profile');
Route::post('/user/changePassword', 'UserController@edit');
Route::get('/users/info', 'UserController@info');

Route::get('/products', 'AdminController@products');
Route::get('/products/list', 'ProductController@index');
Route::get('/products/categoricalList', 'ProductController@categoricalList');
Route::get('/products/categories', 'ProductController@categories');
Route::post('/products/import', 'ProductController@import');
Route::post('/product/create', 'ProductController@create');
Route::post('/product/delete/{product}', 'ProductController@delete');
Route::post('/product/update/{product}', 'ProductController@update');
Route::get('/product/checkDuplicates', 'ProductController@checkDuplicates');
Route::post('/product/setThumbnail/{product}', 'ProductController@setThumbnail');

Route::get('/images', 'AdminController@images');
Route::get('/images/list', 'ImageController@index');
Route::post('/images/import', 'ImageController@import');
Route::post('/image/delete', 'ImageController@delete');
Route::get('/image/search', 'ImageController@search');

Route::post('/product-images/link', 'ImageController@link');
Route::post('/product-image/unlink', 'ImageController@unlink');

Route::post('/contact/create', 'ContactController@create');

Route::get('/notif', 'NotificationController@index');
Route::get('/view/{notif}', 'NotificationController@view');

Route::get('/message', 'ContactController@index');
Route::get('/message/all', 'ContactController@entries');
Route::post('/message/delete/{contact}', 'ContactController@delete');

Route::get('/downloads/excel', 'AdminController@excel');
Route::get('/downloads/db', 'AdminController@database');

Route::get('/test', 'AdminController@test');

Route::get('/email/design', 'EmailController@index');
Route::get('/email/send', 'EmailController@send');

Route::post('/key/generate', 'ApikeyController@create');
Route::get('/key/user', 'ApikeyController@index');
Route::post('/key/delete', 'ApikeyController@delete');
Route::post('/key/deactivate', 'ApikeyController@deactivate');

Route::post('/number/register', 'NumbersController@create');
Route::get('/number/user', 'NumbersController@index');
Route::post('/number/delete/{number}', 'NumbersController@delete');

Route::get('/logs/user', 'SmsController@index');

Route::get('/key/test', 'ApikeyController@test');