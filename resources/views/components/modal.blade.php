<!-- Modal -->
<div class="modal fade" id="{{ $id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" @click="close($event)">
  <div class="modal-dialog modal-dialog-centered {{ $modalSize }}" role="document">
    <div class="modal-content">

      <transition
        name="custom-classes-transition"
        enter-active-class="animated fadeIn"
        leave-active-class="animated fadeOut">
        <div v-if="confirm.delete" class="info-sanz">
          <div class="info-box-sanz"> 
            <h3>Are you sure?</h3>
            <div v-if="loading" key=1 class="loading animated infinite flash">
             Processing <i class="fa fa-circle c-1"></i>
            </div>
            <div v-if="!loading" key=2>
              <button class="btn btn-danger btn-sm" @click="trashUser('true', 'yes')">Yes</button>
              <button class="btn btn-success btn-sm" @click="trashUser('true', 'no')">No</button>
            </div>
          </div>
        </div>
      </transition>

      <div class="modal-header">
        <h5 class="modal-title" id="title">{{ $title }}</h5>
        <button type="button" id="close-x" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" id="close-x">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{ $slot }}
      </div>
      <div class="modal-footer">
        {{ $footer }}
      </div>
    </div>
  </div>
</div>