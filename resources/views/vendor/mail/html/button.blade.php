
<a href="{{ $url }}" width="100%" class="button button-{{ $color ?? 'blue' }}" target="_blank">{{ $slot }}</a>

