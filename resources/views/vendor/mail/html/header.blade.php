<tr class="header-whole">
    <td class="header">
        <a href="{{ $url }}">
            {{ $slot }}
        </a>
    </td>
</tr>
