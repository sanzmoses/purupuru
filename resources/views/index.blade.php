@extends ('layouts.master')

@section ('content')
<div class="container" :class="{'mt-6' : fixed}">
  <div class="row mt-5">

      <div class="col-lg-10 offset-lg-1 col-md-12">
        <div class="row">
          <div class="col-lg-6 d-flex justify-content-center justify-content-lg-end justify-content-xl-end pl-3">
              <img class="img-fluid front-logo" src="{{ asset('files/images/home/logo-nc.png') }}" alt="PuruPuru Logo">
          </div>
          <div class="col-lg-6 text-center text-lg-left">
              <h1 class="text-white mt-5" title="PuruPuru" alt="PuruPuru">PuruPuru</h1>
              <h4 class="text-white">
                  A Simple <span class="badge badge-info">SMS API</span> for learning, testing and other bullshit you can think of.
              </h4>
              <br>
              <button class="btn btn-theme btn-lg text-xlg">
                Let's get started! <i class="fa fa-paper-plane ml-2"></i>
              </button>
          </div>
        </div>
        <hr class="mt-5">
        
      </div>

      <div class="col-lg-10 offset-lg-1 col-md-12 pb-5">
        <div class="row text-center">
          <div class="col-md-4">
            <div class="card text-white bg-dark mb-3 mt-2">
              <div class="card-header">
                  <h4 class="thin mb-0"> <b> <i class="fa fa-thumbs-up mr-2"></i>  Simple and Easy</b> </h4>
              </div>
              <div class="card-body text-justify pb-1">
                  <p>No need to read a huge chunk of documentation. Take a sip. Just Ctrl+c and Ctrl+v. Its that simple! Piece of cake.</p>
              </div>
            </div>
            
          </div>
          <div class="col-md-4">
              
              <div class="card text-white bg-dark mb-3 mt-2">
                <div class="card-header">
                    <h4 class="thin mb-0"> <b> <i class="fa fa-magic mr-2"></i> Smooth and Seamless </b> </h4>
                </div>
                <div class="card-body text-justify pb-1">
                    <p>Your idea will be ready with just a few lines of code. Smooth integration in any application. Like a walk in the park.</p>
                </div>
              </div>
          </div>
          <div class="col-md-4">
              <div class="card text-white bg-dark mb-3 mt-2">
                <div class="card-header">
                    <h4 class="thin mb-0"> <b> <i class="fa fa-check-circle mr-2"></i> Absolutely Free</b> </h4>
                </div>
                <div class="card-body text-justify pb-1">
                    <p>No cost. Just register and get your key. Copy some codes you dont need to understand and BWALA, youre good to go.</p>
                </div>
              </div>
              
          </div>
        </div>
      </div>
  </div>
  <!-- /.row -->
</div>
<div class="container-fluid">
  <div class="row bg-white code-section">
      <div class="col-lg-8 offset-lg-2 col-md-12 pt-5 pb-5">
          {{-- <img src="{{ asset('images/home/logo-nc3.png') }}" style="height: 4em; margin-top: -5.5em;" 
          class="rounded mx-auto d-block" alt="purupuru logo" title="purupuru"> --}}
          <div class="text-center mt-3">
            <h1 class="theme-color mb-4">SEAMLESS INTEGRATION</h1>
          </div>
          <div class="card text-center">
              <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                  <li class="nav-item">
                    <a class="nav-link active" href="#">AJAX</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">CURL</a>
                  </li>
                </ul>
              </div>
              <div class="card-body p-0">
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-md p-0">
                        <pre class="m-0">
                            <code class="language-javascript">
    axios.post('/contact/create', {
      credentials: {
        username: sanzmoses,
        key: i789afsjpasd7f763iIAUSd789auK24Tty,
      },
      number: 63906948595,
      message: 'Hello World!'
    }).then(response => {
      // console.log(response);
    }).catch(e => {
      console.log(e);
    })
                            </code>
                          </pre>
                    </div>
                    <div class="col-md p-0">
                        <div class="p-5 text-left">
                          <p>Let's write your first <span class="badge bg-theme" style="font-size: 1.2em;">PuruPuru</span> Code</p>

                          <p>Copy and paste this code in your own file and put your number and change the message <span class="badge badge-warning" style="font-size: 1em;">limited of 140 characters</span> in the code. Now try running it.</p>

                          <p> <span class="text-success"> Awesome! </span> Now check your phone!</p>
                        </div>
                    </div>
                  </div>
                </div>
                  
              </div>
            </div>
        
      </div>
      <div class="col-md-12 col-lg-12 bg-theme-dark">
          <div class="row mb-5 contact-us-section">
              <div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2 text-center">
                      <h1 class="font-weight-bold mt-5">Contact Us</h1>
                      <hr>
                  <form class="mt-4" v-on:submit.prevent="validateContact()">
                      <div class="row">
                          <div class="col-xs-12 col-sm-6">
                              <input v-model="name" type="text" class="form-control mb-3" required placeholder="Name (required)">
                              <input v-model="email" type="email" class="form-control mb-3" required placeholder="Email (required)">
                              <input v-model="phone" type="number" class="form-control mb-3" placeholder="Phone Number (optional)">
                          </div>
                          <div class="col-xs-12 col-sm-6">
                              <div class="form-group">
                                  <textarea class="form-control" placeholder="Message (required)" required v-model="message" id="exampleFormControlTextarea1" rows="3"></textarea>
                              </div>
                              <button v-if="!loads.contact" class="btn btn-primary btn-block btn-theme">Submit &nbsp <i class="fa fa-send"></i></button>
                              <button v-cloak v-else class="btn btn-primary btn-blockbtn-theme" disabled>Sending &nbsp <i class="fa fa-circle-o-notch fa-spin"></i></button>
                          </div>
                      </div>
                  </form>
                  <hr>
                  <p class="text-white">You can either contact us through the form above or connect with us on <i class="fa fa-facebook"></i>acebook . Links below. &nbsp <i class="fa fa-hand-o-down"></i> </p>
              </div>
          </div>
      </div>
  </div>
  
</div>


@endsection