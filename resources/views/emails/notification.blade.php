<html>
    <style>
        
    </style>
    <body>
           
        <table align="center" style="margin-left: auto; margin-right: auto; display: block; width: 1000px; font-family: Segoe UI; font-weight: lighter; border: none;" cellspacing="0" cellpadding="0">
            <tr>
                <!-- <td colspan="3" style="background-color: #000; background: url('http://www.caseddimensions.com/2016_assets/images/Banner-image.jpg'); background-position: 50%; height: 200px; width: 100%;height: 200px; width: 100%;">                    
                    <h1 style="text-align: center; color: #fff; font-size: 45px; margin-top: 20px;">Be ahead of the curve</h1>             
                </td> -->
                <td colspan="3" style="background-color: #000; background-position: 50%; height: 200px; overflow-x: hidden!important;">                    

                        <table cellpadding="0" cellspacing="0" border="0" height="300" width="1000px">
                                <tr>
                                  <td background="http://www.caseddimensions.com/2016_assets/images/Banner-image-h300-w1000.jpg" bgcolor="#FFFFFF" valign="top" style="overflow: hidden!important;">
                                    <!--[if gte mso 9]>
                                    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:1000px; margin:0 auto; height:300px; display: block; margin: 0 auto;">
                                      <v:fill type="tile" src="http://www.caseddimensions.com/2016_assets/images/Banner-image-h300-w1000.jpg" color="#000000" />
                                      <v:textbox inset="0,0,0,0">
                                    <![endif]-->
                                        <h1 style="text-align: center; color: #fff; font-size: 45px; margin-top: 100px;">Be ahead of the curve</h1>
                                    <!--[if gte mso 9]>
                                        </v:textbox>
                                    </v:rect>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                </td>
            </tr>
            <tr style="color: #FFF;">
                <td style="background-color: #1c9fd7; padding: 30px; width: 33%;">
                    <h2 style="font-size: 30px;">Azure Services</h2>
                    <p style="color: #FFF; font-size: 20px;">Find out how Cased Dimensions can hep your business adopt Azure Services</p>
                    <br><a href="/#" style="color: #FFF; font-size: 22px;">Learn more >></a>
                </td>
                <td style="background-color: #f5705f; padding: 30px; width: 33%;">
                    <h2 style="font-size: 30px;">Asset Management</h2>
                    <p style="color: #FFF; font-size: 20px;">Eliminate waste of underused licenses and improved IT governance</p>
                    <br><a href="/#" style="color: #FFF; font-size: 22px;">Learn more >></a>
                </td>
                <td style="background-color: #774c91; padding: 30px; width: 33%;">
                    <h2 style="font-size: 30px;">SCSM App Store</h2>
                    <p style="color: #FFF; font-size: 20px;">Visit world's most comprehensive SCSM app store</p>
                    <br><a href="/#" style="color: #FFF; font-size: 22px;">Learn more >></a>
                </td>
            </tr>
        </table>
        <table align="center" style="width: 1000px; font-family: Segoe UI; font-weight: lighter; border: none;" cellspacing="0" cellpadding="0">
            <tr>
                <td style="padding: 25px; padding-top: 0px; padding-bottom: 0px; width: 50%;">
                    <h3 style="font-size: 40px;">At Cased Dimensions, our mission is to enable you to do business better</h3>
                    <p style="font-size: 20px;">With our expert people and market leading services, we help you to get more from your technology.</p>
                </td>
                <td style="background-color: #000; background-position: 50%; height: 500px; overflow-x: hidden!important;">                    
                        <table cellpadding="0" cellspacing="0" border="0" height="500" width="500px">
                            <tr>
                                <td background="http://www.caseddimensions.com/2016_assets/images/mgt-w500.jpg" bgcolor="#FFFFFF">
                                    <!--[if gte mso 9]>
                                    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:500px; margin:0 auto; height:500px; display: block; margin: 0 auto;">
                                    <v:fill type="tile" src="http://www.caseddimensions.com/2016_assets/images/mgt-w500.jpg" color="#000000" />
                                    <v:textbox inset="0,0,0,0">
                                    <![endif]-->
                                    <!--[if gte mso 9]>
                                        </v:textbox>
                                    </v:rect>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                </td>         
            </tr>
            <tr>
                <td style="background-color: #000; background-position: 50%; height: 500px; overflow-x: hidden!important;">                    
                        <table cellpadding="0" cellspacing="0" border="0" height="500" width="500px">
                            <tr>
                                <td background="http://www.caseddimensions.com/2016_assets/images/mgt-svc-w500.jpg" bgcolor="#FFFFFF">
                                    <!--[if gte mso 9]>
                                    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:500px; margin:0 auto; height:500px; display: block; margin: 0 auto;">
                                    <v:fill type="tile" src="http://www.caseddimensions.com/2016_assets/images/mgt-svc-w500.jpg" color="#000000" />
                                    <v:textbox inset="0,0,0,0">
                                    <![endif]-->
                                    <!--[if gte mso 9]>
                                        </v:textbox>
                                    </v:rect>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                </td>
                <td style="padding: 25px; padding-top: 0px; padding-bottom: 0px; width: 50%;">
                    <h3 style="font-size: 40px;">Security is engrained in what we do</h3>
                    <p style="font-size: 20px;">We also deliver CESG compliant secure services for UK Government.</p>
                </td>         
            </tr>
            <tr>
                <td style="padding: 25px; padding-top: 0px; padding-bottom: 0px; width: 50%;">
                    <h3 style="font-size: 40px;">We are an Alliance Partner with Microsoft</h3>
                    <p style="font-size: 20px;">We help clients deliver efficiencies using Microsoft Technologies including Service Manager, System Center and Azure.</p>
                </td>
                <td style="background-color: #000; background-position: 50%; height: 500px; overflow-x: hidden!important;">                    
                        <table cellpadding="0" cellspacing="0" border="0" height="500" width="500px">
                            <tr>
                                <td background="http://www.caseddimensions.com/2016_assets/images/svc-w500.jpg" bgcolor="#FFFFFF">
                                    <!--[if gte mso 9]>
                                    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:500px; margin:0 auto; height:500px; display: block; margin: 0 auto;">
                                    <v:fill type="tile" src="http://www.caseddimensions.com/2016_assets/images/svc-w500.jpg" color="#000000" />
                                    <v:textbox inset="0,0,0,0">
                                    <![endif]-->
                                    <!--[if gte mso 9]>
                                        </v:textbox>
                                    </v:rect>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                </td>         
            </tr>
        </table>
        <table align="center" style="margin-left: auto; margin-right: auto; display: block; width: 1000px; font-family: Segoe UI; font-weight: lighter; border: none;" cellspacing="0" cellpadding="0">

            <tr width="1000px" >
            	<td width="250px" style="padding: 20px 0px"></td>
                <td width="250px" style="padding: 20px 0px"><img style="height: 200px" src="http://www.caseddimensions.com/2016_assets/images/support.png" alt=""></td>
                <td width="250px" style="padding: 20px 0px">
                    <p style="line-height: 20px; padding-left: 20px;">Need help with SCSM? <br> We have the help you need!</p>
                    <button style="width: 100%; display: block; padding: 20px 20px; background-color: #007bff; color: white; border: 0px;">Contact Us</button>
                </td>
                <td width="250px" style="padding: 20px 0px"></td>
            </tr>
            <tr width="1000px" style="background-color: #4e4e4e; color: white; font-size: 12px;">
            	<td colspan="3" style="padding: 20px 20px">
            		<p>COPYRIGHT 2018 WRITTEN BY CASED DIMENSIONS LTD. ALL RIGHTS RESERVED. PRIVACY POLICY</p>
            	</td>
            	<td colspan="1" align="right" style="padding: 20px 20px">
            		<p>INFO@CASEDDIMENSIONS.COM</p>
            	</td>
            </tr>
        </table>
    </body>
</html>