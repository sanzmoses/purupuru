<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
    <link href="https://fonts.googleapis.com/css?family=Major+Mono+Display|Open+Sans" rel="stylesheet">
</head>
<body>
<div style="width: 600px; padding: 50px 50px; background-color: #EEEEEE; font-family: system-ui;">
    <div style="text-align: center">
        <img alt="Logo" title="Logo" style="display:block; margin: 0 auto;" width="150" height="150" 
            src="https://img.icons8.com/color/1600/circled-envelope.png" alt="">    
        <h1 style="color: #2e54bb; font-size: 40px; margin: 5px 0px;">Welcome to Photopro</h1>
    </div>
    
    <div style="background-color: white;padding: 50px 50px; color: black; font-size: 20px; border: 1px solid #EEEEE">
        <p>Hi {{$user['fname']}} !</p>
        
        <p>Thank you for signing up!</p>
        <p>
            Your registered email-id is {{$user['email']}} , Please click on the below link to verify your email account.
        </p>
        <br>
        <a style="text-decoration: none; background-color: #2e54bb; color: white;padding: 15px 40px; border-radius: 5px;"
href="{{url('user/verify', $user->verifyUser->token)}}"> <b> Click here to verify </b> </a>
    </div>
    <div style="text-align: center">
        <a href="https://www.photoprotrading.com">Visit our website</a> <br>
        <a href="https://www.facebook.com/PHOTOPROCOPIER/">or visit our facebook page</a>
    </div>
    <br/>
</div>
</body>
</html>