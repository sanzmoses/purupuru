@extends('layouts.auth')

@section('content')
<div class="container auth reset-form">
    <div class="row justify-content-center">
        <div class="col-md-5 col-sm-8">
                
            <div class="text-center">
                <h2><i class="fa fa-lock mr-2" aria-hidden="true"></i>Reset Password</h2>
            </div>

            <div class="card">

                <div class="card-body">
                    <form method="POST" action="{{ route('password.request') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <label for="email" class="col-form-label">{{ __('E-Mail Address') }}</label>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <label for="password" class="col-form-label">{{ __('Password') }}</label>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <label for="password-confirm" class="col-form-label">{{ __('Confirm Password') }}</label>
                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
