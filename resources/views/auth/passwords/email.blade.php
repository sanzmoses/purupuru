@extends('layouts.auth')

@section('content')
<div class="container auth">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="text-center">
                <h2><i class="fa fa-unlock-alt" aria-hidden="true"></i> &nbsp Reset Password</h2>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card">
                <div class="card-body">

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        
                        <label for="email" class="col-form-label">{{ __('E-Mail Address') }}</label>
                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
