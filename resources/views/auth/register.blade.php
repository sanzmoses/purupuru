@extends('layouts.auth')

@section('content')
<div class="container auth registration-form">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="text-center">
                <h2>Register to PuruPuru</h2>
            </div>
            <div class="card mb-5">
                {{-- <div class="card-header">
                    <i class="fa fa-address-book" aria-hidden="true"></i> &nbsp <b>Register</b> 
                </div> --}}

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <input type="hidden" type="text" name="role" value="member">

                        {{-- <div class="form-group row">
                            
                            <div class="col-md-12">
                                <label for="fname" class=" col-md-12 col-form-label  ">{{ __('First Name') }}</label>
                                <input id="fname" type="text" class="form-control{{ $errors->has('fname') ? ' is-invalid' : '' }}" name="fname" value="{{ old('fname') }}" placeholder="Ex. Juan Pedro" required autofocus>

                                @if ($errors->has('fname'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> --}}

                        {{-- <div class="form-group row">
                            
                            <div class="col-md-12">
                                <label for="lname" class=" col-md-12 col-form-label  ">{{ __('Last Name') }}</label>
                                <input id="lname" type="text" class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}" name="lname" value="{{ old('lname') }}" placeholder="Ex. Dela Cruz" required autofocus>

                                @if ($errors->has('lname'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> --}}

                        <div class="form-group row">
                            
                            <div class="col-md-12">
                                <label for="email" class=" col-md-12 col-form-label  ">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Ex. juandelacruz@gmail.com" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            
                            <div class="col-md-12">
                                <label for="username" class=" col-md-12 col-form-label  ">{{ __('Username') }}</label>
                                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" placeholder="Only alphanumeric characters" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            
                            <div class="col-md-12">
                                <label for="password" class=" col-md-12 col-form-label  ">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Must be at least 8 minimum characters" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            
                            <div class="col-md-12">
                                <label for="password-confirm" class=" col-md-12 col-form-label  ">{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="type your password again" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block p-3">
                                    Sign up for Photopro
                                </button>
                            </div>
                        </div>
                        <div class="text-center mt-3 p-2">
                            <small><p>By clicking "Sign up for Photopro" button, you agree to our <a href="#"> terms of service and privacy statement </a>. We'll occassionally send you account related emails.</p></small>
                        </div>
                    </form>
                </div>
                <div class="card-footer">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
