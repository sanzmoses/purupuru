@extends('layouts.auth')

@section('content')
<div class="container auth login-form">
    <div class="row">
        <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 justify-content-center mt-1">
            <img src="{{ asset('files/images/home/logo-nc.png') }}" 
            {{-- src="http://placehold.it/100x100" --}}
            class="img-fluid p-logo-2 mb-5">
            <div class="text-center">
                <h2>Sign in to PuruPuru</h2>
            </div>

            @if (session('status'))
                <div class="alert alert-success">
                    <small>{{ session('status') }}</small>
                </div>
            @endif
            @if (session('warning'))
                <div class="alert alert-warning">
                        <small>{{ session('warning') }}</small>
                </div>
            @endif

            <div class="card mt-3">
                {{-- <div class="card-header">
                    <i class="fa fa-user-circle" aria-hidden="true"></i> &nbsp <b>Login</b>
                </div> --}}

                <div class="card-body pb-1">

                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row mb-0">
                            
                            <div class="col-md-12">
                                <label for="username" class="col-md-12 col-form-label">{{ __('Username') }}</label>
                                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            
                            <div class="col-md-12">
                                <label for="password" class="float-left col-form-label">{{ __('Password') }}
                                </label>
                                
                                <a class="btn btn-link btn-sm float-right mt-1" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                        
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-block">
                                    Sign in &nbsp<i class="fa fa-sign-in" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-footer text-center">
                    {{-- <div class="g-signin2" data-onsuccess="onSignIn"></div> --}}
                    <small>No account? <a href="/register">Create an account</a></small>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
