<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include ('partials.admin-header')
<body>
	<div id="admin">
		
		@include ('partials.admin-navbar')
		@include ('partials.progress-bar')
		<div class="container-fluid screen-height">
			<div class="row screen-height">
				
				@include ('partials.sidebar-mobile')
				
				<div class="col-lg-2 d-none d-lg-block sidebar">
					@include ('partials.sidebar')
				</div>
				<div class="col-sm-12 col-md-12 col-lg-10 offset-lg-2">
					{{-- <div id="load" class="loader-wrapper">
						<div class="loader"></div>
					</div> --}}
					
					<main class="py-4">
			            @yield('content')
			        </main>
			        
			        @include ('partials.notify')
				</div>
			</div>
		</div>    
    </div>
    <!-- Scripts -->
    @yield ('script')
</body>
</html>
