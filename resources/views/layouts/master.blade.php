<!DOCTYPE html>
<html lang="en">

	@include ('partials.header')
	
	<body>		
		<div id="app" class="vessel">	
			@include ('partials.navbar')
			@yield ('content')
			@include ('partials.notify')
		</div>

		@include ('partials.footer')
		@include ('partials.scripts')
		@yield ('i-scripts')

	</body>

</html>