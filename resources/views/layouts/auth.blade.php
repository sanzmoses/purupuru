<!DOCTYPE html>
<html lang="en">

	@include ('partials.header')
	
	<body>		
		<div id="app" class="auth-bg">	
			@include ('partials.navbar')
			@yield ('content')
		</div>

		@include ('partials.footer')
		@include ('partials.scripts')
		@yield ('i-scripts')

	</body>

</html>