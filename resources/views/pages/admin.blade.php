@extends('layouts.app')

@section('content')
  <div class="container-fluid">
    <div class="alert alert-primary alert-dismissible fade show" role="alert">
      <strong> <i class="fa fa-info-circle"></i> &nbsp Hello there {{ Auth::user()->fname }} !</strong> 
      You can check on the docs for more information in the tab on the left.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>

    <div class="row mb-4">
      <div class="col-md-3">
        <div class="card info-card">
          <div class="card-body">
            <h5 v-cloak class="float-right count">  @{{ info.count || 0 }} </h5>
            <h5 v-if="loads.general" class="float-right count"> 0 </h5>
            <p class="card-text mb-0"><b>Total Users</b></p>
            <small v-cloak class="mt-0"> @{{ info.asOf }} </small>
            <small v-if="loads.general" class="mt-0"> Date </small>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card info-card">
          <div class="card-body">
            <h5 v-cloak class="float-right count"> @{{ info.thisMonth }} </h5>
            <h5 v-if="loads.general" class="float-right count"> 0 </h5>
            <p class="card-text mb-0"><b> New Members </b></p>
            <small v-cloak class="text-sm"> @{{ info.month | monthName }} </small>
            <small v-if="loads.general" class="text-sm"> Month </small>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card info-card">
          <div class="card-body">
            <h5 v-cloak class="float-right count"> @{{ info.contactForm }} </h5>
            <h5 v-if="loads.general" class="float-right count"> 0 </h5>
            <p class="card-text mb-0"><b> Contact Form </b></p>
            <small v-cloak class="text-sm"> @{{ info.contactDate || 'No entries' }} </small>
            <small v-if="loads.general" class="text-sm"> Timezone and Datetime </small>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card info-card">
          <div class="card-body">
            <h5 v-cloak class="float-right count"> @{{ info.activeUsers }} </h5>
            <h5 v-if="loads.general" class="float-right count"> 0 </h5>
            <p class="card-text mb-0"><b> Active Users </b></p>
            <small v-cloak class="text-sm"> @{{ dateNow | formatTime }} </small>
            <small v-if="loads.general" class="text-sm"> Time </small>
          </div>
        </div>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header"> <b>Users</b> &nbsp<span><button @click="openModal('addUser')" class="btn btn-default btn-sm">Add &nbsp<i class="fa fa-plus"></i></button></span></div>

          <div class="card-body">
            <div class="row">

              <div class="col-xs-12 col-sm-12 col-md-4 order-md-2">
                <ul class="nav justify-content-end">
                  <li class="nav-item full-width">
                    {{-- <input v-model="search" type="text" class="form-control" placeholder="search"> --}}
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i v-if="loads.search" class="fa fa-search"></i>
                          <i v-if="!loads.search" v-cloak class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></span>
                      </div>
                      <input v-model="search" type="text" class="form-control form-control-sm" placeholder="Search by Name, Username or Email">
                    </div>
                  </li>

                </ul> 
              </div>

              <div class="col-sm-12 col-md-8 order-md-1">
                <ul class="nav nav-tabs">
                  <li class="nav-item">
                    <div v-if="loads.general" class="nav-link"><div class="bar bar-sm"></div></div>
                    <a v-cloak v-bind:class="[navclass, {active: isActive.all}]" @click="setActive('all')">All</a>
                  </li>
                  @if (Auth::user()->isMaster())
                  <li class="nav-item">
                    <div v-if="loads.general" class="nav-link"><div class="bar bar-sm"></div></div>
                    <a v-cloak v-bind:class="[navclass, {active: isActive.master}]" @click="setActive('master')">Masters</a>
                  </li>
                  @endif
                  <li class="nav-item">
                    <div v-if="loads.general" class="nav-link"><div class="bar bar-sm"></div></div>
                    <a v-cloak v-bind:class="[navclass, {active: isActive.admin}]" @click="setActive('admin')">Admins</a>
                  </li>
                  <li class="nav-item">
                    <div v-if="loads.general" class="nav-link"><div class="bar bar-sm"></div></div>
                    <a v-cloak v-bind:class="[navclass, {active: isActive.member}]" @click="setActive('member')">Members</a>
                  </li>
                </ul>
              </div>
              
            </div>
            @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
            @endif

            <div class="row"> 
              <div class="col-md-12 col-sm-12 table-responsive"> 
                <table class="table table-hover">
                  <thead class="thead bg-theme">

                    <tr>
                      <th scope="col" @click="inOrder()"># &nbsp<i class="fa" v-bind:class="[{'fa-caret-down': order.in, 'fa-caret-up': !order.in}]"></i></th>
                      <th scope="col">First</th>
                      <th scope="col">Last</th>
                      <th scope="col">Username</th>
                      <th scope="col">Email</th>
                      <th scope="col">Status</th>
                      <th scope="col">Role</th>
                    </tr>

                  </thead>
                  <tbody>
                    
                    <tr v-if="loads.general">
                      <th> <div class="bar"></div> </th>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                    </tr>
                    
                    <tr v-cloak class="trow" v-for="user in users" @click="showUser(user.id)">
                      <th scope="row">@{{ user.id }}</th>
                      <td>@{{ user.fname }}</td>
                      <td>@{{ user.lname }}</td>
                      <td>@{{ user.username }}</td>
                      <td>@{{ user.email }}</td>
                      <td>
                        <p v-if="user.role != 'member'">N/A</p>
                        <div v-else>
                          <p v-if="user.role == 'member' && user.verified" class="text-success">
                            <i class="fa fa-check-circle mr-2"></i> Verified
                          </p> 
                          <p v-else class="text-danger">
                            <i class="fa fa-times-circle mr-2"></i> Unverified
                          </p> 
                        </div>
                      </td>
                      <td>@{{ user.role }}</td>
                    </tr>

                  </tbody>
                </table>
              </div>
            </div>

            <nav class="row mt-3">
              <div class="col-md-4 col-sm-4"> 
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Entries</label>
                  </div>
                  <select v-model="perpage" class="custom-select" id="inputGroupSelect01">
                    <option selected>10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option value="40">40</option>
                    <option value="50">50</option>
                  </select>
                </div>

              </div>
              <div class="col-md-8 col-sm-8"> 
                <ul class="pagination justify-content-end">

                  <li class="page-item" v-bind:class="[{disabled: back}]">
                    <a class="page-link" aria-label="Previous" @click="skip(-group.divisor)">
                      <i class="fa fa-angle-double-left"></i>
                    </a>
                  </li>
                  
                  <li class="page-item" v-bind:class="[{disabled: prev}]">
                    <a class="page-link" aria-label="Previous" @click="step(-1)">
                      <i class="fa fa-caret-left"></i>
                    </a>
                  </li>
                  
                  <li class="page-item" v-cloak v-bind:class="[{active: pages.active}]" v-for="pages in paginateArr"
                  @click="fetch(pages.num)"><a class="page-link">@{{ pages.num }}</a></li>

                  <li class="page-item" v-if="loads.general"><a class="page-link"><div style="width: 17px" class="bar bar-xs"></div></a></li>

                  <li class="page-item" v-bind:class="[{disabled: next}]">
                    <a class="page-link" aria-label="Next" @click="step(1)">
                      <i class="fa fa-caret-right"></i>
                    </a>
                  </li>

                  <li class="page-item" v-bind:class="[{disabled: forward}]">
                    <a class="page-link" aria-label="Next" @click="skip(group.divisor)">
                      <i class="fa fa-angle-double-right"></i>
                    </a>
                  </li>

                </ul>
              </div>
            </nav>

          </div>
        </div>
      </div>
    </div>
  </div>

@modal()
  @slot('id')
    showUser
  @endslot

  @slot('modalSize')
     none
  @endslot

  @slot('title')
    View User # @{{ viewUser.id }}
  @endslot
  {{-- body --}}
    <table class="table table-sm">
      <h2 class="">
        Access: @{{ viewUser.role }}
        <span v-if="viewUser.role == 'member' && viewUser.verified" class="text-success"> 
          <i class="fa fa-check-circle "></i> 
        </span>
        <span v-else class="text-danger"> 
          <i class="fa fa-times-circle "></i> 
        </span>
      </h2>
      <tbody>

        <tr>
          <th>Name: </th>
          <td colspan="2" v-if="!foo.name">@{{ fullName }}</td>
          <td colspan="2" v-if="foo.name">
              <div class="form-row">
                <div class="col">
                  <input v-model="viewUser.first" type="text" class="form-control form-control-sm" placeholder="First name">
                </div>
                <div class="col">
                  <input v-model="viewUser.last" type="text" class="form-control form-control-sm" placeholder="Last name">
                </div>
              </div>
          </td>
          <td v-if="!foo.name">
            <span class="btn badge badge-info" @click="change('edit', 'name')">
              <i class="fa fa-edit"></i>
            </span>
          </td>
          <td v-if="foo.name">
            <span class="btn badge badge-success" @click="change('save', 'name')">
              <i class="fa fa-check"></i>
            </span>
          </td>
        </tr>

        <tr>
          <th>Email: </th>
          <td colspan="2" v-if="!foo.email">@{{ viewUser.email }}</td>
          <td colspan="2" v-if="foo.email">
              <div class="form-row">
                <div class="col">
                  <input v-model="viewUser.email" type="text" class="form-control form-control-sm" placeholder="First name">
                </div>
              </div>
          </td>
          <td v-if="!foo.email">
            <span class="btn badge badge-info" @click="change('edit', 'email')">
              <i class="fa fa-edit"></i>
            </span>
          </td>
          <td v-if="foo.email">
            <span class="btn badge badge-success" @click="change('save', 'email')">
              <i class="fa fa-check"></i>
            </span>
          </td>
        </tr>

        <tr>
          <th>Username: </th>
          <td colspan="2" v-if="!foo.username">@{{ viewUser.user }}</td>
          <td colspan="2" v-if="foo.username">
              <div class="form-row">
                <div class="col">
                  <input v-model="viewUser.user" type="text" class="form-control form-control-sm" placeholder="First name">
                </div>
              </div>
          </td>
          <td v-if="!foo.username">
            <span class="btn badge badge-info" @click="change('edit', 'username')">
              <i class="fa fa-edit"></i>
            </span>
          </td>
          <td v-if="foo.username">
            <span class="btn badge badge-success" @click="change('save', 'username')">
              <i class="fa fa-check"></i>
            </span>
          </td>
        </tr>
      </tbody>
    </table>
  {{-- end body --}}
  @slot('footer')
    <button @click="trashUser(false, false)" data-toggle="tooltip" data-placement="top" title="Warning: Delete this User?" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
    <button type="button" class="btn btn-primary btn-sm" @click="updateUser()">
      <span v-if="!confirm.update">Save Changes</span>
      <span v-if="confirm.update">Please wait... <i key=1 class="fa fa-circle c-1 loading animated infinite flash"></i></span>
    </button>
  @endslot
@endmodal

@modal()
  @slot('id')
    addUser
  @endslot

  @slot('modalSize')
      none
  @endslot

  @slot('title')
    Register User &nbsp <i class="fa fa-user-plus"></i>
  @endslot
  {{-- body --}}
    <table class="table table-sm">

      <tbody>
        <tr>
          <th>Name: </th>
          <td colspan="2">
              <div class="form-row">
                <div class="col">
                  <input v-model="newUser.first" type="text" class="form-control form-control-sm" v-bind:class="[{'is-invalid': validation.first.invalid, 'is-valid': validation.first.valid}]" placeholder="First name" @blur="validateInput('first')">
                  <div v-if="validation.first.invalid" class="invalid-feedback">@{{ validation.first.feedback }}</div>
                </div>
                <div class="col">
                  <input v-model="newUser.last" type="text" class="form-control form-control-sm" v-bind:class="[{'is-invalid': validation.last.invalid, 'is-valid': validation.last.valid}]" placeholder="Last name" @blur="validateInput('last')">
                  <div v-if="validation.last.invalid" class="invalid-feedback">@{{ validation.last.feedback }}</div>

                </div>
              </div>
          </td>
        </tr>

        <tr>
          <th>Email: </th>
          <td colspan="2">
              <div class="form-row">
                <div class="col">
                  <input v-model="newUser.email" type="text" class="form-control form-control-sm" v-bind:class="[{'is-invalid': validation.email.invalid, 'is-valid': validation.email.valid}]" placeholder="Email" @blur="validateInput('email')"><div v-if="validation.email.invalid" class="invalid-feedback">@{{ validation.email.feedback }}</div>
                </div>
              </div>
          </td>
        </tr>

        <tr>
          <th>Username: </th>
          <td colspan="2">
              <div class="form-row">
                <div class="col">
                  <input v-model="newUser.user" type="text" class="form-control form-control-sm" v-bind:class="[{'is-invalid': validation.user.invalid, 'is-valid': validation.user.valid}]" placeholder="Username" @blur="validateInput('user')" >
                  <div v-if="validation.user.invalid" class="invalid-feedback">@{{ validation.user.feedback }}</div>
                </div>
              </div>
          </td>
        </tr>
        <tr>
          <th>Role: </th>
          <td colspan="2">
              <div class="form-row">
                <div class="col">
                  <select v-model="newUser.role" class="form-control form-control-sm" v-bind:class="[{'is-invalid': validation.role.invalid, 'is-valid': validation.role.valid}]" id="" @blur="validateInput('role')">
                    <option value="" selected disabled>Select Access</option>
                    @if(Auth::user()->isMaster())
                      <option value="master">Master</option>
                    @endif
                    <option value="admin">Admin</option>
                    <option value="member">Member</option>
                  </select>
                </div>
              </div>
          </td>
        </tr>
        <tr>
          <th>Password: </th>
          <td colspan="2">
              <div class="form-row">
                <div class="col">
                  <input v-model="newUser.password" type="password" class="form-control form-control-sm" v-bind:class="[{'is-invalid': validation.password.invalid, 'is-valid': validation.password.valid}]" placeholder="Password" @blur="validateInput('password')">
                  <div v-if="validation.password.invalid" class="invalid-feedback">@{{ validation.password.feedback }}</div>
                </div>
              </div>
          </td>
        </tr>
        <tr>
          <th>Confirm password: </th>
          <td colspan="2">
              <div class="form-row">
                <div class="col">
                  <input v-model="newUser.confirm" type="password" class="form-control form-control-sm" v-bind:class="[{'is-invalid': validation.confirm.invalid, 'is-valid': validation.confirm.valid}]" placeholder="Confirm Password" @blur="validateInput('confirm')">
                  <div v-if="validation.confirm.invalid" class="invalid-feedback">@{{ validation.confirm.feedback }}</div>
                </div>
              </div>
          </td>
        </tr>
        
      </tbody>
    </table>
  {{-- end body --}}
  @slot('footer')
      <button  @click="addUser()" class="btn btn-primary btn-sm">
        <span v-if="!confirm.update">@{{ regbutton }} &nbsp <i class="fa fa-plus"></i></span>
        <span v-if="confirm.update">Please wait... <i key=1 class="fa fa-circle c-1 loading animated infinite flash"></i></span>
      </button>
  @endslot
@endmodal
@endsection

@section('script')
  <script src="{{ asset('js/admin.js') }}" defer></script>
@endsection