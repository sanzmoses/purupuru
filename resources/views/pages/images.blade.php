@extends('layouts.app')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-sm-12 col-md-12">
        <div class="card">
          <div class="card-header">Images &nbsp<span> <button class="btn btn-default btn-sm" disabled><i class="fa fa-file-image-o"></i></button></span></div>

          <div class="card-body">
            <div class="row mb-1">
              <div class="col-sm-12 col-md-6">
                <form class="input-group mt-1" @submit.prevent="importFile" enctype="multipart/form-data">
                  <div class="col-6">
                      <input ref="file" id="file" type="file" class="form-control-file form-control-sm trow" name="import_file" v-on:change="handleFilesUpload" multiple required />
                  </div>
                  <div class="col-6">
                      <button v-cloak v-if="loads.import" class="btn btn-sm bg-theme ml-1"><i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></button>
                      <button v-if="!loads.import" class="btn btn-sm bg-theme ml-1" type="submit">Import File</button>
                      <button class="btn btn-sm btn-warning ml-1" type="button" @click="clearInput()"><i class="fa fa-refresh"></i></button>
                  </div>                   
                </form>
                  {{-- <div class="input-group">
                    <button class="btn btn-default btn-sm bg-theme">Add &nbsp<i class="fa fa-plus"></i></button>
                  </div> --}}
              </div>
              <div class="col-sm-12 col-md-6">
                  <div class="input-group mt-1">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i v-if="loads.search" class="fa fa-search"></i>
                        <i v-if="!loads.search" v-cloak class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></span>
                    </div>
                    <input v-model="search" type="text" class="form-control form-control-sm" placeholder="Search by Name">
                  </div>
              </div>
            </div>

            <hr>

            @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
            @endif
            
            <div class="row">

        {{-- <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 mt-3 frame d-flex align-items-center justify-content-center">
                <div class="card theme-card">
                  <img class="img-thumbnail" src="http://placehold.it/900x400" alt="Card image cap">
                </div>
              </div> --}}
              <div v-cloak v-if="gallery" v-for="(photo, index) in photos" class="col-xs-12 col-sm-4 col-md-3 col-lg-2 frame p-2">
                <div class="theme-card">
                  <img :class="[photo.aspect_ratio]" :src="photo.path + photo.name + '.' + photo.extension" alt="Card image cap">
                  <div class="cover d-flex align-items-center justify-content-center">

                    <button class="btn btn-primary btn-sm" @click="viewPhoto(index)"><i class="fa fa-eye"></i></button>
                    <button class="btn btn-danger btn-sm ml-1" @click="deletePhoto(photo.id)"><i class="fa fa-trash"></i></button>

                    <p>@{{ photo.name+'.'+photo.extension }}</p>
                  </div>
                </div>
              </div>
              <div v-cloak v-if="!gallery" class="col-md-12 text-center">
                <p class="font-one-half"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp <b> No Image </b></p>
              </div>

{{--               <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 frame p-2">
                <div class="theme-card">
                  <img class="" src="http://placehold.it/900x600" alt="Card image cap">
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 frame p-2">
                <div class="theme-card">
                  <img class="" src="http://placehold.it/500x400" alt="Card image cap">
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 frame p-2">
                <div class="theme-card">
                  <img class="" src="http://placehold.it/700x400" alt="Card image cap">
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 frame p-2">
                <div class="theme-card">
                  <img class="" src="http://placehold.it/800x200" alt="Card image cap">
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 frame p-2">
                <div class="theme-card">
                  <img class="" src="http://placehold.it/400x600" alt="Card image cap">
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 frame p-2">
                <div class="theme-card">
                  <img class="" src="http://placehold.it/900x400" alt="Card image cap">
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 frame p-2">
                <div class="theme-card">
                  <img class="" src="http://placehold.it/900x600" alt="Card image cap">
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 frame p-2">
                <div class="theme-card">
                  <img class="" src="http://placehold.it/500x400" alt="Card image cap">
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 frame p-2">
                <div class="theme-card">
                  <img class="" src="http://placehold.it/700x400" alt="Card image cap">
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 frame p-2">
                <div class="theme-card">
                  <img class="" src="http://placehold.it/800x200" alt="Card image cap">
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 frame p-2">
                <div class="theme-card">
                  <img class="" src="http://placehold.it/400x600" alt="Card image cap">
                </div>
              </div> --}}
            </div>
            
          </div>
          <div class="card-footer">
              <nav class="row mt-3">
                <div class="col-md-4 col-sm-4"> 
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <label class="input-group-text" for="inputGroupSelect01">Entries</label>
                    </div>
                    <select v-model="perpage" class="custom-select" id="inputGroupSelect01">
                      <option selected>12</option>
                      <option value="18">18</option>
                      <option value="24">24</option>
                      <option value="32">32</option>
                      <option value="40">40</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-8 col-sm-8"> 
                <ul class="pagination justify-content-end">

                  <li class="page-item" v-bind:class="[{disabled: back}]">
                    <a class="page-link" aria-label="Previous" @click="skip(-group.divisor)">
                      <i class="fa fa-angle-double-left"></i>
                    </a>
                  </li>
                  
                  <li class="page-item" v-bind:class="[{disabled: prev}]">
                    <a class="page-link" aria-label="Previous" @click="step(-1)">
                      <i class="fa fa-caret-left"></i>
                    </a>
                  </li>
                  
                  <li class="page-item" v-cloak v-bind:class="[{active: pages.active}]" v-for="pages in paginateArr"
                  @click="fetch(pages.num)"><a class="page-link">@{{ pages.num }}</a></li>

                  <li class="page-item" v-if="loads.general"><a class="page-link"  ><div style="width: 17px" class="bar bar-xs"></div></a></li>

                  <li class="page-item" v-bind:class="[{disabled: next}]">
                    <a class="page-link" aria-label="Next" @click="step(1)">
                      <i class="fa fa-caret-right"></i>
                    </a>
                  </li>

                  <li class="page-item" v-bind:class="[{disabled: forward}]">
                    <a class="page-link" aria-label="Next" @click="skip(group.divisor)">
                      <i class="fa fa-angle-double-right"></i>
                    </a>
                  </li>

                </ul>
              </div>
            </nav>
          </div>
          
        </div>
      </div>
    </div>
  </div>

@modal()
  @slot('id')
    viewPhoto
  @endslot

  @slot('modalSize')
      modal-lg
  @endslot

  @slot('title')
    <i v-cloak class="fa fa-file-image-o"></i> &nbsp <p class="float-right"><b>Image:</b> <span style="font-size: 15px;">@{{photo.name + '.' + photo.extension}}</span></p>
  @endslot
  {{-- body --}}
    <img v-cloak :src="photo.path + photo.name + '.' + photo.extension" alt="" class="img-fluid mx-auto d-block">
  {{-- end body --}}
  @slot('footer')
      
  @endslot
@endmodal

@endsection

@section('script')
  <script src="{{ asset('js/images.js') }}" defer></script>
@endsection