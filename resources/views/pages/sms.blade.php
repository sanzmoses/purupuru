@extends('layouts.app')

@section('content')

  <div class="container-fluid">

    <div class="row">
      <div class="col-md-8">
          <div class="card bg-light mb-3">
            {{-- <div class="card-header"></div> --}}
            <div class="card-body ">
              <h5 class="card-title" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="cursor: pointer">
                  <span class="badge badge-info p-2 wide-space"> Note &nbsp <i class="fa fa-info-circle"></i> </span>
                  SMS
                  <i class="fa fa-caret-down ml-1"></i>
              </h5>
              <div class="collapse show" id="collapseExample">
                  <ul>
                    <li>The entries below are your sending logs.</li>
                    <li>The logs will automatically delete the older 50+ entries.</li>
                    
                    <li> <span class="badge badge-pill badge-warning" 
                      style="width: 30px"> 
                        &nbsp 
                      </span> <small> Pending </small>
                      <span class="badge badge-pill badge-success ml-2" 
                      style="width: 30px"> 
                        &nbsp
                      </span><small> Successful </small>
                      <span class="badge badge-pill badge-danger ml-2" 
                      style="width: 30px"> 
                        &nbsp
                      </span><small> Unsuccessful </small>
                    </li>
                  </ul>    
              </div>
            </div>
          </div>
      </div>
      <div class="col-md-4">        
        <div class="card bg-dark mb-3">
          <div class="card-body text-white">
            <h5 class="card-title">
              Bullshit Ads!
            </h5>
            <p class="card-text">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet veniam iste nihil, consectetur quis libero fugit, atque repellat similique ipsam, magni ipsum ratione soluta. Nam odit ipsa quam aut consectetur.
            </p>
          </div>
        </div>

      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
          <p style="font-weight: bold;"> 
            <i class="fa fa-paper-plane mr-2"></i>
            Messages
            {{-- <span class="btn-circle ml-2"><i class="fa fa-plus" aria-hidden="true"></i></span> --}}
          </p>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6">
          <ul class="nav justify-content-end">
            <li class="nav-item full-width">
              {{-- <input v-model="search" type="text" class="form-control" placeholder="search"> --}}
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i v-if="loads.search" class="fa fa-search"></i>
                    <i v-if="!loads.search" v-cloak class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></span>
                </div>
                <input v-model="search" type="text" class="form-control" placeholder="Search by Id, Number or Message">
              </div>
            </li>

          </ul> 
        </div>
      <div class="col-md-12">
            <table class="table">
                <thead class="bg-theme">
                  <tr>
                    <th scope="col"></th>
                    <th scope="col">#</th>
                    <th scope="col">Number</th>
                    <th scope="col">Message</th>
                    <th scope="col">Status</th>
                    <th scope="col">Created</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-if="loads.general">
                      <th> <div class="bar"></div> </th>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                    </tr>
                  <tr v-cloak v-for="log in logs">
                    <th scope="row"></th>
                    <th> @{{ log.id }} </th>
                    <td> @{{ log.number }} </td>
                    <td> @{{ log.message }} </td>
                    <td> 
                      <span class="badge badge-pill"
                      :class="[{ 
                        'badge-warning':log.status=='Pending', 
                        'badge-danger':log.status=='Error', 
                        'badge-success':log.status=='Sent' 
                      }]"
                      style="width: 50%"> 
                        &nbsp 
                      </span> 
                    </td>
                    <td> @{{ log.created_at }} </td>
                    <td>
                      {{-- <i class="fa fa-pencil"></i> --}}
                      {{-- <i class="fa fa-trash"></i> --}}
                    </td>
                  </tr>
                </tbody>
              </table>
      </div>
    </div>
    
    <nav class="row mt-3">
      <div class="col-md-4 col-sm-4"> 
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <label class="input-group-text" for="inputGroupSelect01">Entries</label>
          </div>
          <select v-model="perpage" class="custom-select" id="inputGroupSelect01">
            <option selected>10</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="40">40</option>
            <option value="50">50</option>
          </select>
        </div>

      </div>
      <div class="col-md-8 col-sm-8"> 
        <ul class="pagination justify-content-end">

          <li class="page-item" v-bind:class="[{disabled: back}]">
            <a class="page-link" aria-label="Previous" @click="skip(-group.divisor)">
              <i class="fa fa-angle-double-left"></i>
            </a>
          </li>
          
          <li class="page-item" v-bind:class="[{disabled: prev}]">
            <a class="page-link" aria-label="Previous" @click="step(-1)">
              <i class="fa fa-caret-left"></i>
            </a>
          </li>
          
          <li class="page-item" v-cloak v-bind:class="[{active: pages.active}]" v-for="pages in paginateArr"
          @click="fetch(pages.num)"><a class="page-link">@{{ pages.num }}</a></li>

          <li class="page-item" v-if="loads.general"><a class="page-link"><div style="width: 17px" class="bar bar-xs"></div></a></li>

          <li class="page-item" v-bind:class="[{disabled: next}]">
            <a class="page-link" aria-label="Next" @click="step(1)">
              <i class="fa fa-caret-right"></i>
            </a>
          </li>

          <li class="page-item" v-bind:class="[{disabled: forward}]">
            <a class="page-link" aria-label="Next" @click="skip(group.divisor)">
              <i class="fa fa-angle-double-right"></i>
            </a>
          </li>

        </ul>
      </div>
    </nav>

  </div>

@endsection

@section('script')
  <script src="{{ asset('js/sms.js') }}" defer></script>
@endsection