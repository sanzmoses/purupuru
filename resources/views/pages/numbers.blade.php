@extends('layouts.app')

@section('content')

  <div class="container-fluid">

    <div class="row">
      <div class="col-md-8">
          <div class="card bg-light mb-3">
            {{-- <div class="card-header"></div> --}}
            <div class="card-body ">
              <h5 class="card-title" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample" style="cursor: pointer">
                  <span class="badge badge-info p-2 wide-space"> Note &nbsp <i class="fa fa-info-circle"></i> </span>
                  Registered Numbers
                  <i class="fa fa-caret-down ml-1"></i>
              </h5>
              <div class="collapse show" id="collapseExample">
                  <ul>
                    <li>You can register a number by clicking this <i class="fa fa-plus-circle wide-space theme-color"></i> button below.</li>
                    <li>You can only register up to 5 numbers at a time.</li>
                    {{-- <li>Numbers that starts with 63.. are only allowed.</li> --}}
                    <span class="badge badge-pill badge-success" 
                      style="width: 30px"> &nbsp </span> 
                      <small class="ml-1"> Active </small>
                      <span class="badge badge-pill badge-warning ml-2" 
                      style="width: 30px"> &nbsp </span>
                      <small class="ml-1"> Pending Confirmation </small>
                  </ul>    
              </div>
            </div>
          </div>

          <p class="font-weight-bold mt-1"> Registered Numbers
            <span class="btn-circle pointer ml-2 shadow" @click="registerNum()">
              <i v-cloak v-if="!loads.create" class="fa fa-plus"></i>
              <i v-if="loads.create" class="fa fa-circle animated flash infinite"></i>
            </span>
            <span v-if="loads.create" v-cloak class="animated flash slower infinite"> <small>&nbsp @{{ phrases.current }} </small> </span>
          </p>
          <table class="table table-striped" style="margin-top: -15px;">
              <thead class="bg-theme">
                <tr>
                  <th scope="col"></th>
                  <th scope="col">#</th>
                  <th scope="col">Number</th>
                  <th scope="col">Created</th>
                  <th scope="col">Status</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td v-if="registered.length==0" colspan="6" class="text-center">Nothing to show...</td>
                </tr>
                <tr v-cloak v-for="(number,index) in registered">
                  <th scope="row"></th>
                  <th>@{{ number.id }}</th>
                  <td>@{{ number.number }}</td>
                  <td>@{{ number.created_at | formatDate }}</td>
                  <td> 
                    <span v-if="number.status=='pending'" 
                    class="badge badge-pill badge-warning" 
                    style="width: 50%"> 
                      &nbsp 
                    </span>
                    <span v-else class="badge badge-pill badge-success" 
                    style="width: 50%"> 
                      &nbsp
                    </span>
                  </td>
                  <td>
                    <i class="fa fa-trash pointer" @click="deleteNum(index)"></i>
                  </td>
                </tr>
              </tbody>
            </table>
          
      </div>
      <div class="col-md-4">        
        <div class="card bg-dark mb-3">
          <div class="card-body text-white">
            <h5 class="card-title">
              Bullshit Ads!
            </h5>
            <p class="card-text">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet veniam iste nihil, consectetur quis libero fugit, atque repellat similique ipsam, magni ipsum ratione soluta. Nam odit ipsa quam aut consectetur.
            </p>
          </div>
        </div>

      </div>
    </div>
  </div>

@modal()
  @slot('id')
    number-registration
  @endslot

  @slot('modalSize')
    modal-sm
  @endslot

  @slot('title')
    <i class="fa fa-phone-square light-icon mr-2"></i> Register Number
  @endslot
  {{-- body --}}
  <div style="overflow: hidden; padding: 5px 5px;">
  <transition
    name="custom-classes-transition"
    mode="out-in"
    enter-active-class="animated fadeInRight faster"
    leave-active-class="animated fadeOutLeft faster"
  >
  <div v-if="!register.verify" key="1">
    <small> <i class="fa fa-note mr-2"></i>Enter phone number : </small>
    <div class="input-group input-group-sm" :class="[{'mb-2': !validation.number.invalid}]">
      <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-sm">+63</span>
      </div>
      <input v-model="register.number" @keyup="validateInput('number')" type="text" class="form-control" v-bind:class="[{'is-invalid': validation.number.invalid, 'is-valid': validation.number.valid}]" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
    </div>
    <div v-if="validation.number.invalid" class="invalid-feedback mb-2" :class="[{'d-block' : validation.number.invalid}]">@{{ validation.number.feedback }}</div>

    <button @click="registerNum('sendcode')" class="btn btn-primary btn-sm float-right">Send Verification <i class="fa fa-angle-double-right ml-1" aria-hidden="true"></i></button>
  </div>
  <div v-else key="2">
  <small> <i class="fa fa-note"></i>A verification code is sent to: <span class="theme-color"> @{{ '+63'+register.number }} </span></small> <hr class="mt-0">
  <small>Enter the code to verify.</small>
    <input v-model="register.codeInput" type="text" class="form-control" v-bind:class="[{'is-invalid': validation.code.invalid, 'is-valid': validation.code.valid, 'mb-2': !validation.code.invalid}]" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
    <div v-if="validation.code.invalid" class="invalid-feedback mb-2" :class="[{'d-block' : validation.code.invalid}]">@{{ validation.code.feedback }}</div>
    <small> <a href="">Send the code again</a> </small>
    <button @click="registerNum('verify')" class="btn btn-primary btn-sm float-right">Verify <i class="fa fa-check ml-1" aria-hidden="true"></i></button>
  </div>
  </transition>
</div>
  {{-- end body --}}
  @slot('footer')
      
  @endslot
@endmodal

@confirm()
  @slot('id')
    confirmation
  @endslot

  @slot('modalSize')
      modal-sm
  @endslot

  @slot('title')
    
  @endslot
  {{-- body --}}
    <div class="text-center">
      <h3>Are you sure?</h3>
      <div>
        <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-label="Close" @click="confirmed('delete')">Yes</button>
        <button class="btn btn-success btn-sm" id="close-x" data-dismiss="modal" aria-label="Close">No</button>
      </div>
    </div>
  {{-- end body --}}
  @slot('footer')
  @endslot
@endconfirm

@endsection

@section('script')
  <script src="{{ asset('js/numbers.js') }}" defer></script>
@endsection