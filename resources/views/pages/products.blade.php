@extends('layouts.app')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-sm-12 col-md-12">
        <div class="card">
          <div class="card-header">Products &nbsp<span> <button @click="openModal('addProduct')" class="btn btn-default btn-sm">Add &nbsp<i class="fa fa-plus"></i></button></span></div>

          <div class="card-body">
            {{-- <div class="row mb-1">
              <div class="col-sm-12 col-md-6">
                <form class="input-group trow mt-1" @submit.prevent="importFile" enctype="multipart/form-data">
                  <div class="col-6">
                      <input disabled ref="file" id="file" type="file" class="form-control-file form-control-sm trow" name="import_file" v-on:change="handleFilesUpload" required />
                  </div>
                  <div class="col-6">
                      <button disabled class="btn btn-sm bg-theme ml-1" type="submit">Import File</button> 
                  </div>
                </form>
                  <div class="input-group">
                    <button class="btn btn-default btn-sm bg-theme">Add &nbsp<i class="fa fa-plus"></i></button>
                  </div>
              </div> --}}
              <div class="col-sm-12 col-md-6">
                  <div class="input-group mt-1">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i v-if="loads.search" class="fa fa-search"></i>
                        <i v-if="!loads.search" v-cloak class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></span>
                    </div>
                    <input v-model="search" type="text" class="form-control form-control-sm" placeholder="Search by Name, Brand or Products">
                  </div>
              </div>
            </div>

            @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
            @endif

            <div class="row"> 
              <div class="col-md-12 col-sm-12 table-responsive"> 
                <table class="table table-hover">
                  <thead class="thead bg-theme">

                    <tr>
                      <th scope="col" {{-- @click="inOrder()" --}}>#</th>
                      <th scope="col">Image</th>
                      <th scope="col">Name</th>
                      <th scope="col">Brand</th>
                      <th scope="col">Type</th>
                    </tr>

                  </thead>
                  <tbody>
                    
                    <tr v-cloak class="trow" v-for="(product, index) in products" @click="showProduct(product.id,index,$event)">
                      <th scope="row">@{{ product.id }}</th>
                      <td> 
                        <i v-if="product.images.length == 0" class="fa fa-circle text-danger"></i> 
                        <i v-else class="fa fa-circle text-success"></i> 
                      </td>
                      <td>@{{ product.name }}</td>
                      <td>@{{ product.brand }}</td>
                      <td class="d-flex justify-content-between">@{{ product.type }} &nbsp 
                          <button class="btn btn-danger btn-sm float-left d-btn">
                              <i class="fa fa-trash"></i></button>
                      </td>

                      <tr v-if="loads.general || loads.list">
                        <th> <div class="bar"></div> </th>
                        <td> <div class="bar"></div> </td>
                        <td> <div class="bar"></div> </td>
                        <td> <div class="bar"></div> </td>
                        <td> <div class="bar"></div> </td>
                      </tr>
                      {{-- <td >@{{ product.price | currency }} 
                      </td> --}}
                    </tr>

                  </tbody>
                </table>
              </div>
            </div>

            <nav class="row m-3">
              <div class="col-md-4 col-sm-4"> 
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Entries</label>
                  </div>
                  <select v-model="perpage" class="custom-select" id="inputGroupSelect01">
                    <option selected>10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option value="40">40</option>
                    <option value="50">50</option>
                  </select>
                </div>

              </div>
              <div class="col-md-8 col-sm-8"> 
                <ul class="pagination justify-content-end">

                  <li class="page-item" v-bind:class="[{disabled: back}]">
                    <a class="page-link" aria-label="Previous" @click="skip(-group.divisor)">
                      <i class="fa fa-angle-double-left"></i>
                    </a>
                  </li>
                  
                  <li class="page-item" v-bind:class="[{disabled: prev}]">
                    <a class="page-link" aria-label="Previous" @click="step(-1)">
                      <i class="fa fa-caret-left"></i>
                    </a>
                  </li>
                  
                  <li class="page-item" v-cloak v-bind:class="[{active: pages.active}]" v-for="pages in paginateArr"
                  @click="fetch(pages.num)"><a class="page-link">@{{ pages.num }}</a></li>

                  <li class="page-item" v-if="loads.general"><a class="page-link"  ><div style="width: 17px" class="bar bar-xs"></div></a></li>

                  <li class="page-item" v-bind:class="[{disabled: next}]">
                    <a class="page-link"  aria-label="Next" @click="step(1)">
                      <i class="fa fa-caret-right"></i>
                    </a>
                  </li>

                  <li class="page-item" v-bind:class="[{disabled: forward}]">
                    <a class="page-link"  aria-label="Next" @click="skip(group.divisor)">
                      <i class="fa fa-angle-double-right"></i>
                    </a>
                  </li>

                </ul>
              </div>
            </nav>
            
          </div>
          
        </div>
      </div>
    </div>
  </div>

@modal()
  @slot('id')
    viewProduct
  @endslot

  @slot('modalSize')
      modal-lg
  @endslot

  @slot('title')
    <i v-cloak class="fa fa-bars"></i> &nbsp 
    <p class="float-right"><b>Product Type:</b> <span class="badge badge-success"> @{{product.type}} </span> </p>
  @endslot
  {{-- body --}}
    <div class="container-fluid">
      <div class="row">
        <transition
          name="transition" mode="in-out"
          enter-active-class="animated fadeIn faster"
          leave-active-class="animated fadeOut faster">
          <div v-if="confirm.update" class="info-sanz bg-dark">

              {{-- <div v-if="!confirm.fromImages" class="info-box-sanz">
                <div v-if="confirm.loading" key=1 class="loading animated infinite flash">
                Processing <i class="fa fa-circle c-1"></i>
                </div>
                <div v-if="!confirm.loading" key=2>
                  <button class="btn btn-warning btn-sm">
                    <i class="fa fa-paper-plane" aria-hidden="true"></i> &nbsp Import File</button>
                  <h2><span>OR</span></h2>
                  <button class="btn btn-primary btn-sm" @click="confirm.fromImages = !confirm.fromImages">
                    <i class="fa fa-picture-o"></i> &nbsp Choose from Images</button>
                </div>
              </div> --}}

              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-10 offset-md-1 mt-3">
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text">
                                    <i v-if="loads.searchImg" class="fa fa-search"></i>
                                    <i v-if="!loads.searchImg" v-cloak class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></span>
                                </div>
                                <input v-model="searchImg" type="text" class="form-control form-control-sm" placeholder="Search by Name">
                            </div>
                        </div>
                        <div class="col">
                        <button class="btn btn-info btn-sm" @click="link()" :disabled="!product_imgs.length > 0"><i class="fa fa-plus"></i> 
                          Add Images : <span class="badge badge-light">@{{ product_imgs.length }}</span></button> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-10 offset-md-1 mt-3">
                  <div class="container-fluid">
                    <div class="row">
                      <div v-for="img in products_imgs" class="col-xs-6 col-sm-6 col-md-3 searchImg-thumbnail mt-1 p-1">
                        <div class="theme-card d-flex align-items-center justify-content-center">

                          <div :id="img.id + '-cover'" class="cover d-flex align-items-center justify-content-center">
                            <div class="custom-control custom-checkbox">
                              <input @click="checkBox(img.id)" type="checkbox" class="custom-control-input" 
                              :value="img" v-model="product_imgs" :id="img.id">
                              <label style="cursor: pointer" class="custom-control-label" :for="img.id">Name
                              </label>
                            </div>
                          </div>
                          <img :src="img.path + img.name +'.'+img.extension" alt="" class="img-fluid">

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

          </div>
        </transition>
        <div class="col-md-8 d-flex align-items-center justify-content-center bg-dark pl-0 pr-0 fix-height">
          <button v-if="preview.thumbnail == 0" class="btn btn-success btn-sm btn-rt mr-6" @click="setThumbnail()"><i class="fa fa-picture-o"></i></button>
          <button v-if="preview" class="btn btn-warning btn-sm btn-rt mr-5" @click="remove('link')"><i class="fa fa-times"></i></button>
          <button v-if="preview" class="btn btn-danger btn-sm btn-rt" @click="remove('completely')"><i class="fa fa-trash"></i></button>
          <img v-if="preview" :src="preview_src" class="img-fluid" alt="">
          <img v-else src="http://placehold.it/550x350" class="img-fluid" alt="">
        </div>
        <div class="col-md-4 pl-0 pr-0">          
          <table class="table table-sm mb-0">
            <thead>
              <tr>
                <td width="30%"> <b> Product <span class="badge badge-primary"> @{{product.id}} </span><br> Name: <b></td>
                <td width="70%">
                  <h6 v-if="update.edit">@{{ product.name }}</h6>
                  <p v-if="update.edit"><small> <i class="fa fa-calendar"></i>  @{{ product.created_at | formatDate }}</small></p>
                  <input v-if="update.save || update.processing" :disabled="update.processing" 
                  v-model="product.name" type="text" class="form-control form-control-sm" placeholder="Product Name">
                </td>
              </tr>
            </thead>
            <tbody>
             <tr>
                <th scope="row">Brand: </th>
                <td>
                  <p v-if="update.edit">@{{ product.brand }}</p>
                  <input v-if="update.save || update.processing" :disabled="update.processing" 
                  v-model="product.brand" type="text" class="form-control form-control-sm" placeholder="Brand">
                </td>
              </tr>
              <tr>
                <th scope="row">Description: </th>
                <td>
                  <p v-if="update.edit">@{{ product.description }}</p>
                  <input v-if="update.save || update.processing" :disabled="update.processing" 
                  v-model="product.description" type="text" class="form-control form-control-sm" placeholder="Description">
                </td>
              </tr>
            </tbody>
          </table>
          <hr class="mt-0">
          <div class="container">
            <div class="row">
              <div class="col-7 pl-3">

                {{-- <h4 v-if="update.edit">
                  <i class="fa fa-tag" aria-hidden="true"></i>
                  &nbsp @{{ product.price | currency }}
                </h4>
                
                <input v-if="update.save || update.processing" :disabled="update.processing" 
                v-model="product.price" type="text" class="form-control form-control-sm" placeholder="Price"> --}}
                    
              </div>
              <div class="col-5 d-flex align-items-center justify-content-end">

                    <button v-if="update.edit" @click="updating('edit')" class="btn btn-info btn-sm mb-2">Edit &nbsp <i class="fa fa-edit"></i> </button>
                    <button v-if="update.save" @click="updating('save')" class="btn btn-success btn-sm mb-2">Save &nbsp <i class="fa fa-floppy-o" aria-hidden="true"></i> </button>
                    
                    <div v-if="update.processing" key=1 class="loading" style="font-size: 12px;">
                        Updating &nbsp<i class="fa fa-circle c-1 animated infinite flash"></i>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    
    </div>
  {{-- end body --}}
  @slot('footer')
      <div class="container-fluid">
        <div class="row">
          <div v-for="(image, index) in product.images" @click="changePreview(image,index)" class="box d-flex align-items-center justify-content-center"
          :class="[{selected: index == preview_index}]">
            <img :src="image.path + image.name + '.' + image.extension" class="img-fluid" alt="">
          </div>
          <div data-toggle="tooltip" data-placement="top" title="Toggle Update Images" 
            class="box d-flex align-items-center justify-content-center" 
            @click="setClose('toggle')">
              <i class="fa fa-plus" aria-hidden="true"></i>
          </div>
        </div>
      </div>
  @endslot
@endmodal

@modal()
  @slot('id')
    addProduct
  @endslot

  @slot('modalSize')
      none
  @endslot

  @slot('title')
    Register User &nbsp <i class="fa fa-print"></i>
  @endslot
  {{-- body --}}
    <table class="table table-sm">

      <tbody>
        <tr>
          <th>Name: </th>
          <td colspan="2">
              <div class="form-row">
                <div class="col">
                  <input v-model="registration.name" type="text" class="form-control form-control-sm" 
                  v-bind:class="[{'is-invalid': validation.name.invalid, 'is-valid': validation.name.valid}]" 
                  placeholder="Product Name" @blur="validateInput('name')">
                  <div v-if="validation.name.invalid" class="invalid-feedback">@{{ validation.name.feedback }}</div>
                </div>
              </div>
          </td>
        </tr>

        <tr>
          <th>Brand: </th>
          <td colspan="2">
              <div class="form-row">
                <div class="col">
                  <input v-model="registration.brand" type="text" class="form-control form-control-sm" 
                  v-bind:class="[{'is-invalid': validation.brand.invalid, 'is-valid': validation.brand.valid}]" 
                  placeholder="Brand" @blur="validateInput('brand')" >
                  <div v-if="validation.brand.invalid" class="invalid-feedback">@{{ validation.brand.feedback }}</div>
                </div>
              </div>
          </td>
        </tr>

        <tr>
          <th>Type: </th>
          <td colspan="2">
              <div class="form-row">
                  <div class="col">
                    <input v-model="registration.type" type="text" class="form-control form-control-sm" 
                    v-bind:class="[{'is-invalid': validation.type.invalid, 'is-valid': validation.type.valid}]" 
                    placeholder="Type: Printer, Fax, Photocopy, Hybrid" @blur="validateInput('type')" >
                    <div v-if="validation.type.invalid" class="invalid-feedback">@{{ validation.type.feedback }}</div>
                  </div>
              </div>
          </td>
        </tr>

        <tr>
          <th>Description: </th>
          <td colspan="2">
              <div class="form-row">
                <div class="col">
                  <textarea v-model="registration.description" placeholder="Short Description" class="form-control form-control-sm" 
                  v-bind:class="[{'is-invalid': validation.description.invalid, 'is-valid': validation.description.valid}]"
                  @blur="validateInput('description')"></textarea>
                  <div v-if="validation.description.invalid" 
                  class="invalid-feedback">@{{ validation.description.feedback }}</div>
                </div>
              </div>
          </td>
        </tr>
      
        {{-- <tr>
          <th>Price: </th>
          <td colspan="2">
              <div class="form-row">
                <div class="col">
                  <input v-model="registration.price" type="price" class="form-control form-control-sm" 
                  v-bind:class="[{'is-invalid': validation.price.invalid, 'is-valid': validation.price.valid}]" 
                  placeholder="Price" @blur="validateInput('price')">
                  <div v-if="validation.price.invalid" class="invalid-feedback">@{{ validation.price.feedback }}</div>
                </div>
              </div>
          </td>
        </tr>         --}}
      </tbody>
      
    </table>
  {{-- end body --}}
  @slot('footer')
      <button  @click="addProduct()" class="btn btn-primary btn-sm" :disabled="confirm.register">
        <span v-if="!confirm.register">@{{ regbutton }} &nbsp <i class="fa fa-plus"></i></span>
        <span v-if="confirm.register">Please wait... <i key=1 class="fa fa-circle c-1 loading animated infinite flash"></i></span>
      </button>
  @endslot
@endmodal

@endsection

@section('script')
  <script src="{{ asset('js/products.js') }}" defer></script>
@endsection