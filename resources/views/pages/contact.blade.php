@extends('layouts.app')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-sm-12 col-md-12">
        <div class="card">
          <div class="card-header"> <i class="fa fa-envelope theme-color"></i> &nbsp Contact Us </div>

          <div class="card-body">
            <div class="row mb-1">
              <div class="col-sm-12 col-md-6">
                  <div class="input-group mt-1">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i v-if="loads.search" class="fa fa-search"></i>
                        <i v-if="!loads.search" v-cloak class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></span>
                    </div>
                    <input v-model="search" type="text" class="form-control form-control-sm" placeholder="Search by Name, Brand or messages">
                  </div>
              </div>
              <div class="col-sm-12 col-md-6">
              </div>
            </div>

            @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
            @endif

            <div class="row"> 
              <div class="col-md-12 col-sm-12 table-responsive"> 
                <table class="table table-hover">
                  <thead class="thead bg-theme">

                    <tr>
                      <th scope="col" {{-- @click="inOrder()" --}}>#</th>
                      <th scope="col">Name</th>
                      <th scope="col">Email</th>
                      <th scope="col">Phone</th>
                      <th scope="col">Message</th>
                      <th scope="col">Date-time</th>
                      <th scope="col">IP</th>
                    </tr>

                  </thead>
                  <tbody>
                    
                    <tr v-if="loads.general">
                      <th> <div class="bar"></div> </th>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                      <td> <div class="bar"></div> </td>
                    </tr>

                    <tr v-cloak v-if="messages == 0">
                      <td colspan="7" class="text-center">
                          <p class="font-one-half"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp <b> No Entries </b></p>
                      </td>
                    </tr>

                    <tr v-cloak class="trow" v-for="(message, index) in messages" @click="showMessage(message.id,index,$event)">
                      <th scope="row">@{{ message.id }}</th>
                      <td scope="row">@{{ message.name }}</td>
                      <td>@{{ message.email }}</td>
                      <td>@{{ message.phone || 'n/a' }}</td>
                      <td>@{{ message.message | shorten }}</td>
                      <td>@{{ message.created_at | formatDate }}</td>
                      <td class="d-flex justify-content-between">@{{ message.ip }} &nbsp 
                          <button class="btn btn-danger btn-sm float-left d-btn">
                              <i class="fa fa-trash"></i></button>
                      </td>
                      
                      {{-- <td >@{{ message.price | currency }} 
                      </td> --}}
                    </tr>

                  </tbody>
                </table>
              </div>
            </div>

            <nav class="row mt-3">
              <div class="col-md-4 col-sm-4"> 
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Entries</label>
                  </div>
                  <select v-model="perpage" class="custom-select" id="inputGroupSelect01">
                    <option selected>10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option value="40">40</option>
                    <option value="50">50</option>
                  </select>
                </div>

              </div>
              <div class="col-md-8 col-sm-8"> 
                <ul class="pagination justify-content-end">

                  <li class="page-item" v-bind:class="[{disabled: back}]">
                    <a class="page-link" href="#" aria-label="Previous" @click="skip(-group.divisor)">
                      <i class="fa fa-angle-double-left"></i>
                    </a>
                  </li>
                  
                  <li class="page-item" v-bind:class="[{disabled: prev}]">
                    <a class="page-link" href="#" aria-label="Previous" @click="step(-1)">
                      <i class="fa fa-caret-left"></i>
                    </a>
                  </li>
                  
                  <li class="page-item" v-cloak v-bind:class="[{active: pages.active}]" v-for="pages in paginateArr"
                  @click="fetch(pages.num)"><a class="page-link" href="#">@{{ pages.num }}</a></li>

                  <li class="page-item" v-if="loads.general"><a class="page-link" href="#"><div style="width: 17px" class="bar bar-xs"></div></a></li>

                  <li class="page-item" v-bind:class="[{disabled: next}]">
                    <a class="page-link"  href="#" aria-label="Next" @click="step(1)">
                      <i class="fa fa-caret-right"></i>
                    </a>
                  </li>

                  <li class="page-item" v-bind:class="[{disabled: forward}]">
                    <a class="page-link"  href="#" aria-label="Next" @click="skip(group.divisor)">
                      <i class="fa fa-angle-double-right"></i>
                    </a>
                  </li>

                </ul>
              </div>
            </nav>
            
          </div>
          
        </div>
      </div>
    </div>
  </div>

@modal()
  @slot('id')
    viewMessage
  @endslot

  @slot('modalSize')
      modal-md
  @endslot

  @slot('title')
    <i v-cloak class="fa fa-envelope"></i> &nbsp <p class="float-right"><b>Message:</b> <span class="badge badge-primary" style="font-size: 15px;">@{{message.id}}</span></p>
  @endslot
  {{-- body --}}
  <table class="table table-sm">
    <tbody>
      <tr>
        <th scope="row"><i class="fa fa-calendar"></i></th>
        <td>@{{message.created_at | formatDate}}</td>
      </tr>
      <tr>
        <th scope="row"><i class="fa fa-user-circle-o"></i></th>
        <td>@{{message.name}}</td>
      </tr>
      <tr>
        <th scope="row"><i class="fa fa-envelope-o"></i></th>
        <td>@{{message.email}}</td>
      </tr>
      <tr>
        <th scope="row"><i class="fa fa-phone"></i></th>
        <td>@{{message.phone || 'n/a'}}</td>
      </tr>
      <tr>
        <th scope="row"></th>
        <td colspan="2">@{{message.message}}</td>
      </tr>
    </tbody>
  </table>
  {{-- end body --}}
  @slot('footer')
      
  @endslot
@endmodal

@endsection

@section('script')
  <script src="{{ asset('js/contact.js') }}" defer></script>
@endsection