@extends('layouts.app')

@section('content')

  <div class="container-fluid">
    <div class="row">

      <div class="col-sm-7 col-md-7">
          <div class="card profile-card">
            <h5 class="card-header bg-theme">
              <i class="fa fa-user-circle"></i> &nbsp User # <span v-cloak>@{{profile.id}}</span> <i v-if="loads.general" class="fa fa-refresh"></i>
              <button v-if="!update" @click="update=true" class="btn btn-sm btn-reverse float-right"> Update &nbsp <i class="fa fa-edit"></i> </button>
              <button v-cloak v-if="update" @click="updateInfo" class="btn btn-sm btn-success float-right"> Save &nbsp 
                <i v-if="!confirm.update" class="fa fa-save"></i> 
                <i v-if="confirm.update" class="fa fa-spinner fa-spin"></i>
              </button>
              <button v-cloak v-if="update" @click="refreshValidation" class="btn btn-sm btn-warning float-right"> <i class="fa fa-close"></i> </button>
            </h5>
            <div class="card-body">

                <table class="table">
                  <thead>
                    <tr>
                      <th class="first-col" scope="col" width="30%">Access level: </th>
                      <th v-cloak colspan="2"> <span class="badge badge-primary profile-badge">@{{ profile.role }}</span> </th>
                      <th v-if="loads.general" colspan="2"> <div class="bar"></div> </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th class="first-col" scope="row">Name: </th>
                      <td v-cloak v-if="!update"> @{{ profile.first }} </td>
                      <td v-cloak v-if="!update"> @{{ profile.last }} </td>
                      <td v-cloak v-if="update"> <input v-model="profile.first" type="text" class="form-control form-control-sm" v-bind:class="[{'is-invalid': validation.first.invalid, 'is-valid': validation.first.valid}]" placeholder="First name" @blur="validateInput('first')">
                        <div v-if="validation.first.invalid" class="invalid-feedback">@{{ validation.first.feedback }}</div> </td>
                      <td v-cloak v-if="update"> <input v-model="profile.last" type="text" class="form-control form-control-sm" v-bind:class="[{'is-invalid': validation.last.invalid, 'is-valid': validation.last.valid}]" placeholder="First name" @blur="validateInput('last')">
                          <div v-if="validation.last.invalid" class="invalid-feedback">@{{ validation.last.feedback }}</div> </td>
                      <td v-if="loads.general"> <div class="bar"></div>  </td>
                      <td v-if="loads.general"> <div class="bar"></div> </td>
                    </tr>
                    <tr>
                      <th class="first-col" scope="row">Email : </th>
                      <td v-cloak v-if="!update" colspan="2">@{{ profile.email }}</td>
                      <td v-cloak v-if="update" colspan="2"> <input v-model="profile.email" type="text" class="form-control form-control-sm" v-bind:class="[{'is-invalid': validation.email.invalid, 'is-valid': validation.email.valid}]" placeholder="First name" @blur="validateInput('email')">
                        <div v-if="validation.email.invalid" class="invalid-feedback">@{{ validation.email.feedback }}</div> </td>
                      <td v-if="loads.general" colspan="2"> <div class="bar"></div> </td>
                    </tr>
                    <tr>
                      <th class="first-col" scope="row">Username : </th>
                      <td v-cloak v-if="!update" colspan="2">@{{ profile.user }}</td>
                      <td v-cloak v-if="update" colspan="2"> <input v-model="profile.user" type="text" class="form-control form-control-sm" v-bind:class="[{'is-invalid': validation.user.invalid, 'is-valid': validation.user.valid}]" placeholder="First name" @blur="validateInput('user')">
                        <div v-if="validation.user.invalid" class="invalid-feedback">@{{ validation.user.feedback }}</div> </td>
                      <td v-if="loads.general" colspan="2"> <div class="bar"></div> </td>
                    </tr>
                    
                  </tbody>
                </table>
            </div>
            <div class="card-footer">

            </div>
          </div>
      </div>

      <div class="col-sm-5 col-md-5">
        <div class="card password-card">
          <h5 class="card-header bg-theme">
            <i class="fa fa-lock"></i> &nbsp Change Password
          </h5>
          <div class="card-body">
              <form v-on:submit.prevent="changePassword">

                  <div class="form-group">
                    <label class="text-sm" for="old_pass">Password:</label>
                    <input v-model="password.old" type="password" class="form-control form-control-sm"  :class="[{'is-invalid': pass_validation.old.invalid, 'is-valid': pass_validation.old.valid}]" placeholder="Enter old password" @blur="validateInput('old')">
                    <div v-if="pass_validation.old.invalid" class="invalid-feedback">
                      @{{ pass_validation.old.feedback }}
                    </div>
                  </div>

                  <div class="form-group">

                    <label class="text-sm" for="new_pass">New password:</label>
                    <div class="input-group input-group-sm">
                      <input v-model="password.new" :type="password.show_new ? 'text' : 'password'" class="form-control" :class="[{'is-invalid': pass_validation.new.invalid, 'is-valid': pass_validation.new.valid}]" placeholder="Enter New Password" aria-describedby="button-addon1" @blur="validateInput('new')">
                      <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" id="button-addon1" @click="password.show_new=!password.show_new"><i class="fa fa-eye"></i></button>
                      </div>
                      
                      <div v-if="pass_validation.new.invalid" class="invalid-feedback">
                        @{{ pass_validation.new.feedback }}
                      </div>
                    </div>
                    <small v-if="!pass_validation.new.invalid" id="passHelp" class="form-text text-muted">Password must contain a letter and a number. Minimum of 8 characters</small>
                    
                  </div>
                  <div class="form-group">

                    <label class="text-sm" for="con_pass">Confirm password:</label>
                    <div class="input-group input-group-sm mb-3">
                      <input v-model="password.confirm" :type="password.show_confirm ? 'text' : 'password'" class="form-control"  :class="[{'is-invalid': pass_validation.confirm.invalid, 'is-valid': pass_validation.confirm.valid}]" placeholder="Confirm password" aria-describedby="button-addon2" @blur="validateInput('confirm')">
                      <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" id="button-addon2" @click="password.show_confirm=!password.show_confirm"><i class="fa fa-eye"></i></button>
                      </div>
                      <div v-if="pass_validation.confirm.invalid" class="invalid-feedback">
                          @{{ pass_validation.confirm.feedback }}
                      </div>
                    </div>
                    
                  </div>
                  <button type="submit" class="btn btn-sm btn-primary">Submit &nbsp <i class="fa fa-send"></i></button>
                  <button type="reset" @click="refreshPasswordFields" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="right" title="refresh input"> <i class="fa fa-refresh"></i> </button>
                </form>
          </div>
          <div class="card-footer"></div>
        </div>
      </div>

    </div>
  </div>

@endsection

@section('script')
  <script src="{{ asset('js/profile.js') }}" defer></script>
@endsection