@extends('layouts.app')

@section('content')

  <div class="container-fluid">
      <div class="alert alert-primary alert-dismissible fade show" role="alert">
        <strong> <i class="fa fa-info-circle"></i> &nbsp Hello there {{ Auth::user()->fname }} !</strong> 
        You can check on the docs for this page on the collapsible <span class="badge badge-info wide-space">Note &nbsp <i class="fa fa-info-circle"></i> </span> on the lower right corner.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

    <div class="row">
      <div class="col-md-8">
          
          <p style="font-weight: bold;"> API Keys
            <span class="btn-circle pointer ml-2 shadow" @click="generateKey()">
              <i v-cloak v-if="!loads.generate" class="fa fa-plus"></i>
              <i v-if="loads.generate" class="fa fa-circle animated flash infinite"></i>
            </span>
            <span v-if="loads.generate" class="animated flash slower infinite"> <small>&nbsp @{{ phrases.current }} </small> </span>
          </p>
          <table class="table table-sm" style="margin-top: -15px;">
              <thead class="bg-theme">
                <tr>
                  <th></th>
                  <th scope="col">ID</th>
                  <th scope="col">Key</th>
                  <th scope="col">Created</th>
                  <th scope="col">Status</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                <tr v-if="loads.general">
                  <td><div class="bar"></div></td>
                  <td><div class="bar"></div></td>
                  <td><div class="bar"></div></td>
                  <td><div class="bar"></div></td>
                  <td><div class="bar"></div></td>
                  <td><div class="bar"></div></td>
                </tr>
                <tr>
                  <td v-if="apikeys==''" v-cloak colspan="6" class="text-center">
                    Nothing to show...
                  </td>
                </tr>
                <tr v-cloak v-for="apikey in apikeys">
                  <th scope="row"></th>
                  <th> @{{ apikey.id | shorten }} </th>
                  <td> @{{ apikey.key }} </td>
                  <td> @{{ apikey.created_at | formatDate }} </td>
                  <td> 
                    <span class="badge badge-pill"
                    :class="[{
                    'badge-primary': apikey.status=='1', 
                    'badge-danger': apikey.status=='0'}]" 
                    style="width: 50%"> 
                      &nbsp 
                    </span> 
                  </td>
                  <td>
                    {{-- <i class="fa fa-pencil"></i> --}}
                    <i v-if="apikey.status=='1'" @click="confirmDelete(apikey.id)" class="fa fa-times pointer"></i>
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
            <div class="card text-white bg-info mb-3 shadow rounded-0" style="max-width: 30rem;">
              <div class="card-body">
              <h5 class="card-title">Active Key - <span v-if="activeKey==undefined">None</span> <span v-else><small v-cloak>@{{ activeKey.created_at | formatDate }}</small></span> </h5> 
                <div class="card-text" v-if="activeKey" v-cloak>
                  <table>
                    <tr>
                      <td> <strong>ID : </strong> </td>
                      <td> &nbsp </td>
                      <td> @{{ activeKey.id }} </td>
                    </tr>
                    <tr>
                      <td> <strong>Key : </strong> </td>
                      <td> &nbsp </td>
                      <td> @{{ activeKey.key }} </td>
                    </tr>
                  </table>
                  <p class="mb-0">  </p>  
                  
                </div> 
              </div>
            </div>
            <div>
              
               
            </div>
      </div>
      <div class="col-md-4">
        <div class="card bg-warning mb-3">
          <div class="card-body">
            <h5 class="card-title">
              <i class="fa fa-bullhorn"></i>
              &nbsp Announcement!
              <hr>
            </h5>
            <p class="card-text">
                We acknowledge the universal notion that intrusive ads are disturbing, irritating and annoying, nontheless for a free api service such as this, ads are the only way to support the continual operation of this application. Unless of course if you would like to donate. <a href="" class="theme-color">Donate here &nbsp <i class="fa fa-money"></i> </a>
            </p>
          </div>
        </div>
        
        <div class="card bg-light mb-3">
          {{-- <div class="card-header"></div> --}}
          <div class="card-body ">
            <h5 class="card-title" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="cursor: pointer">
                <span class="badge badge-info p-2 wide-space"> Note &nbsp <i class="fa fa-info-circle"></i> </span>
                API Key
                <i class="fa fa-caret-down ml-1"></i>
            </h5>
            <div class="collapse" id="collapseExample">
                <ul class="pl-3">
                  <li>You can generate an api key and an ID by clicking the <i class="fa fa-plus-circle wide-space theme-color"></i> button below.</li>
                  <li >You can only generate a single key which expires after 5 days. </li>
                  <li>After it expires only then you can generate a new key OR delete the current and generate another key before it expires.</li>
                  <li>To save space in database, api keys which are expired will be automatically deleted every week.</li>
                  <li> Status color codes: <span class="badge badge-pill badge-primary" 
                    style="width: 30px"> 
                      &nbsp 
                    </span> <small> Active </small>
                    <span class="badge badge-pill badge-danger ml-2" 
                    style="width: 30px"> 
                      &nbsp
                    </span><small> Expired / Deactivated </small>
                  </li>
                  <li>Deactivate the active key by clicking on the <i class="fa fa-times pointer wide-space"></i> icon</li>
                </ul>    
            </div>
          </div>
        </div>

        {{-- <div class="card bg-dark mb-3">
          <div class="card-body text-white">
            <h5 class="card-title">
              Bullshit Ads!
            </h5>
            <p class="card-text">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet veniam iste nihil, consectetur quis libero fugit, atque repellat similique ipsam, magni ipsum ratione soluta. Nam odit ipsa quam aut consectetur.
            </p>
          </div>
        </div> --}}

      </div>
    </div>
  </div>

@confirm()
  @slot('id')
    confirmation
  @endslot

  @slot('modalSize')
      modal-sm
  @endslot

  @slot('title')
    
  @endslot
  {{-- body --}}
    <div class="text-center">
      <h3>Deactivate Key?</h3>
      <div v-if="loading" key=1 class="loading animated infinite flash">
      Processing <i class="fa fa-circle c-1"></i>
      </div>
      <div v-if="!loading" key=2>
        <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-label="Close" @click="deactivate()">Yes</button>
        <button class="btn btn-success btn-sm" id="close-x" data-dismiss="modal" aria-label="Close">No</button>
      </div>
    </div>
  {{-- end body --}}
  @slot('footer')
  @endslot
@endconfirm

@endsection

@section('script')
  <script src="{{ asset('js/dashboard.js') }}" defer></script>
@endsection