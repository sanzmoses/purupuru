@extends('layouts.master')

@section('content')
<div class="container-fluid mt-1 about">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 justify-content-center p-0">
            <img 
            {{-- src="{{ asset('storage\site_img\image3.jpg') }}"  --}}
            src="http://placehold.it/1200x350"
            class="img-fluid" alt="Responsive image">
        </div>
        <div class="col-sm-12 col-md-12 col-lg-10 offset-lg-1 justify-content-center mb-5">
            <h1 class="font-weight-bold mt-5">About Laravue</h1>
            <h5 class="about-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus nihil tenetur debitis consectetur corporis? Nulla reprehenderit ea dolorem nam vel pariatur perferendis in illo, beatae reiciendis cum eligendi natus eius? Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, molestias architecto maxime eaque, placeat nihil sunt saepe obcaecati odio inventore dolorem soluta exercitationem at, nam in quia facere nulla iste?</h5>
            <h5 class="about-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus nihil tenetur debitis consectetur corporis? Nulla reprehenderit ea dolorem nam vel pariatur perferendis in illo, beatae reiciendis cum eligendi natus eius? Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, molestias architecto maxime eaque, placeat nihil sunt saepe obcaecati odio inventore dolorem soluta exercitationem at, nam in quia facere nulla iste?
            </h5>
        </div>
        <div class="col-mg-12 col-lg-12 bg-theme">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10 offset-lg-1 text-center">
                    <h1 class="font-weight-bold mt-5 mb-5">Services</h1>
                    <div class="row mb-5">
                        <div class="col-xs-12 col-sm-4 col-md-4 text-center">
                            <h1><i class="fa fa-wrench"></i></h1>
                            <h3 class="font-weight-bold">Installation</h3>
                            <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia quis quidem reprehenderit error ad quisquam dicta quas? </p>
                            <p><a href="#" class="btn btn-theme">Learn more &nbsp <i class="fa fa-wrench"></i></a></p>
                        </div>
                        <div class="col-xs-12 col-sm- col-md-4 text-center">
                            <h1><i class="fa fa-print"></i></h1>
                            <h3 class="font-weight-bold">Repair</h3>
                            <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia quis quidem reprehenderit error ad quisquam dicta quas? </p>
                            <p><a href="#" class="btn btn-theme">Learn more &nbsp <i class="fa fa-print"></i></a></p>
                        </div>
                        <div class="col-xs-12 col-sm- col-md-4 text-center">
                            <h1><i class="fa fa-bug"></i></h1>
                            <h3 class="font-weight-bold">Maintenance</h3>
                            <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia quis quidem reprehenderit error ad quisquam dicta quas? </p>
                            <p><a href="#" class="btn btn-theme">Learn more &nbsp <i class="fa fa-bug"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
</div>
@endsection
