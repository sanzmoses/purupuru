@extends('layouts.app')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-sm-12 col-md-12">
            <h3>Downloadables</h3>
            <hr>
            <h4 class="mt-3"> <b> <i class="fa fa-database wide-space"></i> Database </b></h4>
            <p>Download a copy of DB as a back up every time there is a siginificant change.</p>
            <button class="btn btn-primary" @click="dbDownload"> <i class="fa fa-download wide-space"></i> Get SQL file </button>
            <hr>
            <h4 class="mt-3"> <b> <i class="fa fa-file-excel-o wide-space"></i> Excel </b></h4>
            <p>Download a copy of tables as excel files.</p>
            
            <div class="row">
              <div class="col-md-5">
                <div class="input-group">
                  <select v-model="table" class="custom-select" id="inputGroupSelect04">
                    <option value="users" selected>Users</option>
                    <option value="products">Products</option>
                    <option value="contacts">Contact</option>
                  </select>
                  <div class="input-group-append">
                    <button v-cloak class="btn btn-primary" type="button" @click="excelDownload"> @{{ table }} <i class="fa fa-download wide-space"></i></button>
                    <button v-if="loads.general" class="btn btn-primary" type="button"> Users <i class="fa fa-download wide-space"></i></button>
                  </div>
                </div>
              </div>
            </div>
      </div>
    </div>
  </div>

@endsection

@section('script')
  <script src="{{ asset('js/dl.js') }}" defer></script>
@endsection
