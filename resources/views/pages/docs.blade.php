@extends('layouts.app')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-sm-12 col-md-12">
        <div class="alert alert-warning fade show" role="alert">
          <strong> <i class="fa fa-book"></i> &nbsp The Documentation </strong> 
          left out the obvious parts of the app. If there are complicated sections not found in the docs, just contact the devs.
        </div>        
      </div>
      <div class="col-sm-12 col-md-10 offset-md-1">
          <h3 class="mt-3 theme-color"> <b> Products </b></h3>
          <ul class="ml-2 theme-color docs-list">
            <li><b><a href="#createProduct">Creating Products</a></b></li>
            <li><b><a href="#updateProduct">Updating Products</a></b></li>
            <li class="ml-4"><small><a href="#updateInfo">Updating Information</a></small></li>
            <li class="ml-4"><small><a href="#addImageToProduct">Adding Image to Products</a></small></li>
            <li class="ml-4"><small><a href="#removeImageFromProduct">Remove Image from a product</a></small></li>
            <li class="ml-4"><small><a href="#setProductThumbnail">Setting product thumbnail</a></small></li>
            <li><b><a href="#deleteProduct">Delete Products</a></b></li>
          </ul>
          <h3 class="mt-3 theme-color"> <b> Images </b></h3>
          <ul class="ml-2 theme-color docs-list">
            <li><b><a href="#uploadImage">Uploading</a></b></li>
            <li class="ml-4"><small><a href="#uploadImage">Upload Info</a></small></li>
            <li class="ml-4"><small><a href="#refreshInput">Refresh Input</a></small></li>
            <li><b><a href="#viewImage">View Image</a></b></li>
            <li><b><a href="#deleteImage">Delete Image</a></b></li>
          </ul>
          <hr class="mt-4 mb-4">
          <h4 class="mt-5" id="createProduct"># Creating Products</h4>
            <div class="ml-4 mt-3">
              <p>Creating products is pretty straightforward. Just click on the <button class="btn btn-default btn-sm bg-theme wide-space">Add &nbsp <i class="fa fa-plus"></i> </button> button 
              located on the top corner section of the panel and it will open a registration modal.</p>
            </div>

          <h4 class="mt-5" id="updateProduct"># Updating Products</h4>
            <div class="ml-4 mt-3">
                <p id="updateInfo">To update a product information. Just click on the list to choose one product and a modal will open. 
                On the right corner click on <button class="btn btn-info btn-sm wide-space">Edit &nbsp <i class="fa fa-edit"></i></button> 
                The information will become an input form and the button will change to <button class="btn btn-success btn-sm wide-space">Save &nbsp <i class="fa fa-floppy-o"></i></button> </p>

                <h5 class="mt-4" id="addImageToProduct"> <b>Adding Images to the Product</b></h5>
                <p>On the modal of individual product, a button on the lower-left corner with <i class="fa fa-plus wide-space"></i> sign with info <span class="badge badge-dark p-2 wide-space">Toggle Update Images</span> is visible. Click on it then search for the image by its name, make sure the image/s are not associated with another product and present on the Image section of the app.</p>
                <p> <span class="badge badge-info p-2 wide-space">On search input </span> You can type "all" on the search bar and all images <b>unassociated with a product</b> will be shown.</p>
                <div class="alert alert-info fade show" role="alert">
                  <strong> <i class="fa fa-info-circle"></i> &nbsp Note: </strong> 
                  The Image/s that are already linked with a product cannot be added to another product on the list. Will therefore not be shown in the search for image on a product.
                </div>  
                <p> <span class="badge badge-info p-2 wide-space">Second Note:</span> The product will only have up to 5 images!</p>

                <h5 class="mt-4" id="removeImageFromProduct"> <b>Remove Image from a Product</b></h5>
                <p>To unlink an image from a product. Click on the image you want to unlink at the bottom of the product-modal. As you hover over the large image in the modal. 3 or 2 buttons will appear. Click on the <button class="btn btn-warning btn-sm rounded-0"> <i class="fa fa-times"></i> </button> to unlink the image. An alert will appear to confirm it.</p>
                <p> <span class="badge badge-danger p-2 wide-space"> Warning &nbsp <i class="fa fa-exclamation-circle"></i> </span> This button <span class="badge badge-danger p-2 wide-space rounded-0"> <i class="fa fa-trash" style="color:black"></i> </span> on the right side of "unlink" button will permanently delete the image from the app.</p>

                <h5 class="mt-4" id="setProductThumbnail"><b>Setting Product thumbnail</b></h5>
                <p>The product thumbnail will the be the image the visitor/user will see on the frontpage of the site.</p>
                <p> <span class="badge badge-info p-2 wide-space"> Note &nbsp <i class="fa fa-info-circle"></i> </span> Upon linking: the first image from the left will be the default thumbnail of the product unless set otherwise.</p>
                <p> To set the thumbnail, click on the desired image. And on hover (just like on unlinking an image) click this button <button class="btn btn-success btn-sm wide-space rounded-0"><i class="fa fa-picture-o"></i></button> and the image will become the thumbnail of the product.</p>
            </div>

            <h4 class="mt-5" id="deleteProduct"># Delete Products</h4>
            <div class="ml-4 mt-3">
              <p>To delete a product. Just hover over a product in the table-list and click this button <button class="btn btn-danger btn-sm wide-space"><i class="fa fa-trash"></i></button> will appear on the right corner of the table-row.</p>
            </div>

            <hr class="mt-4 mb-4">
            <h4 class="mt-5" id="uploadImage"># Uploading Image/s</h4>
              <div class="ml-4 mt-3">
                <p> <span class="badge badge-info p-1 wide-space"> <i class="fa fa-picture-o"></i> &nbsp Images </span> Navigate to the Images tab.</p>
                <p>Click on the <span class="badge badge-light p-2 wide-space" style="border: 1px solid gray">Choose Files</span> button, choose the file or files you want to upload then click on the import file button <button class="btn btn-primary btn-sm wide-space">Import File</button> The images will be shown on the panel below.</p>
                
                <div class="alert alert-info fade show" role="alert">
                  <strong> <i class="fa fa-info-circle"></i> &nbsp Note: </strong> 
                  Strictly file size will be limited to 550000 kb.
                </div>  

                <h5 class="mt-4" id="refreshInput"> <b>Refresh Input</b></h5>
                <p>The refresh input button <button class="btn btn-sm btn-warning wide-space" type="button"><i class="fa fa-refresh"></i></button> will refresh the files on the 'Choose Files' input </p>
              </div>
            
            <h4 class="mt-5" id="viewImage"># View Image</h4>
            <div class="ml-4 mt-3">
              <p>To view an Image. Just hover over a image in the panel and click this button <button class="btn btn-primary btn-sm wide-space"><i class="fa fa-eye"></i></button>. <br> A modal will open for better viewing of the image.</p>
            </div>

            <h4 class="mt-5" id="deleteImage"># Delete Image</h4>
            <div class="ml-4 mt-3">
              <p>To delete an Image. Just hover over a image in the panel and click this button <button class="btn btn-danger btn-sm wide-space"><i class="fa fa-trash"></i></button>. <br> A confirmation box will appear at the top.</p>
            </div>
      </div> 
    </div>
  </div>

@endsection

@section('script')
  <script src="{{ asset('js/docs.js') }}" defer></script>
@endsection