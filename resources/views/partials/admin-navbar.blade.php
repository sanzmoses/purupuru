
 <nav class="navbar navbar-expand-lg navbar-dark bg-teal fixed-top">
   <div class="container-fluid">
     <i @click="sidenav.toggle = !sidenav.toggle" class="fa fa-th-list sidebar-toggle d-lg-none text-white" aria-hidden="true"></i>

     <a class="navbar-brand" href="/"> 
        <img src="{{ asset('files\images\home\logo-nc.png') }}" alt="PuruPuru" class="img-fluid p-logo mr-2" style="margin-top: -7px;"> {{ config('app.name', 'Laravue MPA') }}</a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
       </button>

       <div class="collapse navbar-collapse" id="navbarResponsive">
         <ul class="navbar-nav ml-auto">
            @auth
                @if(Auth::user()->isAdmin())
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        @if($notif->where('status', 'new')->count() > 0)
                            <i class="fa fa-bell text-warning" aria-hidden="true">
                        @else     
                            <i class="fa fa-bell" aria-hidden="true">
                        @endif
                        </i> <span class="caret"></span>
                    </a>
                    
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                    @if($notif->count() > 0)
                        @foreach($notif as $n)

                            @if($n->status == 'seen')
                                <a class="dropdown-item notif-item"  href="#">
                            @else 
                                <a class="dropdown-item notif-item notif-highligth" href="/view/{{ $n->id }}">
                            @endif 

                                @if($n->status == 'seen')
                                    <span class="badge badge-success badge-pill"> <i class="fa fa-check"></i> </span>
                                @else
                                    <span class="badge badge-primary badge-pill">{{ $n->qty }} </span>
                                @endif
                                &nbsp {{ $n->status }} &nbsp {{ $n->name }}
                                <p class="mb-0"><small> <i class="fa fa-clock-o"></i> &nbsp {{ $n->timeDiff() }}</small></p>
                                </a>
                        @endforeach
                    @else
                            <p class="mb-0 opac text-center">
                                <i aria-hidden="true" class="fa fa-ban theme-color"></i> 
                                &nbsp Nothing to show
                            </p>
                    @endif
                    </div>
                </li>
                <li class="nav-item">
                    <a href="/message" class="nav-link">
                        @if($notif->where('name', 'Message')->where('status', 'new')->count() > 0)
                            <i class="fa fa-envelope text-warning"></i>
                        @else 
                            <i class="fa fa-envelope"></i>
                        @endif
                    </a>
                </li>
                @endif
            @endauth
               <li class="nav-item dropdown">
                   <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                       {{ Auth::user()->fname }} <span class="caret"></span>
                   </a>
                   
                   <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/profile">
                            <small> 
                                PROFILE <i class="fa fa-address-book-o float-right" aria-hidden="true"></i>
                            </small>
                        </a>

                       <a class="dropdown-item" href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            <small> 
                                LOGOUT <i class="fa fa-sign-out float-right" aria-hidden="true"></i>
                            </small>
                        </a>

                       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                           @csrf
                       </form>                       
                   </div>
               </li>
         </ul>
       </div>
     </div>
   </nav>