<div class="bg-dark fixed-top" id="minibar">
    <div class="container">
        <div class="row">
            <div class="col-7 d-flex flex-row bd-highlight mb-3">
                    <div class="mr-2 pl-1 bd-highlight mynav"><a>Follow us on :</a></div>
                    <div class="mr-2 pl-1 bd-highlight mynav"><a target="_blank" 
                        href="https://www.facebook.com/sanzillion"> Facebook 
                        <i class="fa fa-facebook"></i> </a></div>
                    <div class="mr-2 pl-1 bd-highlight mynav"><a target="_blank" 
                        href="https://github.com/sanzmoses"> Github 
                        <i class="fa fa-github"></i> </a></div>
                    {{-- <div class="mr-2 pl-1 bd-highlight mynav"><a href="#"> Twitter <i class="fa fa-twitter"></i> </a></div> --}}
            </div>
            <div class="col-5 d-flex flex-row-reverse bd-highlight">
                @guest
                <div class="mr-2 pl-1 pr-1 bd-highlight mynav"><a class="mini-mynav" >
                    <i class="fa fa-user"></i> {{ __('Login') }}</a></div>
                <div class="mr-2 pl-1 pr-1 bd-highlight mynav"><a class="mini-mynav" > 
                    <i class="fa fa-user-plus"></i> {{ __('Register') }}</a></div>
                @endguest
                @auth
                    <div class="mr-2 pl-1 pr-1 bd-highlight mynav"><a class="mini-mynav"> 
                        <i class="fa fa-user-circle"></i>&nbsp {{ Auth::user()->fname }}</a>
                    </div>

                    @if(Auth::user()->isAdmin())
                        <div class="mr-2 pl-1 pr-1 bd-highlight mynav"><a class="mini-mynav" href="/admin"> 
                            <i class="fa fa-tachometer"></i>&nbsp Dashboard</a>
                        </div>
                    @else 
                        <div class="mr-2 pl-1 pr-1 bd-highlight myna">
                            <a class="mini-mynav" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out" aria-hidden="true"></i> {{ __('Logout') }}
                        </a>
                        </div>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                           @csrf
                        </form> 

                        <div class="mr-2 pl-1 pr-1 bd-highlight mynav"><a class="mini-mynav" href="/user"> 
                            <i class="fa fa-address-card" aria-hidden="true"></i>&nbsp Account</a>
                        </div>
                    @endif
                @endauth
            </div>
            
        </div>
    </div>
</div>

{{-- <ul>
    <li class="mynav-item">
    
    </li>
    <li class="mynav-item">
    </li>
</ul> --}}