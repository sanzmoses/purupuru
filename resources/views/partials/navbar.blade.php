 <!-- Navigation -->
    
  <nav class="navbar navbar-expand-lg navbar-dark" :class="{ 'fixed-top': fixed, navAnimate: fixed}">
    <div class="container">

        <a class="navbar-brand" href="/"> 
          <img src="{{ asset('files\images\home\logo-nc2.png') }}" alt="PuruPuru" class="img-fluid p-logo mr-2" style="margin-top: -7px;"> 
          {{-- <i class="fa fa-circle mr-2"></i> --}}
          <span class="mt-2">PuruPuru</span> </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            
                {{-- <li class="nav-item">
                  <a class="nav-link" :class="{linkActive:nav.about}" href="/about">
                    <i class="fa fa-question-circle d-inline mr-2"></i>Info</a>
                </li> --}}
                @guest
                  <li class="nav-item">
                      <a class="nav-link login-button" :class="{linkActive:nav.services}" href="{{ route('login') }}">Login</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link register-button" :class="{linkActive:nav.contact}" href="{{ route('register') }}">Free Registration</a>
                  </li>
                @endguest
                @auth
                  
                    <li class="nav-item">
                      <a class="nav-link login-button" :class="{linkActive:nav.contact}" 
                      @if(Auth::user()->isAdmin())
                        href="/admin"
                      @else 
                        href="/dashboard"
                      @endif
                      >
                      Hi &nbsp{{ Auth::user()->username }}, back to dashboard <i class="fa fa-arrow-circle-right"></i> </a>
                    </li>
                 
                @endauth

          </ul>
        </div>
      </div>
    </nav>