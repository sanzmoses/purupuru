@auth 
  @if(Auth::user()->isAdmin())
    <transition
        name="custom-classes-transition"
        enter-active-class="animated fadeIn faster"
        leave-active-class="animated fadeOut slow"
        v-on:leave="leave">
      <div id="sidenav-wrapper" v-if="!sidenav.toggle" v-cloak class="wrapper"  @click="sidenav.toggle = !sidenav.toggle">
        <div class=" sidebar sidebar-mobile animated slideInLeft faster">
          <nav class="nav flex-column">
            <a class="nav-link" :class="[{active: sidenav.users}]" href="/admin">
              <i class="fa fa-tachometer animated" :class="[{flipInX: sidenav.users}]" aria-hidden="true"></i>
              Dashboard
            </a>
            <a class="nav-link" :class="[{active: sidenav.products}]" href="/products">
              <i class="fa fa-print animated" :class="[{flipInX: sidenav.products}]" aria-hidden="true"></i> 
              Products
            </a>
            <a class="nav-link" :class="[{active: sidenav.images}]" href="/images">
              <i class="fa fa-picture-o animated" :class="[{flipInX: sidenav.images}]" aria-hidden="true"></i> 
              Images
            </a>
            <a class="nav-link" :class="[{active: sidenav.documentation}]" href="/docs">
              <i class="fa fa-book animated" :class="[{flipInX: sidenav.documentation}]" aria-hidden="true"></i> 
              Documentation
            </a>
            <a class="nav-link" :class="[{active: sidenav.downloadables}]" href="/downloadables">
              <i class="fa fa-download animated" :class="[{flipInX: sidenav.dowbloadables}]" aria-hidden="true"></i> 
              Downloadables 
            </a>
          </nav>
        </div>
      </div>
    </transition>
  @else 
      <transition
      name="custom-classes-transition"
      enter-active-class="animated fadeIn faster"
      leave-active-class="animated fadeOut slow"
      v-on:leave="leave">
    <div id="sidenav-wrapper" v-if="!sidenav.toggle" v-cloak class="wrapper"  @click="sidenav.toggle = !sidenav.toggle">
      <div class=" sidebar sidebar-mobile animated slideInLeft faster">
        <nav class="nav flex-column">
          <a class="nav-link" :class="[{active: sidenav.dashboard}]" href="/dashboard">
            <i class="fa fa-tachometer animated" :class="[{flipInX: sidenav.dashboard}]" aria-hidden="true"></i>
            Dashboard
          </a>
          <a class="nav-link" :class="[{active: sidenav.numbers}]" href="/numbers">
            <i class="fa fa-address-book animated" :class="[{flipInX: sidenav.numbers}]" aria-hidden="true"></i>
            Numbers
          </a>
          <a class="nav-link" :class="[{active: sidenav.sms}]" href="/sms">
            <i class="fa fa-telegram animated" :class="[{flipInX: sidenav.sms}]" aria-hidden="true"></i>
            Sms
          </a>
        </nav>
      </div>
    </div>
    </transition>
  @endif 
@endauth