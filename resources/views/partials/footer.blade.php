    <!-- Footer -->
    <footer class="py-3 bg-dark">
      <div class="container">
        <div class="row mt-2">
          <div class="col-md-4">
              <div class="d-flex">
                <img class="footer-logo" src="{{ asset('files/images/home/logo-nc1.png') }}" alt="Footer Logo Purupuru">
                <h2 class="thick text-white ml-2">PuruPuru</h2>
              </div>
          </div>
          <div class="col-md-8">
              <div class="d-flex mt-3 float-md-right float-left">
                  <p>
                    <a href="" class="theme-color"> Register</button> 
                    <span class="theme-color mr-3 ml-3">|</span>
                    <a href="" class="theme-color"> Sign in</a>
                    <span class="theme-color mr-3 ml-3">|</span>
                    <a href="" class="theme-color"> <i class="fa fa-facebook-official"></i> </a>
                    <span class="theme-color mr-3 ml-3">|</span>
                    <a href="" class="theme-color"> <i class="fa fa-twitter-square"></i> </a>
                    <br>
                    <span class="text-white">Devs:</span> 
                    <a class="ml-3 theme-color" href="https://www.facebook.com/sanzillion" target="_blank">Sanz Moses</a>
                    <span class="theme-color mr-3 ml-3">|</span>
                    <a class="theme-color" href="https://www.facebook.com/johnmichael.andales" target="_blank">John Micheal</a>
                  </p>
                </div>
          </div>
          
        </div>        
        
        <hr class="white-line">

        <p class="m-0 d-inline text-left text-white">Copyright &copy; <a class="text-white"
          href="https://www.facebook.com/sanzillion" style="text-decoration: none;"> <small>
          2019 PuruPuru</a> </small></p>
        <p class="m-0 d-inline float-right text-white float-xs-left"> <small>Powered by: 
          <a class="synapse" href="http://synapsetechenterprise.com/">
            Synapse Tech Enterprise &nbsp <i class="fa fa-rocket"></i>
          </a> </small>
          {{-- <a class="synapse" href="https://github.com/sanzmoses/laravue-mpa"> 
            Sanz 
          <i class="fa fa-facebook"></i> </a> --}}
        </p>
      </div>
      <!-- /.container -->
    </footer>