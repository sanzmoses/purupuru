<transition
    name="notification-transition"
    enter-active-class="animated fadeInDown faster"
    leave-active-class="animated fadeOut faster">
    <div v-cloak v-show="notif.display" class="notification">
        <p :class="notif.status">@{{ notif.message }}</p>
    </div>
</transition>