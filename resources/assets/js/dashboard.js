require('./bootstrap');

import axios from 'axios';
import { loadProgressBar } from 'axios-progress-bar';
window.Vue = require('vue');
import {store} from './store';

const app = new Vue({
    el: '#admin',
    data: {
        sidenav: store.state.sidenav,
		notif: store.state.notification,
		phrases: store.state.phrases,
        changes: false,
        loads: {
            general: true,
			search: true,
			updates: false,
			generate: false,
		},
		confirm: {
			delete: false,
			remove: false,
		},
		loading: false,
		modalOpen: "",
		apikeys: "",
		toDelete: "",
		activeKey: "",
    },
    mounted() {
    	//axios progress bar package
		loadProgressBar();
		        
    	// console.log('test');
        this.loads.general = false;

		let inst = this;
		setTimeout(function(){
			inst.changes = false;
		}, 2000);

		inst.fetchList();
    },
    methods: {
		fetchList() {
			axios.get('key/user')
			.then(response => {
				let data = response.data;
				this.apikeys = data.apikey;

				this.activeKey = data.apikey.find(function(el) {
					return el.status == '1';
				});
			})
			.catch(e => {
				console.log(e);
				this.notify('bg-danger', 'Something\'s wrong, call the devs!');
			});
		},
		generateKey() {
			if(this.loads.generate) {
				this.notify('bg-warning', 'Dont spam please!');
			}
			else {
				let inst = this;
				inst.loads.generate = true;
				let error = [false, ''];
				//check if there is an active key
				inst.apikeys.forEach(el => {
					if(el.status == 1) {
						error[0] = true;
						error[1] = 'Only one active key allowed!';
					}
				});
	
				if(error[0]) {
					inst.notify('bg-info', error[1]);
					inst.loads.generate = false;
				}
				else {
					let index = 0;
					let len = inst.phrases.all.length;
					inst.phrases.current = inst.phrases.all[index];
					let interval = setInterval(function() {
						index++;
						inst.phrases.current = inst.phrases.all[index];
						if (index == len-1) { location.reload(); }
					}, 10000);

					axios.post('/key/generate')
					.then(response => {
						console.log(response.data);
						inst.info = response.data;
						inst.loads.generate = false;
						clearInterval(interval)
						inst.fetchList();
						inst.notify('bg-success', 'Key generated!');
					})
					.catch(e => {
					  console.log(e);
					  inst.notify('bg-danger', 'Something\'s wrong, call the devs!'); 					})
				}
			}			
		},
		confirmDelete(id) {
			this.toDelete = id;
			this.openModal('confirmation');
		},
		deactivate() {
			axios.post('/key/deactivate', {
				id: this.toDelete
			})
			.then(response => {
				console.log(response.data);
				close(this.modalOpen);
				this.fetchList();
				this.notify('bg-success', 'Successfully deactivated!');
            })
            .catch(e => {
			  console.log(e);
			  this.notify('bg-success', 'Something\'s wrong');
            })
		},
        leave: function (el, done) {
	        $('.sidebar-mobile').addClass('slideOutLeft');
	        setTimeout(function() {
	            done();
	        }, 1000);
		},
		openModal(modalId) {
			this.modalOpen = modalId;
	        $('#'+modalId).modal('toggle');
	    },
	    close(e) {
			// console.log(e.target.id);
			if(e == 'close') {
				$('#showUser').modal('toggle');
				this.setClose('close');
			}
	
			try {
			   if(e.target.id == this.modalOpen || e.target.id == 'close-x') {
					this.setClose('close');
				} 
			}
			catch(err) {
				console.log(err);
				this.notify('bg-danger', 'Something went wrong!');
			}
		},
		setClose() {
			this.confirm.remove = false;
			// console.log('closing');
		},
		notify(status, message, delay) {
			if(delay == undefined){ delay = 1500 };
			let inst = this;
			//status = ['bg-warning', 'bg-info', and etc]
			inst.notif.status = status;
			inst.notif.message = message;
			inst.notif.display = true;
			setTimeout(function() {
				inst.notif.display = false;
			}, delay);
		}
	},
	watch: {
		confirm: {
			handler: function(p) {
				if(p.remove) {
					this.openModal('confirmation');
				}
			},
			deep: true
		},
	},
	filters: {
		formatDate: function(value) {
			var dt = new Date(value);
			var dd = dt.getDate();
			var mm = dt.getMonth()+1;
			var yyyy = dt.getFullYear();
			var time = dt.toLocaleTimeString([], {hour: '2-digit'});;
			return dt = mm + '/' + dd + '/' + yyyy + ' | ' + time ;
		},
		formatTime: function(value) {
			var dt = new Date(value);
			return dt.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});;
		},
		shorten: function(string) {
			if(string.length > 15) {
				return string = string.substring(0,14)+"...";
			} else {
				return string;
			}
		}
	}
});