require('./bootstrap');

import axios from 'axios';
import { loadProgressBar } from 'axios-progress-bar';
window.Vue = require('vue');
import {store} from './store';

const products = new Vue({
    el: '#admin',
    data: {
    	sidenav: store.state.sidenav,
		notif: store.state.notification,
		validation: store.state.validation,
		confirm: store.state.confirm,
		pass_validation: store.state.pass_validation,
		profile: "",
		bool: "",
		update: false,
		changes: false,
		fields : ['first', 'last', 'email', 'user'],
		password: {
			old: "",
			new: "",
			confirm: "",
			show_new: false,
			show_confirm: false,
		},
        loads: {
            general: true,
			search: true,
			updates: false,
		},
    },
    created() {
		//axios progress bar package
		loadProgressBar();
		
		// tooltip
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		});
    	// document.getElementById("load").style.display = "none";
        this.sidenav.users = false;
		this.sidenav.dashboard = false;
		
    	// console.log('test');
    	this.loads.general = false;
		this.fetch();

		let inst = this;
		setTimeout(function(){
			inst.changes = false;
		}, 2000);
    },
    computed: {
    	imageURL: function (image) {
    		return this.url+'no-image.png';
		},
		preview_src: function() {
			let p = this.preview;
			return p.path + p.name + '.' + p.extension; 
		}
    },
    methods: {
	    fetch() {	        
	        // console.log(orderIn);
	        axios.get('/user/profile')
	            .then(response => {
					// console.log(response);
					let data = response.data;
					this.profile = {
						id: data.id,
						first: data.fname,
						last: data.lname,
						role: data.role,
						user: data.username,
						email: data.email,
					}
					// console.log(this.profile);
	            })
	            .catch(e => {
				  console.log(e);
				  this.notify('bg-danger', 'Something went wrong!');
            })
            //end axios
		},
		refreshPasswordFields() {
			let inst = this;
			inst.password = {
				old: "",
				new: "",
				confirm: "",
				show_new: false,
				show_confirm: false,
			};

			Object.keys(inst.pass_validation).forEach(function(key) {
				inst.pass_validation[key].invalid = false;
				inst.pass_validation[key].valid = false;
				inst.pass_validation[key].feedback = "";
			});
		},
		changePassword() {
			let inst = this;
			let error = 0;
			//check if new and confirm passwords are valid
			Object.keys(inst.pass_validation).forEach(el => {
				
				if(!inst.pass_validation[el].valid) {
					inst.validateInput(el);
					error++;
				}
			});
			
			//submit to backend for old password validation
			if(error > 0) {
				inst.notify('bg-danger', 'Pls enter required input fields!');
			}else {

				axios.post('/user/changePassword', 
					inst.password)
				.then(response => {
					// console.log(response);
					let data = response.data;
					if(data.status != 'success') {
						inst.notify('bg-warning', data.msg);
					}
					else {
						inst.refreshPasswordFields();
						inst.notify('bg-success', 'Password successfully changed!');
					}
					// 
				})
				.catch(e => {
					console.log(e);
					inst.notify('bg-danger', 'Something went wrong!');
				}); 
				
			}
		},
		refreshValidation(){
			let inst = this;

			inst.fields.forEach(element => {
				inst.validation[element].valid = false;
				inst.validation[element].invalid = false;
			});

			inst.update = false;
			inst.changes = false;
			this.fetch();
		},
		updateDb() {
			let user = this.profile;
			let inst = this;
			axios.post('/user/update', 
					{ 
						id: user.id,
						fname: user.first,
						lname: user.last,
						email: user.email,
						username: user.user
					})
				.then(response => {
					// console.log(response);
					inst.confirm.update = false;
					inst.refreshValidation();
					inst.notify('bg-success', 'Changes applied!');
				})
				.catch(e => {
					console.log(e);
					inst.notify('bg-danger', 'Something went wrong!');
				}); 
		},
		updateInfo() {
			let inst = this;
			inst.confirm.update = true;
			let user = inst.profile;

			setTimeout(function() {
				let error = 0;
				inst.fields.forEach(el => {
					if(inst.validation[el].invalid) {
						error++;
					}
				});

				if(error > 0) {
					inst.notify('bg-warning', 'Check input fields!');
				} else {
					inst.updateDb();
				}

				inst.confirm.update = false;

			}, 3000);
		},
		async checkDuplicate(field, input) {
			// console.log(bool);
			return await axios.get('/user/checkDuplicates', {
				params: {
				    field: field,
					input: input,
					update: true
				}
			})
			.then(response => {
			   return response.data;
				// bool = true;
			})
			.catch(e => {
			  console.log(e);
			});
		},
		validate(input, type) {
			if(type == 'email') {
				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				   return re.test(String(input).toLowerCase());
			}
			else if(type == 'alphanumonly') {
				let reg = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
				return reg.test(input);
			}
			else if(type == 'alphanum') {
					let reg = /^[a-zA-Z0-9_]*$/;
					return reg.test(input);
			}
		},
		async validateInput(input) {
			let v = this;

			if(v.profile[input] == "") {
				v.validation[input].invalid = true;
				v.validation[input].feedback = "This input field is required!"
			}
			else if(input == 'email') {
				v.bool = false;
				await v.checkDuplicate('email', v.profile[input]).then(data => { v.bool = data });

				if(!v.validate(v.profile[input], 'email')) {
					v.validation[input].invalid = true; 
					v.validation[input].feedback = "Invalid Email"
				}
				else if (v.bool) {
					v.validation[input].invalid = true; 
					v.validation[input].feedback = "Email already taken!"
				}
				else {
					valid();
				}
			}
			else if(input == 'old' && v.password.old == '') {
				v.pass_validation.old.invalid = true;
				v.pass_validation.old.valid = false;
				v.pass_validation.old.feedback = "Input field required!";
			}
			else if(input == 'new') {
				if(v.password.new == "") {
					v.pass_validation.new.invalid = true;
					v.pass_validation.old.valid = false;
					v.pass_validation.new.feedback = "Input field required!";
				}
				else if(!v.validate(v.password.new, 'alphanumonly')) {
					v.pass_validation.new.invalid = true;
					v.pass_validation.old.valid = false;
					v.pass_validation.new.feedback = "Password must contain a letter and a number.";
				}
				else if(v.password.new.length < 8) {
					v.pass_validation.new.invalid = true;
					v.pass_validation.old.valid = false;
					v.pass_validation.new.feedback = "Minimum of 8 characters";
				}
				else {
					validPass();
				}            
			}
			else if(input == 'confirm') {
				if(v.password.confirm == '') {
					v.pass_validation.confirm.invalid = true;
					v.pass_validation.old.valid = false;
					v.pass_validation.confirm.feedback = "Input field required!";
				}
				else if (v.password.new != v.password.confirm) {
					v.pass_validation.confirm.invalid = true;
					v.pass_validation.old.valid = false;
					v.pass_validation.confirm.feedback = "Must match with New password";
				}
				else {
					validPass();
				}
				
			}
			else if(input == 'user'){
				v.bool = false;
				await v.checkDuplicate('username', v.profile[input]).then(data => { v.bool = data });

				if(!v.validate(v.profile[input], 'alphanum')) {
						v.validation[input].invalid = true;
						v.validation[input].feedback = "Only Alphanumeric Characters and Underscore is allowed!"; 
				}
				else if (v.profile[input].length < 6) {
						v.validation[input].invalid = true;
						v.validation[input].feedback = "Minimum of 6 characters"; 
				}
				else if (v.bool) {
						v.validation[input].invalid = true;
						v.validation[input].feedback = "Username already taken!"; 
				}
				else {
						valid();
				}
				
			}
			else if(input == 'old' || input == 'new' || input == 'confirm') {
				validPass();
			}
			else {
				//check for duplicate
				valid();
			}
	
			function valid() {
				v.validation[input].invalid = false;
				v.validation[input].valid = true;
				v.validation[input].feedback = "Validated!"
			}

			function validPass() {
				v.pass_validation[input].invalid = false;
				v.pass_validation[input].valid = true;
				v.pass_validation[input].feedback = "Validated!"
			}
			
		},
        leave: function (el, done) {
	        $('.sidebar-mobile').addClass('slideOutLeft');
	        setTimeout(function() {
	            done();
	        }, 1000);
		},
		openModal(modalId) {
	        $('#'+modalId).modal('toggle');
	    },
	    close(e) {
			// console.log(e.target.id);
			if(e == 'close') {
				$('#showUser').modal('toggle');
				this.setClose('close');
			}
	
			try {
			   if(e.target.id == 'viewProduct' || e.target.id == 'close-x') {
					this.setClose('close');
				} 
			}
			catch(err) {
				console.log(err);
				this.notify('bg-danger', 'Something went wrong!');
			}
		},
		notify(status, message, delay) {
			if(delay == undefined){ delay = 1500 };
			let inst = this;
			//status = ['bg-warning', 'bg-info', and etc]
			inst.notif.status = status;
			inst.notif.message = message;
			inst.notif.display = true;
			setTimeout(function() {
				inst.notif.display = false;
			}, delay);
		}
    },
    watch: {
	    profile: {
	        handler: function(p) {
	            this.changes = true;
	        },
	        deep: true
		},
    },
    filters: {
    	currency: function(price) {
			// return price;
			if(price) {
				return '₱ ' + price.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
			}
    		// .toFixed(2)
		},
		formatDate: function(value) {
			var dt = new Date(value);
            var dd = dt.getDate();
            var mm = dt.getMonth()+1;
            var yyyy = dt.getFullYear();
            var time = dt.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});;
            return dt = mm + '/' + dd + '/' + yyyy + ' - ' + time;
        },
		shorten: function(string) {
			if(string.length > 20) {
				return string = string.substring(0,19)+"...";
			} else {
				return string;
			}
		}
    }
});