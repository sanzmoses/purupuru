require('./bootstrap');

import axios from 'axios';
import { loadProgressBar } from 'axios-progress-bar';
window.Vue = require('vue');
import {store} from './store';

const app = new Vue({
    el: '#admin',
    data: {
        sidenav: store.state.sidenav,
		notif: store.state.notification,
		phrases: store.state.phrases,
		validation: {
			number: {
				invalid: false,
				valid: false,
				feedback: '',
			},
			code: {
				invalid: false,
				valid: false,
				feedback: '',
			}
		},
        changes: false,
        loads: {
            general: true,
			search: true,
			updates: false,
			create: false
		},
		confirm: {
			delete: false,
			remove: false,
		},
		modalOpen: "",
		register: {
			number: "",
			code: "",
			codeInput: "",
			verify: false,
		},
		registered: "",
		loading: "",
		toDelete: "",
    },
    created() {
    	//axios progress bar package
		loadProgressBar();
		
		// tooltip
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		});
        
    	// console.log('test');
        this.loads.general = false;
        this.sidenav.dashboard = false;
        this.sidenav.numbers = true;

		let inst = this;
		inst.fetch();
		setTimeout(function(){
			inst.changes = false;
			console.log(inst.registered);
		}, 2000);
    },
    methods: {
		fetch() {
			let inst = this;
			axios.get('/number/user')
			  .then(response => {
				let data = response.data;
				inst.registered = data.numbers;
			  })
			  .catch(e => {
				console.log(e);
			  })
		},
		registerNum($status) {
			let inst = this;
			if($status == 'sendcode') {
				inst.validateInput('number');
				if(!inst.validation.number.invalid) {
					inst.register.verify = true;
					//generate code
					inst.register.code = Math.random().toString(36).substring(7);
					console.log(inst.register.code);
				}
				else {
					console.log('not valid');
				}
			}
			else if($status == 'verify') {
				inst.validateInput('code');
				if(!inst.validation.code.invalid) {
					inst.saveToDb();
					console.log('verified');
				}
				else {
					console.log('not valid');
				}
				
			}
			else {
				this.openModal('number-registration');
			}
		},
		saveToDb() {
			let inst = this;
			axios.post('/number/register', {
				number: inst.register.number,
			  }).then(response => {
				inst.close('close');
				inst.notify('bg-success', 'Successfully Registered!');
				inst.fetch();
			  }).catch(e => {
				console.log(e);
			  })
		},
		deleteNum(index) {
			this.toDelete = this.registered[index].id;
			this.openModal('confirmation');
		},
		confirmed(type) {
			let inst = this;
			if(type == 'delete') {
				axios.post('/number/delete/'+inst.toDelete)
				.then(response => {
					inst.notify('bg-success', 'Successfully Deleted!');
					inst.fetch();
				}).catch(e => {
					console.log(e);
				});
			}
		},
		validate(input, type) {
			let reg = '';
			if(type == 'alphanumonly') {
			    reg = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
			}
			else if(type == 'alphanum') {
				reg = /^[a-zA-Z0-9_]*$/;
			}
			else if(type == 'numonly') {
				reg = /^[0-9]+$/;
			}

			return reg.test(input);
		},
		async validateInput(input) {
			let v = this;
	
			if(input == 'number') {
				console.log(input);
				if(v.register.number == "") {
					console.log('wtf');
					v.validation[input].invalid = true;
					v.validation[input].feedback = "This input field is required!"
				}
				else if(!v.validate(v.register.number, 'numonly')) {
					v.validation[input].invalid = true;
					v.validation[input].feedback = "Only numeric values are allowed!"
				}
				else if (v.register.number.length != 10) {
					v.validation[input].invalid = true;
					v.validation[input].feedback = "Invalid PH number length"
				}else {
					valid();
				}
			}
			else if(input == 'code') {
				if(v.register.codeInput == '') {
					v.validation[input].invalid = true;
					v.validation[input].feedback = "This input field is required!"
				}
				else if(v.register.code !== v.register.codeInput) {
					v.validation[input].invalid = true;
					v.validation[input].feedback = "Incorrect Code"
				} else {
					valid();
				}
			}
			else {
				valid();
			}
	
			function valid() {
				v.validation[input].invalid = false;
				v.validation[input].valid = true;
				v.validation[input].feedback = "Validated!"
			}
			// console.log("yeah");
				
		},
        leave: function (el, done) {
	        $('.sidebar-mobile').addClass('slideOutLeft');
	        setTimeout(function() {
	            done();
	        }, 1000);
		},
		openModal(modalId) {
			this.modalOpen = modalId;
	        $('#'+modalId).modal('toggle');
	    },
	    close(e) {
			// console.log(e.target.id);
			if(e == 'close') {
				$('#'+this.modalOpen).modal('toggle');
				this.setClose('close');
			}
			else {
				try {
				   if(e.target.id == this.modalOpen || e.target.id == 'close-x') {
						this.setClose('close');
					} 
				}
				catch(err) {
					console.log(err);
					this.notify('bg-danger', 'Something went wrong!');
				}
			}
		},
		setClose() {
			let inst = this;
			setTimeout(function(){
				inst.register.verify = false;
			}, 500);
			
			inst.confirm.remove = false;
			// console.log('closing');
		},
		notify(status, message, delay) {
			if(delay == undefined){ delay = 1500 };
			let inst = this;
			//status = ['bg-warning', 'bg-info', and etc]
			inst.notif.status = status;
			inst.notif.message = message;
			inst.notif.display = true;
			setTimeout(function() {
				inst.notif.display = false;
			}, delay);
		}
	},
	filters: {
		formatDate: function(value) {
			var dt = new Date(value);
			var dd = dt.getDate();
			var mm = dt.getMonth()+1;
			var yyyy = dt.getFullYear();
			var time = dt.toLocaleTimeString([], {hour: '2-digit'});;
			return dt = mm + '/' + dd + '/' + yyyy + ' | ' + time ;
		},
		formatTime: function(value) {
			var dt = new Date(value);
			return dt.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});;
		},
		shorten: function(string) {
			if(string.length > 15) {
				return string = string.substring(0,14)+"...";
			} else {
				return string;
			}
		}
	}
});