require('./bootstrap');

import axios from 'axios';
import { loadProgressBar } from 'axios-progress-bar';
window.Vue = require('vue');
import {store} from './store';

const products = new Vue({
    el: '#admin',
    data: {
    	sidenav: store.state.sidenav,
		notif: store.state.notification,
		validation: store.state.product_validation,
		confirm: store.state.confirm,
		messages: "",
		message: "",
		search: '',
        perpage: 10,
        order: {
            by: 'id',
            in: true
        },
        paginateArr: [],
        paginate: {
            lastpage: '',
            from: '',
            to: '',
            curpage: '',
            dividedLinks: '',
        },
        prev: true,
        next: false,
        back: true,
        forward: false,
        group: {
            limit: '',
            current: 1,
            jumps: 1,
            divisor: 5
        },
        loads: {
            general: true,
			search: true,
			updates: false,
		},
		trigger: '',
        file: [],
    },
    created() {
		//axios progress bar package
		loadProgressBar();
		
		// tooltip
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		});
    	// document.getElementById("load").style.display = "none";
        this.sidenav.users = false;
        
    	// console.log('test');
    	this.loads.general = false;
    	this.fetch();
    },
    computed: {
    	imageURL: function (image) {
    		return this.url+'no-image.png';
		},
		preview_src: function() {
			let p = this.preview;
			return p.path + p.name + '.' + p.extension; 
		}
    },
    methods: {
	    fetch(num) {
	        // fetch data
	        // console.log("passed = "+num);
	        if(num == this.paginate.curpage) {
	            return;
	        }

	        let diz = this;
	        let orderIn; 
	        (diz.order.in) ? orderIn = 'asc' : orderIn = 'desc';
	        
	        // console.log(orderIn);
	        axios.get('/message/all', 
	                { params: 
	                    {
	                        paginate: this.perpage,
	                        string: this.search,
	                        page: num,
	                        orderby: this.order.by,
	                        orderin: orderIn
	                    } 
	                })
	            .then(response => {
					
	                //setting up shortcuts
	                let data = response.data;
	                let paginate = this.paginate;
	                let group = this.group;
	                let divisor = this.group.divisor;

	                this.messages = data.data;
	                paginate.lastpage = data.last_page;
	                paginate.from = data.from;
	                paginate.to = data.to;
	                paginate.curpage = data.current_page;
	                group.limit = Math.ceil(data.last_page / divisor);

	                // console.log(this.paginateArr.length);
	                //cleaning up pagination num links
	                if(this.paginateArr.length > 0){
	                    this.paginateArr = [];
	                }

	                //setting up pagination links
	                var num = group.jumps;
	                // console.log('num = '+num);
	                if(data.last_page % divisor > 0 && group.limit == group.current) {
	                    divisor = data.last_page % divisor
	                }

	                for(var x = 0; x < divisor; x++){
	                    let p = {
	                        num: num,
	                        active: false,
	                    };

	                    if(num == data.current_page) {
	                        p.active = true;
	                    }

	                    this.paginateArr.push(p);
	                    num++;
	                }
	                // console.log(num);
	                // console.log(this.paginate);
	            })
	            .catch(e => {
				  console.log(e);
				  this.notify('bg-danger', 'Something went wrong!');
            })
            //end axios
		},
		setThumbnail() {
			let inst = this;
			axios.post('/product/setThumbnail/'+inst.product.id, 
			{
					new: inst.preview.id,
					current: inst.product.images[0].id,
			})
			.then(response => {
				inst.product.images = response.data;
				inst.preview_index = 0;
			})
			.catch(e => {
				console.log(e);
				this.notify('bg-danger', 'Something went wrong!');
			});
		},
		showMessage(id, index, event) {
			//if delete button is clicked
			let e = event.target;
			let value = ['', ''];

			try {
				value[1] = e.className;
				value[0] = e.children[0].className;
			}
			catch(e) {
				// console.log(e);
			}

			if(value[0] == 'fa fa-trash' || value[1] == 'fa fa-trash') {
				if(window.confirm('Are you sure?')) {
										
					//delete record in db
					axios.post('/message/delete/'+id)
					.then(response => {
						this.fetch();
						this.notify('bg-success', 'Record Deleted!');
					})
					.catch(e => {
						// console.log(e);
						this.notify('bg-danger', 'Something went wrong!');
					});
				}
				return ;
			}

			this.message = this.messages[id-1];
			this.openModal('viewMessage');
			
		},
		async checkDuplicate(field, input) {
			// console.log(bool);
			return await axios.get('/product/checkDuplicates', {
				params: {
				    field: field,
					input: input 
				}
			})
			.then(response => {
			   return response.data;
				// bool = true;
			})
			.catch(e => {
			  console.log(e);
			})
		},
		validate(input, type) {
			let reg;
			if(type == 'alphanumonly') reg = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
			//alphanum and whitespaces
			if(type == 'alphanum') reg = /^[a-zA-Z0-9\- ]*$/;
			if(type == 'alpha') reg = /^[a-zA-Z]*$/;
			if(type == 'numeric') reg = /^[0-9.]*$/;
			return reg.test(input);
		},
		async validateInput(input) {
			let v = this;
	
			if(v.registration[input] == "" && input != 'description') {
				v.validation[input].invalid = true;
				v.validation[input].feedback = "This input field is required!"
			}
			else if(input == 'name') {
				v.bool = false;
				await v.checkDuplicate(input, v.registration[input]).then(data => { v.bool = data });
	
				if(!v.validate(v.registration[input], 'alphanum')) {
					v.validation[input].invalid = true; 
					v.validation[input].feedback = "Invalid Input. Only alphanumerics are allowed!"
				}
				else if (v.bool) {
					v.validation[input].invalid = true; 
					v.validation[input].feedback = "Name already taken!"
				}
				else {
					valid();
				}
			}
			else if(input == 'brand') {
				if(!v.validate(v.registration[input], 'alphanum')) {
					v.validation[input].invalid = true; 
					v.validation[input].feedback = "Invalid Input. Only alphanumerics are allowed!"
				}
				else {
					valid();
				}
			}
			else if(input == 'type'){
				if(!v.validate(v.registration[input], 'alpha')) {
						v.validation[input].invalid = true;
						v.validation[input].feedback = "Only Alphabetic Characters are allowed!"; 
				}
				else {
					valid();
				}
			}
			// else if(input == 'price') {
			// 	if(!v.validate(v.registration[input], 'numeric')) {
			// 		v.validation[input].invalid = true;
			// 		v.validation[input].feedback = "Invalid Characters!"; 
			// 	}
			// 	else {
			// 		valid();
			// 	}
			// }
			else {
				valid();
			}
	
			function valid() {
				v.validation[input].invalid = false;
				v.validation[input].valid = true;
				v.validation[input].feedback = "Validated!"
			}
			// console.log("yeah");
		},
        skip(num) {
	        this.group.jumps += num;
	        (num > 1) ? this.group.current++: this.group.current--;
	        this.fetch(this.group.jumps);
	    },
	    step(page) {
	        if(this.paginateArr[4] !== undefined) {
	            if (this.paginate.curpage == this.paginateArr[4].num && page > 0) {
	                this.skip(this.group.divisor);
	                return;
	            } 
	        }
	        
	        if (this.paginate.curpage == this.paginateArr[0].num && page < 0) {
	            this.skip(-this.group.divisor);
	            return;
	        }
	        
	        this.fetch(this.paginate.curpage + page);
	        
	    },
        leave: function (el, done) {
	        $('.sidebar-mobile').addClass('slideOutLeft');
	        setTimeout(function() {
	            done();
	        }, 1000);
		},
		openModal(modalId) {
	        $('#'+modalId).modal('toggle');
	    },
	    close(e) {
			// console.log(e.target.id);
			if(e == 'close') {
				$('#showUser').modal('toggle');
				this.setClose('close');
			}
	
			try {
			   if(e.target.id == 'viewProduct' || e.target.id == 'close-x') {
					this.setClose('close');
				} 
			}
			catch(err) {
				console.log(err);
				this.notify('bg-danger', 'Something went wrong!');
			}
		},
		setClose(set) {
			this.update.edit = true;
			this.update.save = false;
			this.preview_index = 0;

			if(this.product.images.length >= 5 && set == 'toggle') {
				this.notify('bg-info', 'You can add up to 5 images only!');
				return;
			}

			if(this.confirm.update == false && set == 'toggle') {
				this.confirm.update = true;
				return ;
			} else {
				set = 'close';
			}

			if(set == 'close') {
				this.confirm.update = false;
				this.confirm.fromImages = false;
			}
		},
		notify(status, message, delay) {
			if(delay == undefined){ delay = 1500 };
			let inst = this;
			//status = ['bg-warning', 'bg-info', and etc]
			inst.notif.status = status;
			inst.notif.message = message;
			inst.notif.display = true;
			setTimeout(function() {
				inst.notif.display = false;
			}, delay);
		}
    },
    watch: {
	    perpage: function() {
	    	this.fetch();
	    },
        search: function() {
	        let inst = this;
			inst.loads.search = false;
			clearTimeout(inst.trigger);
	        inst.trigger = setTimeout(function() {
	            inst.loads.search = true;
	            inst.fetch();
	        }, 1500);
		},
		searchImg: function() {
	        let inst = this;
			inst.loads.searchImg = false;
			clearTimeout(inst.trigger);
	        inst.trigger = setTimeout(function() {
	            inst.loads.searchImg = true;
	            inst.fetchImgs();
	        }, 1500);
		},
	    paginate: {
	        handler: function(p) {
	            (p.curpage > 1) ? this.prev = false : this.prev = true;
	            (p.curpage == p.lastpage) ? this.next = true: this.next = false;
	        },
	        deep: true
	    },
	    group: {
	        handler: function(p) {
	            (p.current > 1) ? this.back = false : this.back = true;
	            (p.current == p.limit) ? this.forward = true: this.forward = false;
	        },
	        deep: true
		},
    },
    filters: {
    	currency: function(price) {
			// return price;
			if(price) {
				return '₱ ' + price.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
			}
    		// .toFixed(2)
		},
		formatDate: function(value) {
			var dt = new Date(value);
            var dd = dt.getDate();
            var mm = dt.getMonth()+1;
            var yyyy = dt.getFullYear();
            var time = dt.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});;
            return dt = mm + '/' + dd + '/' + yyyy + ' - ' + time;
        },
		shorten: function(string) {
			if(string.length > 20) {
				return string = string.substring(0,19)+"...";
			} else {
				return string;
			}
		}
    }
});