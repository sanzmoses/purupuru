require('./bootstrap');

import axios from 'axios';
import { loadProgressBar } from 'axios-progress-bar';
window.Vue = require('vue');
import {store} from './store';

const app = new Vue({
    el: '#admin',
    data: {
        sidenav: store.state.sidenav,
        notif: store.state.notification,
		changes: false,
		logs: "",
        loads: {
            general: true,
			search: true,
			updates: false,
			list: false
		},
		search: '',
        perpage: 10,
        order: {
            by: 'id',
            in: true
        },
        paginateArr: [],
        paginate: {
            lastpage: '',
            from: '',
            to: '',
            curpage: '',
            dividedLinks: '',
        },
        prev: true,
        next: false,
        back: true,
        forward: false,
        group: {
            limit: '',
            current: 1,
            jumps: 1,
            divisor: 5
		},
    },
    created() {
    	//axios progress bar package
		loadProgressBar();
		
		// tooltip
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		});
        
    	// console.log('test');
        this.loads.general = false;
        this.sidenav.dashboard = false;
        this.sidenav.sms = true;

		let inst = this;
		setTimeout(function(){
			inst.changes = false;
		}, 2000);

		inst.fetch();
    },
    methods: {
		fetch(num) {
			this.loads.list = true;
			// fetch data
			// console.log("passed = "+num);
			if(num == this.paginate.curpage) {
				return;
			}
	
			let diz = this;
			let orderIn; 
			(diz.order.in) ? orderIn = 'asc' : orderIn = 'desc';
				// console.log('page called: '+num);
			axios.get('/logs/user', 
					{ params: 
						{
							paginate: this.perpage,
							string: this.search,
							page: num,
							orderby: this.order.by,
							orderin: orderIn
						} 
					})
				.then(response => {
					//setting up shortcuts
					let data = response.data;
					let paginate = this.paginate;
					let group = this.group;
					let divisor = this.group.divisor;
	
					this.logs = data.data;
					paginate.lastpage = data.last_page;
					paginate.from = data.from;
					paginate.to = data.to;
					paginate.curpage = data.current_page;
					group.limit = Math.ceil(data.last_page / divisor);
	
	
					// console.log(this.paginateArr.length);
					//cleaning up pagination num links
					if(this.paginateArr.length > 0){
						this.paginateArr = [];
					}
	
					//setting up pagination links
					var num = group.jumps;
					// console.log('num = '+num);
					if(data.last_page % divisor > 0 && group.limit == group.current) {
						divisor = data.last_page % divisor
					}
	
					for(var x = 0; x < divisor; x++){
						let p = {
							num: num,
							active: false,
						};
	
						if(num == data.current_page) {
							p.active = true;
						}
	
						this.paginateArr.push(p);
						num++;
					}
					// console.log(num);
					// console.log(this.paginate);
				})
				.catch(e => {
				  console.log(e);
				})
				//end axios
		},
		skip(num) {
			this.group.jumps += num;
			(num > 1) ? this.group.current++: this.group.current--;
			this.fetch(this.group.jumps);
		},
		step(page) {
			if(this.paginateArr[4] !== undefined) {
				if (this.paginate.curpage == this.paginateArr[4].num && page > 0) {
					this.skip(this.group.divisor);
					return;
				} 
			}
			
			if (this.paginate.curpage == this.paginateArr[0].num && page < 0) {
				this.skip(-this.group.divisor);
				return;
			}
			
			this.fetch(this.paginate.curpage + page);
			
		},
        leave(el, done) {
	        $('.sidebar-mobile').addClass('slideOutLeft');
	        setTimeout(function() {
	            done();
	        }, 1000);
		},
		openModal(modalId) {
	        $('#'+modalId).modal('toggle');
	    },
	    close(e) {
			// console.log(e.target.id);
			if(e == 'close') {
				$('#showUser').modal('toggle');
				this.setClose('close');
			}
	
			try {
			   if(e.target.id == 'viewProduct' || e.target.id == 'close-x') {
					this.setClose('close');
				} 
			}
			catch(err) {
				console.log(err);
				this.notify('bg-danger', 'Something went wrong!');
			}
		},
		notify(status, message, delay) {
			if(delay == undefined){ delay = 1500 };
			let inst = this;
			//status = ['bg-warning', 'bg-info', and etc]
			inst.notif.status = status;
			inst.notif.message = message;
			inst.notif.display = true;
			setTimeout(function() {
				inst.notif.display = false;
			}, delay);
		}
	},
	watch: {
		perpage: function() {
			this.fetch();
		},
		search: function() {
			this.loads.search = false;
			let inst = this;
			setTimeout(function() {
				inst.loads.search = true;
				inst.fetch();
			}, 1500);
		},
		paginate: {
			handler: function(p) {
				(p.curpage > 1) ? this.prev = false : this.prev = true;
				(p.curpage == p.lastpage) ? this.next = true: this.next = false;
			},
			deep: true
		},
		group: {
			handler: function(p) {
				(p.current > 1) ? this.back = false : this.back = true;
				(p.current == p.limit) ? this.forward = true: this.forward = false;
			},
			deep: true
		}
	}
	
});