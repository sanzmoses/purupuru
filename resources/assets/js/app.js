
require('./bootstrap');

import axios from 'axios';
import { loadProgressBar } from 'axios-progress-bar';
window.Vue = require('vue');
import {store} from './store';
import Prism from 'prismjs';

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
const app = new Vue({

    el: '#app',
    data: {
		notif: store.state.notification,
		nav: store.state.nav,
        categories: {},
		active_category: "",
		category_list: "",
        number: 5,
        scanning: true,
        activity: 0,
        products: "",
		product: "",
		product_index: "",
		products_imgs: [],
		product_imgs: [],
		preview: "",
		preview_index: 0,
    	url: "/storage/",
        type: 'all',
        brand: 'all',
		search: '',
		searchImg: '',
		name: "",
		email: "",
		phone: "",
		message: "",
        loads: {
            general: true,
			search: true,
			contact: false,
			perpage: false,
		},
		confirm: {
            delete: false,
			update: false,
			loading: false,
			fromImages: false,
			register: false,
		},
		route: "",
		validation: store.state.validation,
		confirm: store.state.confirm,
		pass_validation: store.state.pass_validation,
		profile: "",
		bool: "",
		update: false,
		changes: false,
		fields : ['first', 'last', 'email', 'user'],
		fixed: false

    },
    created() {
		//axios progress bar package
		loadProgressBar();

        $(function () {
			$('[data-toggle="tooltip"]').tooltip()
		});

		let str = window.location.href;
		str = str.substr(str.lastIndexOf("/") + 1, str.length - 1);
		// this.nav[str] = true;
		this.route = str;
		
		// if(str != '' && str != 'home' && str != 'register') {
		// 	$('footer').addClass('fixed-bottom');
		// 	$('.vessel').css('height', '100vh');
		// }

		window.addEventListener("scroll", this.navbarAnimation);

    },
    methods: {
		navbarAnimation() {
			let scroll = $(window).scrollTop();
			if(scroll >= 300) {
				this.fixed = true;
			}
			else {
				this.fixed = false;
			}
		},
		accordingTo(type, brand) {
			this.search = '';
			this.type = type;
			this.brand = brand;

			this.fetch();
		},
		validate(input, type) {
			if(type == 'email') {
				 var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
					return re.test(String(input).toLowerCase());
			}
			else if(type == 'alphanumonly') {
			   	let reg = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
				return reg.test(input);
			}
			else if(type == 'alphanum') {
				let reg = /^[a-zA-Z0-9_]*$/;
				return reg.test(input);
			}
			else if(type == 'num') {
				let reg = /^[0-9]*$/;
				return reg.test(input);
			}
			
		},
		validateContact() {
			console.log('in');
			let error = 0;

			if(this.name.length < 2) {
				this.notify('bg-warning', 'Invalid Name!');
				error++;
			}
			
			if(this.email.length < 8 || !this.validate(this.email, 'email')) {
				this.notify('bg-warning', 'Invalid Email!');
				error++;
			}
			
			if(this.phone != "") { 
				if(this.phone.length < 7 || !this.validate(this.phone, 'num')) {
					this.notify('bg-warning', 'Invalid Number!');
					error++;
				}
			}
			
			if(this.message.length < 2 ) {
				this.notify('bg-warning', 'Invalid Message!');
				error++;
			}

			if(error == 0) {
				console.log('Validation pass!');
				this.loads.contact = true;
				let inst = this;
				setTimeout(() => {
					inst.contact();	
				}, 2000);
			}
		},
		contact() {
			this.loads.contact = false;
			axios.post('/contact/create', {
				name: this.name,
				email: this.email,
				phone: this.phone,
				message: this.message,
			}).then(response => {
				// console.log(response);
				this.loads.contact = false;
				this.notify('bg-success', 'Message delivered!');
				this.name = "";
				this.email = "";
				this.phone = "";
				this.message = "";
			}).catch(e => {
				console.log(e);
				this.loads.contact = false;
				this.notify('bg-danger', 'Pls try again later!');
			})
		},
		refreshValidation(){
			let inst = this;

			inst.fields.forEach(element => {
				inst.validation[element].valid = false;
				inst.validation[element].invalid = false;
			});

			inst.update = false;
			inst.changes = false;
			this.fetchProfile();
		},
        openModal(modalId) {
	        $('#'+modalId).modal('toggle');
	    },
	    close(e) {
			// console.log(e.target.id);
			if(e == 'close') {
				$('#showUser').modal('toggle');
				this.setClose('close');
			}
	
			try {
			   if(e.target.id == 'viewProduct' || e.target.id == 'close-x') {
					this.setClose('close');
				} 
			}
			catch(err) {
				console.log(err);
				this.notify('bg-danger', 'Something went wrong!!');
			}
		},
		setClose(set) {
			if(this.confirm.update == false && set == 'toggle') {
				this.confirm.update = true;
				return ;
			} else {
				set = 'close';
			}

			if(set == 'close') {
				this.confirm.update = false;
				this.confirm.fromImages = false;
			}
		},
        notify(status, message, delay) {
			if(delay == undefined){ delay = 1500 };
			let inst = this;
			//status = ['bg-warning', 'bg-info', and etc]
			inst.notif.status = status;
			inst.notif.message = message;
			inst.notif.display = true;
			setTimeout(function() {
				inst.notif.display = false;
			}, delay);
		},
    },
    filters: {
		formatDate: function(value) {
			var dt = new Date(value);
            var dd = dt.getDate();
            var mm = dt.getMonth()+1;
            var yyyy = dt.getFullYear();
            // var time = dt.toLocaleTimeString();
            return dt = mm + '/' + dd + '/' + yyyy;
		},
    }

});