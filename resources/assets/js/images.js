require('./bootstrap');

import axios from 'axios';
import { loadProgressBar } from 'axios-progress-bar';
window.Vue = require('vue');
import {store} from './store';

const imgs = new Vue({
    el: '#admin',
    data: {
    	sidenav: store.state.sidenav,
		notif: store.state.notification,
		progress_bar: store.state.progress_bar,
		confirm: {
            delete: false,
            update: false
        },
		gallery: true,
    	photos: [],
    	photo: {
    		path: 'http://',
			name: 'placehold',
			extension: 'it/900x600'
    	},
    	url: "/storage/",
        role: 'all',
        search: '',
        perpage: 12,
        order: {
            by: 'id',
            in: true
        },
        paginateArr: [],
        paginate: {
            lastpage: '',
            from: '',
            to: '',
            curpage: '',
            dividedLinks: '',
        },
        prev: true,
        next: false,
        back: true,
        forward: false,
        group: {
            limit: '',
            current: 1,
            jumps: 1,
            divisor: 5
        },
        loads: {
            general: true,
			search: true,
			import: false,
        },
        file: [],
    },
    created() {
		//axios progress bar package
		loadProgressBar();
		
    	// document.getElementById("load").style.display = "none";
    	// this.sidenav.products = false;
    	this.sidenav.users = false;
		this.sidenav['images'] = true;
		
    	this.loads.general = false;
    	this.fetch();
    },
    methods: {
	    fetch(num) {
	        // fetch data
	        // console.log("passed = "+num);
	        if(num == this.paginate.curpage) {
	            return;
	        }
		
	        let diz = this;
	        let orderIn; 
	        (diz.order.in) ? orderIn = 'asc' : orderIn = 'desc';
	        
	        // console.log(orderIn);
	        axios.get('/images/list', 
	                { params: 
	                    {
	                        paginate: this.perpage,
	                        role: this.role,
	                        string: this.search,
	                        page: num,
	                        orderby: this.order.by,
	                        orderin: orderIn
	                    } 
	                })
	            .then(response => {
	                // console.log(response);
	                // setting up shortcuts
	                let data = response.data;
	                let paginate = this.paginate;
	                let group = this.group;
	                let divisor = this.group.divisor;

	                this.photos = data.data;
	                paginate.lastpage = data.last_page;
	                paginate.from = data.from;
	                paginate.to = data.to;
	                paginate.curpage = data.current_page;
	                group.limit = Math.ceil(data.last_page / divisor);
					
					(this.photos.length > 0)? diz.gallery = true : diz.gallery = false;
	                //cleaning up pagination num links
	                if(this.paginateArr.length > 0){
	                    this.paginateArr = [];
	                }

	                //setting up pagination links
	                var num = group.jumps;
	                // console.log('num = '+num);
	                if(data.last_page % divisor > 0 && group.limit == group.current) {
	                    divisor = data.last_page % divisor
	                }

	                for(var x = 0; x < divisor; x++){
	                    let p = {
	                        num: num,
	                        active: false,
	                    };

	                    if(num == data.current_page) {
	                        p.active = true;
	                    }

	                    this.paginateArr.push(p);
	                    num++;
	                }
	                // console.log(num);
	                // console.log(this.paginate);
	            })
	            .catch(e => {
	              console.log(e);
            })
            //end axios
        },
        skip(num) {
	        this.group.jumps += num;
	        (num > 1) ? this.group.current++: this.group.current--;
	        this.fetch(this.group.jumps);
	    },
	    step(page) {
	        if(this.paginateArr[4] !== undefined) {
	            if (this.paginate.curpage == this.paginateArr[4].num && page > 0) {
	                this.skip(this.group.divisor);
	                return;
	            } 
	        }
	        
	        if (this.paginate.curpage == this.paginateArr[0].num && page < 0) {
	            this.skip(-this.group.divisor);
	            return;
	        }
	        
	        this.fetch(this.paginate.curpage + page);
	        
	    },
        leave: function (el, done) {
	        $('.sidebar-mobile').addClass('slideOutLeft');
	        setTimeout(function() {
	            done();
	        }, 1000);
	    },
	    importFile() {
	    	let formData = new FormData();
	    	let inst = this;
			let error = "";
			inst.loads.import = true;
		    // var file = document.getElementById('file');
            for(var i = 0; i < this.file.length; i++ ){
	          let file = this.file[i];
	        //   console.log(file);
	          if(file.type != "image/jpeg" && file.type != "image/png") {
	          	console.log('Invalid file: ' + file.name);
	          	error = "Invalid File!";
	          }

	          if(file.size > 550000) {
				  error = "File ("+file.name+") too large!";
				  i = this.file.length;
	          }

	          formData.append('file[' + i + ']', file);
	        }

	        if(error != "") {
				inst.loads.import = false;
	        	console.log('Invalid File');
	        	inst.notify('bg-warning', error);
	        	inst.clearInput();
	        } 
	        else {
	        	axios.post( '/images/import',
					formData,
						{
						headers: {
						    'Content-Type': 'multipart/form-data'
						}
					}
		        ).then(function(response){
					// console.log(response);
					inst.loads.import = false;
					inst.clearInput();
					inst.fetch();
					inst.notify('bg-success', 'File Uploaded!');
		        })
		        .catch(function(){
		        	inst.notify('bg-danger', 'File Upload Error!');
          			console.log('Upload Failed!');
		        });
	        }
            
	    },
		handleFilesUpload(){
			//get files from input using refs and register into the instance
			let uploadedFiles = this.$refs.file.files;
			for( var i = 0; i < uploadedFiles.length; i++ ){
	          this.file.push(uploadedFiles[i]);
	        }
		},
		clearInput(){
			//delete from vue instance and in input
			this.file = [];
			document.getElementById('file').value = "";
		},
		viewPhoto(index) {
			this.photo = this.photos[index];
			this.openModal('viewPhoto');
		},
		deletePhoto(id) {
			console.log(id);
			let inst = this;
			let foo = confirm('Are you sure?');
			if(foo) {
				axios.post( '/image/delete',
					{
						id: id
					}
		        ).then(function(response){
					console.log(response);
					inst.fetch();
					inst.notify('bg-success', 'Image Deleted!');
		        })
		        .catch(function(){
		        	inst.notify('bg-danger', 'DB Error!');
          			console.log('Upload Failed!');
		        });
			}
		},
		notify(status, message, delay) {
			if(delay == undefined){ delay = 1500 };
			let inst = this;
			inst.notif.status = status;
			inst.notif.message = message;
			inst.notif.display = true;
			setTimeout(function() {
				inst.notif.display = false;
			}, delay);
		},
		openModal(modalId) {
	        $('#'+modalId).modal('toggle');
	    },
	    close(event) {
	    	// console.log(event);
	    }
    },
    watch: {
	    perpage: function() {
	    	this.fetch();
	    },
        search: function() {
	        this.loads.search = false;
	        let inst = this;
	        setTimeout(function() {
	            inst.loads.search = true;
	            inst.fetch();
	        }, 1500);
    	},
	    paginate: {
	        handler: function(p) {
	            (p.curpage > 1) ? this.prev = false : this.prev = true;
	            (p.curpage == p.lastpage) ? this.next = true: this.next = false;
	        },
	        deep: true
	    },
	    group: {
	        handler: function(p) {
	            (p.current > 1) ? this.back = false : this.back = true;
	            (p.current == p.limit) ? this.forward = true: this.forward = false;
	        },
	        deep: true
	    }
    },
    filters: {
    	currency: function(price) {
    		return '₱ ' + price.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    		// .toFixed(2)
    	},
    	filename: function(name, ex) {
    		return name+'.'+extension;
    	}
    }
});