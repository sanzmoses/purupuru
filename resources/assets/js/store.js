require('./bootstrap');
window.Vue = require('vue');
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    validation: {
        first: {
            invalid: false,
            valid: false,
            feedback: '',
        },
        last: {
            invalid: false,
            valid: false,
            feedback: '',
        },
        email: {
            invalid: false,
            valid: false,
            feedback: '',
        },
        user: {
            invalid: false,
            valid: false,
            feedback: '',
        },
        role: {
            invalid: false,
            valid: false,
            feedback: '',
        },
        password: {
            invalid: false,
            valid: false,
            feedback: '',
        },
        confirm: {
            invalid: false,
            valid: false,
            feedback: '',
        },
    },
    product_validation: {
        name: {
            invalid: false,
            valid: false,
            feedback: '',
        },
        description: {
            invalid: false,
            valid: false,
            feedback: '',
        },
        brand: {
            invalid: false,
            valid: false,
            feedback: '',
        },
        type: {
            invalid: false,
            valid: false,
            feedback: '',
        },
        price: {
            invalid: false,
            valid: false,
            feedback: '',
        },
    },
    pass_validation: {
        old: {
            invalid: false,
            valid: false,
            feedback: '',
        },
        new: {
            invalid: false,
            valid: false,
            feedback: '',
        },
        confirm: {
            invalid: false,
            valid: false,
            feedback: '',
        },
    },
    foo: {
        name: false,
        email: false,
        username: false
    },
    confirm: {
        delete: false,
        update: false
    },
    sidenav: {
        products: false,
        users: true,
        toggle: true,
        images: false,
        settings: false,
        documentation: false,
        downloadables: false,
        dashboard: true,
        numbers: false,
        sms: false,
    },
    notification: {
        display: false,
        status: 'bg-success',
        message: 'Success!',
    },
    nav: {
        about: false,
        services: false,
        contact: false
    },
    progress_bar: {
        progress: 0
    },
    phrases: {
        all: ['generating...', 'here it goes...', 'this may take a few minutes..', 'maybe an hour?', 'taking longer than expected!', 'nothing to be afraid of...', 'I assure you!', 'did the devs increase the difficulty?', 'do you know that sitting too long is bad for your health?', 'give yourself a break', 'stand up and stretch those nerves...', 'I think this page is about to crash', 'reading this phrase aint good anymore!', 'Ok thats enough, I\' reload this for you'],
		current: "",
    }
  },
  mutations: {
    increment (state) {
      state.count++
    }
  }
})