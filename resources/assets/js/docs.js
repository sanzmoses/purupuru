require('./bootstrap');

import axios from 'axios';
import { loadProgressBar } from 'axios-progress-bar';
window.Vue = require('vue');
import {store} from './store';

const products = new Vue({
    el: '#admin',
    data: {
    	sidenav: store.state.sidenav,
		notif: store.state.notification,
		validation: store.state.product_validation,
		confirm: store.state.confirm,
		search: '',
        loads: {
            general: true,
			search: true,
			updates: false,
		},
    },
    created() {
		//axios progress bar package
		loadProgressBar();
		
		// tooltip
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		});
    	// document.getElementById("load").style.display = "none";
        this.sidenav.documentation = true;
        this.sidenav.users = false;
        
    	// console.log('test');
		this.loads.general = false;
		
		
		window.addEventListener("hashchange", function () {
			window.scrollTo(window.scrollX, window.scrollY - 70);
		});
    },
    computed: {
    	imageURL: function (image) {
    		return this.url+'no-image.png';
		},
		preview_src: function() {
			let p = this.preview;
			return p.path + p.name + '.' + p.extension; 
		}
    },
    methods: {        
        leave: function (el, done) {
	        $('.sidebar-mobile').addClass('slideOutLeft');
	        setTimeout(function() {
	            done();
	        }, 1000);
		},
		openModal(modalId) {
	        $('#'+modalId).modal('toggle');
	    },
	    close(e) {
			// console.log(e.target.id);
			if(e == 'close') {
				$('#showUser').modal('toggle');
				this.setClose('close');
			}
	
			try {
			   if(e.target.id == 'viewProduct' || e.target.id == 'close-x') {
					this.setClose('close');
				} 
			}
			catch(err) {
				console.log(err);
				this.notify('bg-danger', 'Something went wrong!');
			}
		},
		setClose(set) {
			this.update.edit = true;
			this.update.save = false;
			this.preview_index = 0;

			if(this.product.images.length >= 5 && set == 'toggle') {
				this.notify('bg-info', 'You can add up to 5 images only!');
				return;
			}

			if(this.confirm.update == false && set == 'toggle') {
				this.confirm.update = true;
				return ;
			} else {
				set = 'close';
			}

			if(set == 'close') {
				this.confirm.update = false;
				this.confirm.fromImages = false;
			}
		},
		notify(status, message, delay) {
			if(delay == undefined){ delay = 1500 };
			let inst = this;
			//status = ['bg-warning', 'bg-info', and etc]
			inst.notif.status = status;
			inst.notif.message = message;
			inst.notif.display = true;
			setTimeout(function() {
				inst.notif.display = false;
			}, delay);
		}
    }
   
});
