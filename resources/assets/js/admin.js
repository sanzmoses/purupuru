require('./bootstrap');

import axios from 'axios';
import { loadProgressBar } from 'axios-progress-bar';
window.Vue = require('vue');
import {store} from './store';

const admin = new Vue({
    el: '#admin',
    data: {
        validation: store.state.validation,
        sidenav: store.state.sidenav,
        notif: store.state.notification,
        progress_bar: store.state.progress_bar,
        info: {},
        loads: {
            general: true,
            search: true,
        },
        regbutton: "Register User",
        users: "",
        viewUser: {
            id: '',
            first: '',
            last: '',
            email: '',
            user: '',
            role: '',
            verified: '',
        },
        newUser: {
            first: '',
            last: '',
            email: '',
            user: '',
            role: '',
            password: '',
            confirm: '',
        },
        navclass: 'nav-link',
        items: '',
        isActive: {
            all: 1,
            master: 0,
            admin: 0,
            member: 0
        },
        role: 'all',
        search: '',
        perpage: 10,
        order: {
            by: 'id',
            in: true
        },
        paginateArr: [],
        paginate: {
            lastpage: '',
            from: '',
            to: '',
            curpage: '',
            dividedLinks: '',
        },
        prev: true,
        next: false,
        back: true,
        forward: false,
        group: {
            limit: '',
            current: 1,
            jumps: 1,
            divisor: 5
        },
        foo: {
            name: false,
            email: false,
            username: false
        },
        confirm: {
            delete: false,
            update: false
        },
        loading: false,
        bool: false,
        done: ""
},
mounted () {
    //axios progress bar package
    loadProgressBar();
        
    // document.getElementById("load").style.display = "none";
    // let div = document.getElementById("load");
    // div.className += " animated fadeOut";
    // this.loader(div);
    // let ins = this; 
    // setTimeout(function() {
    //     div.className += " animated fadeOut";
    //     ins.loader(div);
    // }, 1000);

    this.loads.general = false;

    this.sidenav.users = true;
    this.sidenav.products = false;
    this.sidenav['images'] = false;

    this.fetch();
    this.fetchInfo();
    $(function () {
        //enable tooltip and popovers
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    })

},
computed: {
},
methods: {
    fetchInfo() {
        axios.get('/users/info')
            .then(response => {
                this.info = response.data;
            })
            .catch(e => {
              console.log(e);
            })
            //end axios
    },
    loader(div) {
        // setTimeout(function() {
        //     div.style.display = 'none';
        // }, 500);
    },
    setActive(index) {
        // console.log('test');
        //mapping which is which in the active class to toggle tabs
        var arr = $.map(this.isActive, function(key,val){
            if(key == 1) return val
        });
        //setting up changed of active element in the dom
        this.isActive[arr] = 0;
        this.isActive[index] = 1;
        //setting up for fetching data according to role
        this.role = index;
        //cleaning pagination links
        this.refreshTable();

        this.fetch();

    },
    refreshTable() {
        this.group.jumps = 1;
        this.group.current = 1;
        this.search = '';
    },
    refreshInputs() {
        let i = this;
        let v = this.validation;  
        let user = this.newUser;

        for (var key in v) {
            if (v.hasOwnProperty(key)) {
                v[key].valid = "";
            }
        }

        user.first = "";
        user.last = "";
        user.email = "";
        user.user = "";
        user.password = "";
        user.confirm = "";
        i.confirm.update = !i.confirm.update;
    },
    fetch(num) {
        this.loads.list = true;
        // fetch data
        // console.log("passed = "+num);
        if(num == this.paginate.curpage) {
            return;
        }

        let diz = this;
        let orderIn; 
        (diz.order.in) ? orderIn = 'asc' : orderIn = 'desc';
            // console.log('page called: '+num);
        axios.get('/user/all', 
                { params: 
                    {
                        paginate: this.perpage,
                        role: this.role,
                        string: this.search,
                        page: num,
                        orderby: this.order.by,
                        orderin: orderIn
                    } 
                })
            .then(response => {
                // console.log(response);
                //setting up shortcuts
                let data = response.data;
                let paginate = this.paginate;
                let group = this.group;
                let divisor = this.group.divisor;

                this.users = data.data;
                paginate.lastpage = data.last_page;
                paginate.from = data.from;
                paginate.to = data.to;
                paginate.curpage = data.current_page;
                group.limit = Math.ceil(data.last_page / divisor);


                // console.log(this.paginateArr.length);
                //cleaning up pagination num links
                if(this.paginateArr.length > 0){
                    this.paginateArr = [];
                }

                //setting up pagination links
                var num = group.jumps;
                // console.log('num = '+num);
                if(data.last_page % divisor > 0 && group.limit == group.current) {
                    divisor = data.last_page % divisor
                }

                for(var x = 0; x < divisor; x++){
                    let p = {
                        num: num,
                        active: false,
                    };

                    if(num == data.current_page) {
                        p.active = true;
                    }

                    this.paginateArr.push(p);
                    num++;
                }
                // console.log(num);
                // console.log(this.paginate);
            })
            .catch(e => {
              console.log(e);
            })
            //end axios
        },
    inOrder() {
        this.order.in = !this.order.in;
        this.refreshTable();
        this.fetch();
    },
    skip(num) {
        this.group.jumps += num;
        (num > 1) ? this.group.current++: this.group.current--;
        this.fetch(this.group.jumps);
    },
    step(page) {
        if(this.paginateArr[4] !== undefined) {
            if (this.paginate.curpage == this.paginateArr[4].num && page > 0) {
                this.skip(this.group.divisor);
                return;
            } 
        }
        
        if (this.paginate.curpage == this.paginateArr[0].num && page < 0) {
            this.skip(-this.group.divisor);
            return;
        }
        
        this.fetch(this.paginate.curpage + page);
        
    },
    fetchUser(id) {
        axios.get('/user/show', 
                { params: 
                    {
                        id: id
                    } 
                })
            .then(response => {
                // console.log(response);
                let data = response.data;
                let user = this.viewUser;

                user.first = data.fname;
                user.last = data.lname;
                user.email = data.email;
                user.user = data.username;
                user.role = data.role;
                user.verified = data.verified;
                user.id = data.id;
            })
            .catch(e => {
              console.log(e);
            })
            //end axios
    },
    showUser(id) {
        // console.log(id);
        this.fetchUser(id);
        $('#showUser').modal('toggle');
    },
    addUser() {
        // this.confirm.update = !this.confirm.update;
        let v = this.validation;
        let i = this;
        let errorCount = 0;
        let user = this.newUser;

        for (var key in v) {
            if (v.hasOwnProperty(key)) {
                i.validateInput(key);
                if (v[key].invalid) errorCount++;
            }
        }
        // console.log(errorCount);
        if (errorCount == 0) {

            this.confirm.update = !this.confirm.update;
            axios.post('/user/create', 
                    {
                        fname: user.first,
                        lname: user.last,
                        email: user.email,
                        role: user.role,
                        username: user.user,
                        password: user.password
                    } 
                )
            .then(response => {
                // console.log(response);

                setTimeout(function(){

                    i.openModal('addUser');
                    i.refreshInputs();

                }, 2000);
            })
            .catch(e => {
              console.log(e);
            })

        } else {
            i.regbutton = "Come on!";
        }
    },
    openModal(modalId) {
        $('#'+modalId).modal('toggle');
    },
    change(action, type) {
        // console.log('in');
        (action == 'edit') ? this.foo[type] = true : this.foo[type] = false;
    },
    close(e) {

        if(e == 'close') {
            $('#showUser').modal('toggle');
            this.setClose();
        }

        try {
           if(e.target.id == 'showUser' || e.target.id == 'close-x') {
                this.setClose();
            } 
        }
        catch(err) {
            console.log(err);
        }
        
    },
    setClose() {
        this.foo.name = false;
        this.foo.username = false;
        this.foo.email = false;
    },
    trashUser(confirmation, value) {
        if(confirmation == false) {
            this.confirm.delete = true;
        }
        else {
            if (value == 'yes') {
                this.loading = true;
                let instance = this;
                setTimeout(function() {
                    instance.confirm.delete = false;
                    instance.loading = false;
                    instance.close('close');
                    instance.deleteUser(instance.viewUser.id);
                }, 1000);
            }
            else{
                this.confirm.delete = false;
            }

        }
    },
    updateUser() {
        this.confirm.update = true;
        let user = this.viewUser;
        let ins = this;
        setTimeout(function() {
           axios.post('/user/update', 
                { 
                    id: user.id,
                    fname: user.first,
                    lname: user.last,
                    email: user.email,
                    username: user.user
                })
            .then(response => {
                // console.log(response);
                ins.confirm.update = false;
                ins.fetch();
                ins.close("close");
            })
            .catch(e => {
              console.log(e);
            })     
        }, 3000);
    },
    deleteUser(id) {
        axios.post('/user/delete', 
                { id: id })
            .then(response => {
                this.fetch();
            })
            .catch(e => {
              console.log(e);
            })
    },
    async checkDuplicate(field, input) {

        // console.log(bool);
        return await axios.get('/user/checkDuplicates', {
            params: {
               field: field,
                input: input 
            }
        })
        .then(response => {
           return response.data;
            // bool = true;
        })
        .catch(e => {
          console.log(e);
        })
    },
    validate(input, type) {
        if(type == 'email') {
             var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(input).toLowerCase());
        }
        else if(type == 'alphanumonly') {
           let reg = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
            return reg.test(input);
        }
        else if(type == 'alphanum') {
             let reg = /^[a-zA-Z0-9_]*$/;
             return reg.test(input);
        }
        
    },
    async validateInput(input) {
        let v = this;

        if(v.newUser[input] == "") {
            v.validation[input].invalid = true;
            v.validation[input].feedback = "This input field is required!"
        }
        else if(input == 'email') {
            v.bool = false;
            await v.checkDuplicate('email', v.newUser[input]).then(data => { v.bool = data });

            if(!v.validate(v.newUser[input], 'email')) {
                v.validation[input].invalid = true; 
                v.validation[input].feedback = "Invalid Email"
            }
            else if (v.bool) {
                v.validation[input].invalid = true; 
                v.validation[input].feedback = "Email already taken!"
            }
            else {
                valid();
            }
        }
        else if(input == 'password') {
            if(!v.validate(v.newUser[input], 'alphanumonly')) {
                v.validation[input].invalid = true;
                v.validation[input].feedback = "Password must contain a letter and a number.";
            }
            else if(v.newUser[input].length < 8) {
                v.validation[input].invalid = true;
                v.validation[input].feedback = "Minimum of 8 characters";
            }
            else {
                valid();
            }            
        }
        else if(input == 'confirm' && v.newUser[input] != v.newUser.password) {
            v.validation[input].invalid = true;
            v.validation[input].feedback = "Must match with password";
        }
        else if(input == 'user'){
            v.bool = false;
            await v.checkDuplicate('username', v.newUser[input]).then(data => { v.bool = data });

            if(!v.validate(v.newUser[input], 'alphanum')) {
                    v.validation[input].invalid = true;
                    v.validation[input].feedback = "Only Alphanumeric Characters and Underscore is allowed!"; 
            }
            else if (v.newUser[input].length < 6) {
                    v.validation[input].invalid = true;
                    v.validation[input].feedback = "Minimum of 6 characters"; 
            }
            else if (v.bool) {
                    v.validation[input].invalid = true;
                    v.validation[input].feedback = "Username already taken!"; 
            }
            else {
                    valid();
            }
              
        }
        else {
            //check for duplicate
            valid();
        }

        function valid() {
            v.validation[input].invalid = false;
            v.validation[input].valid = true;
            v.validation[input].feedback = "Validated!"
        }
        // console.log("yeah");
            
    },
    leave: function (el, done) {
        $('.sidebar-mobile').addClass('slideOutLeft');
        setTimeout(function() {
            done();
        }, 1000);

    }
},
computed: {
    filteredSearch: function(){
    	// 		if(this.search != ''){
					// return $.map(this.shown, (value, index) => {
					// 	var search = new RegExp(this.search.toLowerCase())
	    // 				if(search.test(value.fname.toLowerCase()) ||
	    // 					search.test(value.lname.toLowerCase()) ||
	    // 					search.test(value.username.toLowerCase()) ||
	    // 					search.test(value.email.toLowerCase())){
	    // 					return value;
	    // 				}
	    // 			});
    	// 		}
    	// 		return this.shown;
    },
    fullName: function() {
        return this.viewUser.first +' '+this.viewUser.last;
    },
    upperFirst: function() {
        return lower.replace(/^\w/, c => c.toUpperCase());
    },
    dateNow: function() {
        return new Date();
    }
},
watch: {
    perpage: function() {
        this.fetch();
    },
    search: function() {
        this.loads.search = false;
        let inst = this;
        setTimeout(function() {
            inst.loads.search = true;
            inst.fetch();
        }, 1500);
    },
    paginate: {
        handler: function(p) {
            (p.curpage > 1) ? this.prev = false : this.prev = true;
            (p.curpage == p.lastpage) ? this.next = true: this.next = false;
        },
        deep: true
    },
    group: {
        handler: function(p) {
            (p.current > 1) ? this.back = false : this.back = true;
            (p.current == p.limit) ? this.forward = true: this.forward = false;
        },
        deep: true
    }
},
filters: {
    formatDate: function(value) {
        var dt = new Date(value);
        var dd = dt.getDate();
        var mm = dt.getMonth()+1;
        var yyyy = dt.getFullYear();
        var time = dt.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});;
        return dt = mm + '/' + dd + '/' + yyyy + ' - ' + time;
    },
    formatTime: function(value) {
        var dt = new Date(value);
        return dt.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});;
    },
    monthName: function(monthNum) {
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        return month[monthNum-1];
    }
}
});