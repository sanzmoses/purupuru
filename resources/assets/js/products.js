require('./bootstrap');

import axios from 'axios';
import { loadProgressBar } from 'axios-progress-bar';
window.Vue = require('vue');
import {store} from './store';

const products = new Vue({
    el: '#admin',
    data: {
    	sidenav: store.state.sidenav,
		notif: store.state.notification,
		validation: store.state.product_validation,
		confirm: {
            delete: false,
			update: false,
			loading: false,
			fromImages: false,
			register: false,
		},
		update: {
			edit: true,
			save: false,
			processing: false,
			changes: false,
		},
		registration: {
			name: '',
			description: '',
			brand: '',
			type: '',
		},
		regbutton: "Register Product",
		products: "",
		product: "",
		product_index: "",
		products_imgs: [],
		product_imgs: [],
		preview: "",
		preview_index: 0,
    	url: "/storage/",
		type: 'all',
		brand: 'all',
		search: '',
		searchImg: '',
        perpage: 10,
        order: {
            by: 'id',
            in: true
        },
        paginateArr: [],
        paginate: {
            lastpage: '',
            from: '',
            to: '',
            curpage: '',
            dividedLinks: '',
        },
        prev: true,
        next: false,
        back: true,
        forward: false,
        group: {
            limit: '',
            current: 1,
            jumps: 1,
            divisor: 5
        },
        loads: {
            general: true,
			search: true,
			searchImg: true,
			updates: false,
			list: false,
		},
		trigger: '',
        file: [],
    },
    created() {
		//axios progress bar package
		loadProgressBar();
		
		// tooltip
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		});
    	// document.getElementById("load").style.display = "none";
    	this.sidenav.products = true;
    	this.sidenav.users = false;
    	this.sidenav['images'] = false;
    	// console.log('test');
    	this.loads.general = false;
    	this.fetch();
    },
    computed: {
    	imageURL: function (image) {
    		return this.url+'no-image.png';
		},
		preview_src: function() {
			let p = this.preview;
			return p.path + p.name + '.' + p.extension; 
		}
    },
    methods: {
	    fetch(num) {
			this.loads.list = true;
	        // fetch data
	        // console.log("passed = "+num);
	        if(num == this.paginate.curpage) {
	            return;
	        }

	        let diz = this;
	        let orderIn; 
	        (diz.order.in) ? orderIn = 'asc' : orderIn = 'desc';
	        
	        // console.log(orderIn);
	        axios.get('/products/list', 
	                { params: 
	                    {
	                        paginate: this.perpage,
	                        type: this.type,
	                        brand: this.brand,
	                        string: this.search,
	                        page: num,
	                        orderby: this.order.by,
	                        orderin: orderIn
	                    } 
	                })
	            .then(response => {
	                // console.log(response);
	                //setting up shortcuts
	                let data = response.data;
	                let paginate = this.paginate;
	                let group = this.group;
	                let divisor = this.group.divisor;

	                this.products = data.data;
	                paginate.lastpage = data.last_page;
	                paginate.from = data.from;
	                paginate.to = data.to;
	                paginate.curpage = data.current_page;
	                group.limit = Math.ceil(data.last_page / divisor);

	                // console.log(this.paginateArr.length);
	                //cleaning up pagination num links
	                if(this.paginateArr.length > 0){
	                    this.paginateArr = [];
	                }

	                //setting up pagination links
	                var num = group.jumps;
	                // console.log('num = '+num);
	                if(data.last_page % divisor > 0 && group.limit == group.current) {
	                    divisor = data.last_page % divisor
	                }

	                for(var x = 0; x < divisor; x++){
	                    let p = {
	                        num: num,
	                        active: false,
	                    };

	                    if(num == data.current_page) {
	                        p.active = true;
	                    }

	                    this.paginateArr.push(p);
	                    num++;
					}
					
					this.loads.list = false;
	                // console.log(num);
	                // console.log(this.paginate);
	            })
	            .catch(e => {
				  console.log(e);
				  this.notify('bg-danger', 'Something went wrong!');
            })
            //end axios
		},
		fetchImgs() {
			axios.get('/image/search', { 
				params: {
					string: this.searchImg
				}
			}).then(response => {
				this.products_imgs = response.data;
				// console.log(this.products_imgs);
			}).catch(e => {
				console.log(e);
				this.notify('bg-danger', 'Something went wrong!');
			});
		},
		link() {
			let inst = this;
			if(inst.product.images.length + inst.product_imgs.length > 5) {
				inst.notify('bg-info', 'Only 5 images per product are allowed!');
				return ;
			}

			axios.post('/product-images/link', 
			{
					images: inst.product_imgs,
					product: inst.product,
			})
			.then(response => {
				inst.product = response.data.product;
				inst.products[inst.product_index] = response.data.product;
				inst.products_imgs = [];
				inst.product_imgs = [];
				inst.preview = inst.product.images[0];
				inst.searchImg = "";
				inst.setClose();
			})
			.catch(e => {
				console.log(e);
				this.notify('bg-danger', 'Something went wrong!');
			});
		},
		checkBox(id) {
			console.log('checkbox');
			let ticked = document.getElementById(id).checked;
			if(ticked == true) {
				document.getElementById(id+'-cover').style.opacity = "1";
			} else {
				document.getElementById(id+'-cover').style.opacity = "0";
			}
		},
		setThumbnail() {
			let inst = this;
			axios.post('/product/setThumbnail/'+inst.product.id, 
			{
					new: inst.preview.id,
					current: inst.product.images[0].id,
			})
			.then(response => {
				inst.product.images = response.data;
				inst.preview_index = 0;
			})
			.catch(e => {
				console.log(e);
				this.notify('bg-danger', 'Something went wrong!');
			});
		},
		showProduct(id, index, event) {
			console.log('showproduct');
			//if delete button is clicked
			let e = event.target;
			let value = ['', ''];

			try {
				value[1] = e.className;
				value[0] = e.children[0].className;
			}
			catch(e) {
				// console.log(e);
			}

			if(value[0] == 'fa fa-trash' || value[1] == 'fa fa-trash') {
				if(window.confirm('Images under this product will be deleted too. Are you sure?')) {
										
					//delete record in db
					axios.post('/product/delete/'+id)
					.then(response => {
						this.fetch();
						this.notify('bg-success', 'Record Deleted!');
					})
					.catch(e => {
						// console.log(e);
						this.notify('bg-danger', 'Something went wrong!');
					});
				}
				return ;
			}

			this.product = this.products[index];
			this.product_index = index;
			// console.log(this.product.images);
			if(this.product.images.length > 0) {
				this.preview = this.product.images[0];
			}
			else {
				this.preview = "";
			}

			this.openModal('viewProduct');
			// console.log(this.product);
		},
		changePreview(image, index) {
			this.preview = image;
			this.preview_index = index;
		},
		remove(type) {		
			//do the actual removal
			let url = '';
			let question = '';
			switch(type) {
				case 'link':
					url = '/product-image/unlink';
					question = 'Are you sure you want to unlink the image from the current product?';
					break;
				case 'completely':
					url = '/image/delete';
					question = 'Are you sure you want to DELETE the image?';
					break;
				default:
					console.log('wtf are you doing here?!');
			}
			let confirm = window.confirm(question);
			if(url != '' && confirm) {
				axios.post(url, 
				{
						id: this.preview.id,
				})
				.then(response => {
					if(response.data.status == 'success') {
						this.product.images.splice(this.preview_index, 1);
						if(this.product.images.length == this.preview_index) {
							this.preview_index -= 1;
							// console.log(this.preview_index);
							this.preview = this.product.images[this.preview_index];
						} else {
							this.preview = this.product.images[this.preview_index];
						}
					}
				})
				.catch(e => {
					console.log(e);
					this.notify('bg-danger', 'Something went wrong!');
				});
			}
		},
		updating(process) {
			let inst =  this;

			if(process == 'edit') {
				inst.update.changes = false;
				inst.update.edit = false;
				inst.update.save = true;
			}

			if(process == 'save') {
				let changes = false;
				if(inst.update.changes == true) {
					//changes were made save to db
					axios.post('/product/update/'+this.product.id, 
						{
								product: this.product,
						})
						.then(response => {
							// console.log(response.data);
							changes = true;
						})
						.catch(e => {
							console.log(e);
							this.notify('bg-danger', 'Something went wrong!');
						});
				}

				inst.update.save = false;
				inst.update.processing = true;

				setTimeout(function(){
					inst.update.processing = false;
					inst.update.changes = false;
					inst.update.edit = true;

					if(changes) {
						inst.notify('bg-success', 'Product Updated!');
					}

				}, 3000);
			}
		},
		async checkDuplicate(field, input) {
			// console.log(bool);
			return await axios.get('/product/checkDuplicates', {
				params: {
				    field: field,
					input: input 
				}
			})
			.then(response => {
			   return response.data;
				// bool = true;
			})
			.catch(e => {
			  console.log(e);
			})
		},
		validate(input, type) {
			let reg;
			if(type == 'alphanumonly') reg = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
			//alphanum and whitespaces
			if(type == 'alphanum') reg = /^[a-zA-Z0-9\- ]*$/;
			if(type == 'alpha') reg = /^[a-zA-Z]*$/;
			if(type == 'numeric') reg = /^[0-9.]*$/;
			return reg.test(input);
		},
		async validateInput(input) {
			let v = this;
	
			if(v.registration[input] == "" && input != 'description') {
				v.validation[input].invalid = true;
				v.validation[input].feedback = "This input field is required!"
			}
			else if(input == 'name') {
				v.bool = false;
				await v.checkDuplicate(input, v.registration[input]).then(data => { v.bool = data });
	
				if(!v.validate(v.registration[input], 'alphanum')) {
					v.validation[input].invalid = true; 
					v.validation[input].feedback = "Invalid Input. Only alphanumerics are allowed!"
				}
				else if (v.bool) {
					v.validation[input].invalid = true; 
					v.validation[input].feedback = "Name already taken!"
				}
				else {
					valid();
				}
			}
			else if(input == 'brand') {
				if(!v.validate(v.registration[input], 'alphanum')) {
					v.validation[input].invalid = true; 
					v.validation[input].feedback = "Invalid Input. Only alphanumerics are allowed!"
				}
				else {
					valid();
				}
			}
			else if(input == 'type'){
				if(!v.validate(v.registration[input], 'alpha')) {
						v.validation[input].invalid = true;
						v.validation[input].feedback = "Only Alphabetic Characters are allowed!"; 
				}
				else {
					valid();
				}
			}
			// else if(input == 'price') {
			// 	if(!v.validate(v.registration[input], 'numeric')) {
			// 		v.validation[input].invalid = true;
			// 		v.validation[input].feedback = "Invalid Characters!"; 
			// 	}
			// 	else {
			// 		valid();
			// 	}
			// }
			else {
				valid();
			}
	
			function valid() {
				v.validation[input].invalid = false;
				v.validation[input].valid = true;
				v.validation[input].feedback = "Validated!"
			}
			// console.log("yeah");
		},
		addProduct() {
			// this.confirm.update = !this.confirm.update;
			let v = this.validation;
			let i = this;
			let errorCount = 0;
			let user = this.registration;
	
			for (var key in v) {
				if (v.hasOwnProperty(key)) {
					i.validateInput(key);
					if (v[key].invalid) errorCount++;
				}
			}
			// console.log(errorCount);
			if (errorCount == 0) {
				
				this.confirm.register = !this.confirm.register;
				axios.post('/product/create', 
						{
							product: this.registration,
						} 
					)
				.then(response => {					  
					setTimeout(function(){
						i.openModal('addProduct');
						i.refreshInputs();
						i.notify('bg-success', response.data.status);
						i.fetch();
					}, 2000);
				})
				.catch(e => {
				  console.log(e);
				  this.notify('bg-danger', 'Something went wrong!');
				})
			} else {
				i.regbutton = "Come on!";
				this.notify('bg-danger', 'Please enter valid data!');
			}
		},
		refreshInputs() {
			let i = this;
			let v = this.validation;  
			let product = this.registration;
	
			for (var key in v) {
				if (v.hasOwnProperty(key)) {
					v[key].valid = "";
				}
			}
	
			for (var key in product) {
				product[key] = "";
			}

			i.confirm.register = !i.confirm.register;
			// user.first = "";
			// user.last = "";
			// user.email = "";
			// user.user = "";
			// user.password = "";
			// user.confirm = "";
			// i.confirm.update = !i.confirm.update;
		},
        skip(num) {
	        this.group.jumps += num;
	        (num > 1) ? this.group.current++: this.group.current--;
	        this.fetch(this.group.jumps);
	    },
	    step(page) {
	        if(this.paginateArr[4] !== undefined) {
	            if (this.paginate.curpage == this.paginateArr[4].num && page > 0) {
	                this.skip(this.group.divisor);
	                return;
	            } 
	        }
	        
	        if (this.paginate.curpage == this.paginateArr[0].num && page < 0) {
	            this.skip(-this.group.divisor);
	            return;
	        }
	        
	        this.fetch(this.paginate.curpage + page);
	        
	    },
        leave: function (el, done) {
	        $('.sidebar-mobile').addClass('slideOutLeft');
	        setTimeout(function() {
	            done();
	        }, 1000);
		},
		openModal(modalId) {
	        $('#'+modalId).modal('toggle');
	    },
	    close(e) {
			// console.log(e.target.id);
			if(e == 'close') {
				$('#showUser').modal('toggle');
				this.setClose('close');
			}
	
			try {
			   if(e.target.id == 'viewProduct' || e.target.id == 'close-x') {
					this.setClose('close');
				} 
			}
			catch(err) {
				console.log(err);
				this.notify('bg-danger', 'Something went wrong!');
			}
		},
		setClose(set) {
			this.update.edit = true;
			this.update.save = false;
			this.preview_index = 0;

			if(this.product.images.length >= 5 && set == 'toggle') {
				this.notify('bg-info', 'You can add up to 5 images only!');
				return;
			}

			if(this.confirm.update == false && set == 'toggle') {
				this.confirm.update = true;
				return ;
			} else {
				set = 'close';
			}

			if(set == 'close') {
				this.confirm.update = false;
				this.confirm.fromImages = false;
			}
		},
	    importFile() {
	    	let formData = new FormData();
		    // var file = document.getElementById('file');
            for( var i = 0; i < this.file.length; i++ ){
	          let file = this.file[i];
	          console.log(file.name);
	          formData.append('file', file);
	        }

            axios.post( '/products/import',
	          formData,
	          {
	            headers: {
	                'Content-Type': 'multipart/form-data'
	            }
	          }
	        ).then(function(response){
	          console.log(response);
	        })
	        .catch(function(e){
			  console.log(e);
			  this.notify('bg-danger', 'Something went wrong!');
	        });
	    },
		handleFilesUpload(){
			let uploadedFiles = this.$refs.file.files;
			for( var i = 0; i < uploadedFiles.length; i++ ){
	          this.file.push(uploadedFiles[i]);
	        }
		},
		notify(status, message, delay) {
			if(delay == undefined){ delay = 1500 };
			let inst = this;
			//status = ['bg-warning', 'bg-info', and etc]
			inst.notif.status = status;
			inst.notif.message = message;
			inst.notif.display = true;
			setTimeout(function() {
				inst.notif.display = false;
			}, delay);
		}
    },
    watch: {
	    perpage: function() {
	    	this.fetch();
	    },
        search: function() {
	        let inst = this;
			inst.loads.search = false;
			clearTimeout(inst.trigger);
	        inst.trigger = setTimeout(function() {
	            inst.loads.search = true;
	            inst.fetch();
	        }, 1500);
		},
		searchImg: function() {
	        let inst = this;
			inst.loads.searchImg = false;
			clearTimeout(inst.trigger);
	        inst.trigger = setTimeout(function() {
	            inst.loads.searchImg = true;
	            inst.fetchImgs();
	        }, 1500);
		},
	    paginate: {
	        handler: function(p) {
	            (p.curpage > 1) ? this.prev = false : this.prev = true;
	            (p.curpage == p.lastpage) ? this.next = true: this.next = false;
	        },
	        deep: true
	    },
	    group: {
	        handler: function(p) {
	            (p.current > 1) ? this.back = false : this.back = true;
	            (p.current == p.limit) ? this.forward = true: this.forward = false;
	        },
	        deep: true
		},
		product: {
			handler: function(p) {
				this.update.changes = true;
			},
			deep: true
		}
    },
    filters: {
    	currency: function(price) {
			// return price;
			if(price) {
				return '₱ ' + price.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
			}
    		// .toFixed(2)
		},
		formatDate: function(value) {
			var dt = new Date(value);
            var dd = dt.getDate();
            var mm = dt.getMonth()+1;
            var yyyy = dt.getFullYear();
            // var time = dt.toLocaleTimeString();
            return dt = mm + '/' + dd + '/' + yyyy;

		}
    }
});